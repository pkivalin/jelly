About PlayHaven

PlayHaven is the business engine for mobile games offering developers flexible tools to intelligently manage player acquisition, engagement, and monetization. PlayHaven is the only end-to-end solution that helps developers manage users at every valuable point in their lifecycle.

PlayHaven’s platform spans 20,000+ games, 800+ million unique users, 176+ million monthly active users, and more than 14 billion in-game events each month. Game developers including Cartoon Network, Glu Mobile, Game Circus, Namco Bandai, NaturalMotion, Nickelodeon, and SEGA leverage PlayHaven’s tools to manage the business of their games.


Documentation

All documentation is available in the PlayHaven Help Center at http://help.playhaven.com/customer/portal/articles/1347100-unity-plug-in.


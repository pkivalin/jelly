﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using Holoville.HOTween;

public class InteractiveTutorial : MonoBehaviour {

    public tk2dSprite bubble;
    public tk2dSprite fingerSprite;
    public tk2dSprite touchSprite;
    public tk2dSprite arrowSprite;
    public tk2dSprite textSprite;
    public tk2dTextMesh text;
    public PulsationLifebarAnimator textPulsator;
    public PulsationLifebarAnimator handPulsator;
    public tk2dSprite wellDoneSpite;

    protected int _curStep;
    protected int _previousStep;
    protected FruitFactory _fruitFactory;
    protected Game _game;
    protected XmlNode _settings;
    protected int _stepCounter;
    protected int _stepNeededCounter;

    private const float FIRST_TWEEN_DURATION = 0.27f;
    private const float SECOND_TWEEN_DURATION = 0.4f;
    private const float THIRD_TWEEN_DURATION = 0.4f;

    private Color _lastColor = Color.white;
    private Vector3 _newScale = new Vector3(0.0f, 0.0f, 0.0f);
    private Sequence _sequence;

    public void init(XmlNode settings, FruitFactory fruitFactory, Game game)
    {
        _settings = settings;
        _fruitFactory = fruitFactory;
        _game = game;
        gameObject.SetActive(false);
    }
        
    public void start()
    {
        GameManager.HIDE_COMBO = true;
        _curStep = 0;
        _previousStep = -1;
        _stepCounter = 0;
        _stepNeededCounter = 0;
        GameManager.TUTORIAL_RUNNING = true;
        _game.lifebar.hideForTutorial = true;

        text.text = _settings["step1"]["text"].InnerText;
        text.Commit();
        text.gameObject.SetActive(true);
        fingerSprite.gameObject.SetActive(true);
        bubble.gameObject.SetActive(true);
        touchSprite.gameObject.SetActive(true);
        arrowSprite.gameObject.SetActive(false);
        wellDoneSpite.gameObject.SetActive(false);
        textSprite.SetSprite("textTip1");
        fingerSprite.transform.Rotate(0, 0, -fingerSprite.transform.eulerAngles.z);

        gameObject.SetActive(true);
        StartCoroutine(_processStep());
        StartCoroutine(_handPulsation());

        textPulsator.startPulsation(2);

        TweenParms tweenParams1 = new TweenParms();
        tweenParams1.Prop("scale", new Vector3(1.0f, 1.0f, 1.0f)); 
        tweenParams1.Ease(EaseType.Linear);

        TweenParms tweenParams2 = new TweenParms();
        tweenParams2.Prop("scale", new Vector3(1.2f, 1.2f, 1.2f));
        tweenParams2.Ease(EaseType.Linear);

        TweenParms tweenParams3 = new TweenParms();
        tweenParams3.Prop("color", _lastColor);
        tweenParams3.Prop("scale", new Vector3(1.3f, 1.3f, 1.3f)); 
        tweenParams3.Ease(EaseType.Linear);
        tweenParams3.OnComplete(_hideWellDone);

        _sequence = new Sequence();
        _sequence.Append(HOTween.To(wellDoneSpite, FIRST_TWEEN_DURATION, tweenParams1));
        _sequence.Append(HOTween.To(wellDoneSpite, SECOND_TWEEN_DURATION, tweenParams2));
        _sequence.Append(HOTween.To(wellDoneSpite, THIRD_TWEEN_DURATION, tweenParams3));
        _sequence.autoKillOnComplete = false;
    }

    protected void _showWellDone()
    {
        return; // TODO убрать если вернем
        wellDoneSpite.gameObject.SetActive(true);
        wellDoneSpite.color = Color.white;
        wellDoneSpite.scale = _newScale;
        _sequence.Restart();
    }

    private void _hideWellDone()
    {
        wellDoneSpite.gameObject.SetActive(false);
    }

    public virtual void catchFruit(Fruit fruitController, bool success, bool isBug)
    {
    }

    protected virtual IEnumerator _processStep()
    {
        return null;
    }

    protected virtual IEnumerator _handPulsation()
    {
        return null;
    }

    public virtual void closeTutorial()
    {
        GameManager.HIDE_COMBO = false;
        _curStep = -1;
        GameManager.TUTORIAL_RUNNING = false;
        gameObject.SetActive(false);
        _game.resetTutorial();
        _sequence.Kill();
    }

    protected void _addFruit(XmlNode settings, int fruitIndex = -1)
    {
        string[] allowedFruits = settings["fruits"].InnerText.Trim().Split(new Char [] {';'});
        if (fruitIndex == -1)
        {
            fruitIndex = UnityEngine.Random.Range(0, allowedFruits.Length);
            fruitIndex = System.Array.IndexOf(Fruit.TYPES, allowedFruits[fruitIndex]);
        }

        Vector2 horizontalForce = new Vector2(float.Parse(settings["force"]["horizontal"]["min"].InnerText)
            , float.Parse(settings["force"]["horizontal"]["max"].InnerText));

        Vector2 verticalForce = new Vector2(float.Parse(settings["force"]["vertical"]["min"].InnerText)
            , float.Parse(settings["force"]["vertical"]["max"].InnerText));

        _fruitFactory.addFruitFromSide(fruitIndex, UnityEngine.Random.Range(horizontalForce.x, horizontalForce.y)
            , UnityEngine.Random.Range(verticalForce.x, verticalForce.y));
    }
}

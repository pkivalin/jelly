﻿using UnityEngine;
using System.Collections;
using System.Xml;

public class RecipeTutorial : InteractiveTutorial 
{

    public override void catchFruit(Fruit fruitController, bool success, bool needInRecipe)
    {
        if (_curStep == 0)
        {
            if (success)
            {
                _stepNeededCounter++;
                Alert.addAlert(fruitController.gameObject.transform.position.x, Game.JELLY_ALERT_Y, Alert.NICE);
            }

            if (_stepNeededCounter == 2) 
            {
                _stepNeededCounter = 0;
                _curStep++;
                textSprite.SetSprite("textTip2");
                textPulsator.startPulsation(2);
                text.text = _settings["step2"]["text"].InnerText;
                text.Commit();
                touchSprite.gameObject.SetActive(false);
                fingerSprite.transform.Rotate(0, 0, 290);
                _showWellDone();
            }
        }
        else if (_curStep == 1)
        {
            if (success)
                _stepNeededCounter = 0;
            else
            {
                Alert.addSideAlert(fruitController.gameObject.transform.position.y, !fruitController.fromLeft, Alert.COOL);
                _stepNeededCounter++;
            }

            if (_stepNeededCounter == 2) 
            {
                _stepNeededCounter = 0;
                text.text = _settings["step3"]["text"].InnerText;
                text.Commit();
                touchSprite.gameObject.SetActive(false);
                fingerSprite.gameObject.SetActive(false);
                arrowSprite.gameObject.SetActive(true);
                _game.lifebar.hideForTutorial = false;
                _curStep++;
                _showWellDone();
                textPulsator.startPulsation(2);
                textSprite.SetSprite("textTip4");
            }
        }
        else if (_curStep == 2)
        {
            if (success && needInRecipe)
                Alert.addAlert(fruitController.gameObject.transform.position.x, Game.JELLY_ALERT_Y, Alert.NICE);
            else if (!success && !needInRecipe)
                Alert.addSideAlert(fruitController.gameObject.transform.position.y, !fruitController.fromLeft, Alert.COOL);
        }

        StartCoroutine(_processStep());
    }

    protected override IEnumerator _handPulsation()
    {
        while (_curStep == 0)
        {
            handPulsator.startPulsation(2);
            yield return new WaitForSeconds(1);
        }
    }

    protected override IEnumerator _processStep()
    {
        if (_previousStep != _curStep)
        {
            _previousStep = _curStep;
            yield return new WaitForSeconds(2.0f);
        }

        if (_curStep == 0)
            _addFruit(_settings["step1"]);
        else if (_curStep == 1)
            _addFruit(_settings["step2"]);
        else if (_curStep == 2)
            GameManager.TUTORIAL_RUNNING = false;

        yield return null;
    }
}

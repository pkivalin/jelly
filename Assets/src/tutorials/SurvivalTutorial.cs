﻿using UnityEngine;
using System.Collections;
using System.Xml;

public class SurvivalTutorial : InteractiveTutorial 
{

    public override void catchFruit(Fruit fruitController, bool success, bool needInRecipe)
    {
        if (_curStep == 0)
        {
            if (success)
            {
                Alert.addAlert(fruitController.gameObject.transform.position.x, Game.JELLY_ALERT_Y, Alert.NICE);
                _curStep++;
                text.text = _settings["step2"]["text"].InnerText;
                text.Commit();
            }

            StartCoroutine(_processStep());
        }
        else if (_curStep == 1)
        {
            if (success)
            {
                _stepNeededCounter++;
                Alert.addAlert(fruitController.gameObject.transform.position.x, Game.JELLY_ALERT_Y, Alert.NICE);
            }
            else
            {
                _stepNeededCounter = 0;
            }

            if (_stepNeededCounter == 3) // settings
            {
                _stepNeededCounter = 0;
                _curStep++;
                text.text = _settings["step3"]["text"].InnerText;
                text.Commit();
                touchSprite.gameObject.SetActive(false);
                fingerSprite.transform.Rotate(0, 0, 290);
                textPulsator.startPulsation(2);
                textSprite.SetSprite("textTip2");
                _showWellDone();
            }

            _stepCounter++;
            if (_stepCounter == 3) // settings
            {
                _stepCounter = 0;
                StartCoroutine(_processStep());
            }
        }
        else if (_curStep == 2)
        {
            if (!success)
            {
                Alert.addSideAlert(fruitController.gameObject.transform.position.y, !fruitController.fromLeft, Alert.COOL);
                _stepNeededCounter++;
            }
            else
            {
                _stepNeededCounter = 0;
            }

            if (_stepNeededCounter == 2)
            {
                _stepNeededCounter = 0;
                text.text = _settings["step4"]["text"].InnerText;
                text.Commit();
                fingerSprite.gameObject.SetActive(false);
                textPulsator.startPulsation(2);
                textSprite.SetSprite("textTip3");
                _showWellDone();
                _curStep++;
            }

            StartCoroutine(_processStep());
        }
        else if (_curStep == 3)
        {
            if (success)
            {
                if (fruitController is Bug)
                    _stepNeededCounter = 0;
                else
                {
                    _stepNeededCounter++;
                    Alert.addAlert(fruitController.gameObject.transform.position.x, Game.JELLY_ALERT_Y, Alert.NICE);
                }
            }
            else
            {
                if (fruitController is Bug)
                {
                    _stepNeededCounter++;
                    Alert.addSideAlert(fruitController.gameObject.transform.position.y, !fruitController.fromLeft, Alert.COOL);
                }
                else
                    _stepNeededCounter = 0;
            }

            if (_stepNeededCounter == 6)
            {
                _stepNeededCounter = 0;
                text.text = _settings["step5"]["text"].InnerText;
                text.Commit();
                arrowSprite.gameObject.SetActive(true);
                _game.lifebar.hideForTutorial = false;
                _curStep++;
                _showWellDone();
                textPulsator.startPulsation(2);
                textSprite.SetSprite("textTip4");
                UserData.needSurvivalTutorial = false;
                StartCoroutine(_processStep());
                return;
            }

            _stepCounter++;
            if (_stepCounter == 3) // settings
            {
                _stepCounter = 0;
                StartCoroutine(_processStep());
            }
        }
        else if (_curStep == 4 && success)
        {
            if (success && fruitController is Bug == false)
            {
                Alert.addAlert(fruitController.gameObject.transform.position.x, Game.JELLY_ALERT_Y, Alert.NICE);
            }
            else if (success == false && fruitController is Bug)
            {
                Alert.addSideAlert(fruitController.gameObject.transform.position.y, !fruitController.fromLeft, Alert.COOL);
            }

            _stepCounter++;
            if (_stepCounter == 10) // settings
            {
                _stepCounter = 0;
                _curStep++;
                closeTutorial();
            }
        }
    }

    protected override IEnumerator _handPulsation()
    {
        while (_curStep == 0 || _curStep == 1)
        {
            handPulsator.startPulsation(2);
            yield return new WaitForSeconds(1);
        }
    }

    protected override IEnumerator _processStep()
    {
        if (_curStep != 1 && _previousStep != _curStep)
        {
            _previousStep = _curStep;
            yield return new WaitForSeconds(2.0f);
        }

        if (_curStep == 0)
            _addFruit(_settings["step1"]);
        else if (_curStep == 1)
        {
            for (int i = 0; i < 3; i++) //settings
            {
                _addFruit(_settings["step2"]);
                yield return new WaitForSeconds(float.Parse(_settings["step2"]["fruitInterval"].InnerText));
            }
        }
        else if (_curStep == 2)
        {
            _addFruit(_settings["step3"], Bug.INDEX);
        }
        else if (_curStep == 3)
        {
            _addFruit(_settings["step4"]);
            yield return new WaitForSeconds(float.Parse(_settings["step4"]["fruitInterval"].InnerText));
            _addFruit(_settings["step4"], Bug.INDEX);
            yield return new WaitForSeconds(float.Parse(_settings["step4"]["fruitInterval"].InnerText));
            _addFruit(_settings["step4"]);
        }
        else if (_curStep == 4)
            GameManager.TUTORIAL_RUNNING = false;

        yield return null;
    }
}

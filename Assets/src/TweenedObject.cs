﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class TweenedObject : MonoBehaviour {

    public Vector3[] positions;
    private Tweener _tweener;
    private bool _needDeactivate;

    void Awake()
    {
        for (int i = 0; i < positions.Length; i++)
            positions[i] = PositionedSprite.getCalcedPosition(positions[i]);
    }

    public void tween(int index, float duration, bool needDeactivate = false, int fromIndex = -1, EaseType easyType = EaseType.Linear, float delay = 0.0f)
    {
        if (index >= positions.Length)
        {
            Debug.Log("incorrect index in TweenedObject");
            return;
        }

        _needDeactivate = needDeactivate;
        gameObject.SetActive(true);

        if (fromIndex >= 0 && fromIndex < positions.Length)
            gameObject.transform.position = positions[fromIndex];

        TweenParms tweenParams = new TweenParms();
        tweenParams.Prop("position", positions[index]); 
        tweenParams.Ease(easyType);
        tweenParams.Delay(delay);
        tweenParams.OnComplete(deactivate);

        _tweener = HOTween.To(gameObject.transform, duration, tweenParams);
    }

    private void deactivate()
    {
        if (_needDeactivate)
            gameObject.SetActive(false);

        if (_tweener != null)
        {
            _tweener.Kill();
            _tweener = null;
        }
    }

    public bool inProgress
    {
        get
        {
            return _tweener != null;
        }
    }
}

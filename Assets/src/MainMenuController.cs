﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class MainMenuController : MonoBehaviour 
{

    private const float TWEEN_DURATION = 0.3f;
    private const float SETTINGS_TWEEN_DURATION = 0.2f;
    private const int STATE_MAIN = 0;
    private const int STATE_SHOP = 1;
    private const int STATE_PACKS = 2;
    private const int STATE_LVEL_MAP = 3;
    private const int STATE_SETTINGS = 4;

	public tk2dUIItem recipesBtn;
	public tk2dUIItem recipesBackBtn;
	public tk2dUIItem shopBtn;
    public tk2dUIItem shopBackBtn;
    public tk2dUIItem showSettingsBtn;
    public tk2dUIItem closeSettingsBtn;
    public GameObject recipesMenu;
	public GameObject selectPackMenu;
	public GameObject levelMapMenu;
    public GameObject shopMenu;
    public GameObject mainMenu;
    public GameObject settingsMenu;
	public tk2dUIItem[] packBtns;
    public TweenedObject shopMenuTweener;
    public TweenedObject recipesMenuTweener;
    public TweenedObject selectPackMenuTweener;
    public TweenedObject levelMapMenuTweener;
    public TweenedObject settingsMenuTweener;

    private int _curState = 0;
	private LevelMapController _levelMapController;
	private ShopMenu _shopMenuController;
    private Vector3 _defaultPackMenuPosition;
    private bool _shopInGame;

    private GameManager _gameManager;
    public GameManager gameManager
    {
        get
        {
            if (_gameManager == null)
                _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            return _gameManager;
        }
    }

    private PluginsProxy _plugins;
    public PluginsProxy plugins
    {
        get
        {
            if (_plugins == null)
            {
                _plugins = GameObject.Find("PluginsController").GetComponent<PluginsProxy>();
            }

            return _plugins;
        }
    }

    private SoundController _soundController;
    public SoundController soundController
    {
        get
        {
            if (_soundController == null)
                _soundController = GameObject.Find("SoundController").GetComponent<SoundController>();
            return _soundController;
        }
    }

	void Start() 
	{
		_shopMenuController = shopMenu.GetComponent<ShopMenu>();
        _shopMenuController.init();
        shopMenu.SetActive(false);
        recipesMenu.SetActive(false);
        levelMapMenu.SetActive(false);
        settingsMenu.SetActive(false);
        _defaultPackMenuPosition = selectPackMenu.transform.position;
        mainMenu.transform.position = new Vector3(0, 0, 10);
        _curState = STATE_MAIN;

		recipesBtn.OnClick += showRecipes;
		shopBtn.OnClick += _showShop;
        shopBackBtn.OnClick += _closeShop;
        recipesBackBtn.OnClick += recipesBack;
        showSettingsBtn.OnClick += showSettings;
        closeSettingsBtn.OnClick += closeSettings;
		_levelMapController = levelMapMenu.GetComponent<LevelMapController>();
        _levelMapController.init();
        for (int i = 0; i < packBtns.Length; i++)
		{
			packBtns[i].OnClickUIItem += showLevelMap;
		}
	}

    void showSettings()
    {
        if (anyTweenerActive())
            return;

        soundController.playSound(soundController.tweenMenu);
        settingsMenuTweener.tween(1, SETTINGS_TWEEN_DURATION, false, 0);
        _curState = STATE_SETTINGS;
    }

    void closeSettings()
    {
        if (anyTweenerActive())
            return;
        soundController.playSound(soundController.tweenMenu);
        settingsMenuTweener.tween(0, TWEEN_DURATION, true);
        _curState = STATE_MAIN;
    }

    void closeGame()
    {
        plugins.logEvent("quitByBackBtn");
        Application.Quit();
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape)) 
        {
            if (_curState == STATE_SHOP)
                _closeShop();
            else if (_curState == STATE_LVEL_MAP || _curState == STATE_PACKS)
                recipesBack();
            else if (_curState == STATE_SETTINGS)
                closeSettings();
            else if (_curState == STATE_MAIN)
                closeGame();
        }

        if (Input.GetKeyUp(KeyCode.Menu)) 
        {
            if (_curState == STATE_MAIN)
                showSettings();
        }
    }

    bool anyTweenerActive()
    {
        return levelMapMenuTweener.inProgress
             || recipesMenuTweener.inProgress
             || shopMenuTweener.inProgress
             || settingsMenuTweener.inProgress
             || selectPackMenuTweener.inProgress;
    }

    public void showShopInGame()
    {
        _shopInGame = true;
        plugins.openStore();
        plugins.logEvent("pShop");
        shopMenu.gameObject.SetActive(true);
        shopMenu.GetComponent<PositionedSprite>().calc();
        _shopMenuController.show(3);
        _curState = STATE_SHOP;
    }

    void _showShop()
    {
        if (anyTweenerActive())
            return;
        _shopInGame = false;
        plugins.openStore();
        plugins.logEvent("pShop");
        soundController.playSound(soundController.tweenMenu);
        shopMenuTweener.tween(1, TWEEN_DURATION, false, 0);
        _shopMenuController.show(0);
        _curState = STATE_SHOP;
    }

    void _closeShop()
    {
        if (anyTweenerActive())
            return;
        if (_shopInGame)
        {
            gameManager.closeShop();
        }
        else
        {
            plugins.logEvent("pMain");
            plugins.closeStore();
            soundController.playSound(soundController.tweenMenu);
            shopMenuTweener.tween(0, TWEEN_DURATION, true);
            _curState = STATE_MAIN;
            settingsMenu.SetActive(false);
        }
    }

    void showRecipes()
    {
        if (anyTweenerActive())
            return;
        soundController.playSound(soundController.tweenMenu);
        if (selectPackMenu.activeSelf == false)
        {
            selectPackMenu.SetActive(true);
            selectPackMenu.transform.position = _defaultPackMenuPosition;
        }
        recipesMenuTweener.tween(1, TWEEN_DURATION, false, 0);
        _curState = STATE_PACKS;
        plugins.logEvent("pRecipes");
    }

    void closeRecipes()
    {
        if (anyTweenerActive())
            return;
        recipesMenuTweener.tween(0, TWEEN_DURATION, true);
        _curState = STATE_MAIN;
        plugins.logEvent("pMain");
        settingsMenu.SetActive(false);
    }

    void recipesBack()
    {
        if (anyTweenerActive())
            return;
        soundController.playSound(soundController.tweenMenu);
        if (levelMapMenu.activeSelf)
        {
            levelMapMenuTweener.tween(0, TWEEN_DURATION, true);
            selectPackMenuTweener.tween(1, TWEEN_DURATION, false, 0);
            _curState = STATE_PACKS;
            plugins.logEvent("pRecipes");
        }
        else
            closeRecipes();
    }

    public void showMainMenu()
    {
        _curState = STATE_MAIN;
        settingsMenu.SetActive(false);
        shopMenu.SetActive(false);
        recipesMenu.SetActive(false);
        plugins.logEvent("pMain");
    }

	void showLevelMap(tk2dUIItem packBtn)
	{
        if (anyTweenerActive())
            return;
        soundController.playSound(soundController.tweenMenu);
		for (int i = 0; i < packBtns.Length; i++)
		{
			if (packBtns[i] == packBtn)
			{
				Game.PACK_NUMBER = i + 1;
				break;
			}
		}

        levelMapMenuTweener.tween(1, TWEEN_DURATION, false, 0);
        selectPackMenuTweener.tween(0, TWEEN_DURATION, true);
        _levelMapController.updateInfo();
        _curState = STATE_LVEL_MAP;
        plugins.logEvent("pPack" + Game.PACK_NUMBER);
	}

	

	public void startLevel(int levelNumber)
    {
        soundController.playSound(soundController.click);
		levelMapMenu.SetActive(false);
		Game.GAME_TYPE = Game.GAME_TYPE_RECIPE;
		Game.LEVEL_NUMBER = levelNumber;
        gameManager.startGame();
	}

}

﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class HUDRecipeFruits : MonoBehaviour 
{

    public TweenedObject tweener;
    public TweenedObject fruitsBgTweener;
    public HUDRecipeFruit[] fruits;
    public tk2dSprite fruitsBg;

    private int[][] fruitXPositionsIphone4 = new int[][] 
    {
        new int[] {-1},
        new int[] {-11, 9},
        new int[] {-21, -1, 19}
    };

    private int[][] fruitXPositionsIpad = new int[][] 
    {
        new int[] {-1},
        new int[] {-11, 9},
        new int[] {-21, -1, 19}
    };

    private int[][] fruitXPositionsIphone5 = new int[][] 
    {
        new int[] {-1},
        new int[] {-10, 8},
        new int[] {-19, -1, 17}
    };

    private int[][] fruitXPositions;

    private bool[] _completed;
    private int _activeCnt;

    void Start()
    {
        for (int i = 0; i < fruits.Length; i++)
        {
            fruits[i].init(this, i);
        }

        switch (MultiResolution.aspectRatioIndex) 
        {
            case 0:
                fruitXPositions = fruitXPositionsIphone4;
                break;
            case 1:
                fruitXPositions = fruitXPositionsIpad;
                break;
            default:
                fruitXPositions = fruitXPositionsIphone5;
                break;
        }

        _completed = new bool[fruits.Length];
    }

    public void fruitComplete(int index)
    {
        _completed[index] = true;
        fruits[index].gameObject.SetActive(false);
        _activeCnt--;
        if (_activeCnt == 0)
        {
            fruitsBgTweener.tween(0, 0.2f, true, -1);
            return;
        }

        int curIndex = 0;

        for (int i = 0; i < _completed.Length; i++)
        {
            if (_completed[i] == false)
            {
                Vector3 newPosition = fruits[i].gameObject.transform.position;
                newPosition.x = PositionedSprite.getCalcedX(fruitXPositions[_activeCnt - 1][curIndex]);
                HOTween.To(fruits[i].gameObject.transform, 0.2f, "position", newPosition);
                curIndex++;
            }
        }

        fruitsBg.SetSprite("fruitHUDbg_" + _activeCnt.ToString());
    }

    private void _shiftFruits(HUDRecipeFruit fromFruit, HUDRecipeFruit toFruit)
    {
        HOTween.To(fromFruit.gameObject.transform, 0.2f, "position", toFruit.gameObject.transform.position);
    }

    public void resetPosition()
    {
        PositionedSprite containerPositioned = gameObject.GetComponent<PositionedSprite>();
        containerPositioned.currentOrientation = new Vector2(0, 0);
        containerPositioned.calc();
    } 

    public void show(int activeCnt)
    {
        fruitsBg.gameObject.SetActive(true);
        fruitsBg.SetSprite("fruitHUDbg_" + activeCnt.ToString());

        for (int i = 0; i < activeCnt; i++)
        {
            fruits[i].initPosition(fruitXPositions[activeCnt - 1][i]);
        }

        PositionedSprite containerPositioned = gameObject.GetComponent<PositionedSprite>();
        containerPositioned.currentOrientation = new Vector2(0, 10);
        containerPositioned.calc();

        fruitsBg.gameObject.GetComponent<PositionedSprite>().calc();

        _activeCnt = activeCnt;
        gameObject.SetActive(true);

        for (int i = 0; i < fruits.Length; i++)
        {
            if (i < _activeCnt)
            {
                _completed[i] = false;
            }
            else
            {
                _completed[i] = true;
                fruits[i].gameObject.SetActive(false);
            }

        }
    }

    public void tween()
    {
        tweener.tween(1, 0.4f, false, -1, Holoville.HOTween.EaseType.EaseInOutBack);
        fruitsBgTweener.tween(1, 0.2f, false, -1);
    }
}

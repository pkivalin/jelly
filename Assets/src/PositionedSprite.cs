using UnityEngine;
using System.Collections;

public class PositionedSprite : MonoBehaviour {

	public Vector2 iphone;
	public Vector2 ipad;
	public Vector2 iphone5;
	public float z;
	public bool calcOnStart = true;


	// Use this for initialization
    void Awake () 
	{
		if (calcOnStart)
			calc();
	}

	public void init(Vector2 iphone, Vector2 ipad, Vector2 iphone5)
	{
		this.iphone = iphone;
		this.ipad = ipad;
		this.iphone5 = iphone5;
		calc();
	}

    public Vector2 currentOrientation
    {
        set
        {
            switch (MultiResolution.aspectRatioIndex) 
            {
                case 0:
                    iphone = value;
                    break;
                case 1:
                    ipad = value;
                    break;
                default:
                    iphone5 = value;
                    break;
            }
        }
        get
        {
            switch (MultiResolution.aspectRatioIndex) 
            {
                case 0:
                    return iphone;
                case 1:
                    return ipad;
                default:
                    return iphone5;
            }
        }
    }

	public void calc()
	{
		Vector2 position;
		switch (MultiResolution.aspectRatioIndex) 
        {
		case 0:
			position = iphone;
			break;
		case 1:
			position = ipad;
			break;
		default:
			position = iphone5;
			break;
		}

        gameObject.transform.position = getCalcedPosition(new Vector3(position.x, position.y, z));
	}

    static public Vector3 getCalcedPosition(Vector3 position)
    {
        return Camera.main.ScreenToWorldPoint(new Vector3 (getX(position.x), getY(position.y), position.z + 10));
    }

    static public float getCalcedX(float x)
    {
        return Camera.main.ScreenToWorldPoint(new Vector3 (getX(x), 0, 0)).x;
    }

    static public float getCalcedY(float y)
    {
        return Camera.main.ScreenToWorldPoint(new Vector3 (0, getY(y), 0)).y;
    }

    static public float getX(float x)
    {
        return (x * MultiResolution.resolution.x / 200 + MultiResolution.resolution.x / 2);
    }

    static public float getY(float y)
    {
        return (y * MultiResolution.resolution.y / 200 + MultiResolution.resolution.y / 2);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}

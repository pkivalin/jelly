using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class SoundMusicBtnsController : MonoBehaviour {

    public tk2dUIItem musicBtn;
    public tk2dSlicedSprite musicBtnSprite;
    public tk2dUIItem soundBtn;
    public tk2dSlicedSprite soundBtnSprite;

    private SoundController _soundController;
    public SoundController soundController
    {
        get
        {
            if (_soundController == null)
                _soundController = GameObject.Find("SoundController").GetComponent<SoundController>();
            return _soundController;
        }
    }

    void OnEnable()
    {
        musicBtnSprite.SetSprite(UserData.musicIsOn ? "musicOnBtn" : "musicOffBtn");
        soundBtnSprite.SetSprite(UserData.soundIsOn ? "soundOnBtn" : "soundOffBtn");
    }

	// Use this for initialization
	void Start () 
    {
        musicBtn.OnClick += clickMusic;
        soundBtn.OnClick += clickSound;
        switchMusic(UserData.musicIsOn);
        switchSound(UserData.soundIsOn);
	}

    void clickMusic()
    {
        soundController.playSound(soundController.click);
        switchMusic(!UserData.musicIsOn);
    }

    void switchMusic(bool value)
    {
        UserData.musicIsOn = value;
        soundController.musicIsEnabled = value;
        musicBtnSprite.SetSprite(value ? "musicOnBtn" : "musicOffBtn");
    }

    void clickSound()
    {
        switchSound(!UserData.soundIsOn);
        soundController.playSound(soundController.click);
    }

    void switchSound(bool value)
    {
        UserData.soundIsOn = value;
        soundController.soundIsEnabled = value;
        soundBtnSprite.SetSprite(value ? "soundOnBtn" : "soundOffBtn");
    }

}
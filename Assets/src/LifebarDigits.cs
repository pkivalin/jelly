using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Holoville.HOTween;

public class LifebarDigits : MonoBehaviour
{
    public GameObject digit1;
    public GameObject digit2;
    public tk2dSprite digit1Sprite;
    public tk2dSprite digit2Sprite;

    private const int PRECACHED_DIGITS_CNT = 7;
	static private Color _lastColor = Color.white;
	static private Vector3 _newScale = new Vector3(0.4f, 0.4f, 0.4f);
	static private Stack<GameObject> _prefabs;
	static private List<GameObject> _curDigits = new List<GameObject>();
	static private bool _inited = false;
	static private TweenParms _tween1;
	static private TweenParms _tween2;
	static private TweenParms _tween3;
	static private TweenParms _tween4;
	static private float _x = float.MaxValue;
	static private float _oneDigitX;
	private const float DIGIT_Z = -3;

	static void init ()
	{
		_lastColor.a = 0.0f;
		_prefabs = new Stack<GameObject> ();
        GameObject prefab;
        for (int i = 0; i < PRECACHED_DIGITS_CNT; i++)
        {
            prefab = Instantiate(Resources.Load("LifebarDigit", typeof(GameObject))) as GameObject;
            prefab.SetActive(false);
            _prefabs.Push(prefab);
        }

		_tween1 = new TweenParms();
		_tween1.Prop("localScale", new Vector3(1.0f, 1.0f, 1.0f)); 
		_tween1.Ease(EaseType.Linear);

		_tween2 = new TweenParms();
		_tween2.Ease(EaseType.Linear);

		_tween3 = new TweenParms();
		_tween3.Ease(EaseType.Linear);

		_tween4 = new TweenParms();
		_tween4.Prop("color", _lastColor);
		_tween4.Delay(0.5f);
		_tween4.Ease(EaseType.Linear);

		_inited = true;

		switch (MultiResolution.aspectRatioIndex) {
			case 0:
			_x = 93;
			break;
			case 1:
			_x = 90;
			break;
			default:
			_x = 90;
			break;
		}

        _x = Camera.main.ScreenToWorldPoint (new Vector3 (PositionedSprite.getX(_x), 0, 0)).x;
		_oneDigitX = _x - 0.5f;
	}

	static public void gameOver()
	{
		for (int i = 0; i < _curDigits.Count; i++)
		{
			_curDigits[i].SetActive(false);
			_prefabs.Push(_curDigits[i]);
		}

		_curDigits.Clear();
	}

	static public void addDigits(int number, float height)
	{
		if (!_inited)
		{
			init();
		}
		GameObject prefab;
		if (_prefabs.Count == 0)
		{
			prefab = Instantiate(Resources.Load("LifebarDigit", typeof(GameObject))) as GameObject;
            prefab.transform.parent = GameObject.Find("GameScreen").transform;
		}
		else 
		{
			prefab = _prefabs.Pop();
			prefab.SetActive(true);
		}

		LifebarDigits lifebarDigits = prefab.GetComponent<LifebarDigits>();
		lifebarDigits.init(number, height);
		_curDigits.Add(prefab);
	}
	
	private void init(int number, float height)
	{
		string prefix = "r";
		if (number < 0)
		{
			prefix = "g";
			number = -number;
		}
		float containerX;
		if (number >= 10)
		{
			digit1.SetActive(true);
			HOTween.To(digit1Sprite, 0.233f, _tween4);
			digit1Sprite.color = Color.white;
			digit1Sprite.SetSprite(prefix + (number / 10).ToString());
			containerX = _x;
		}
		else
		{
			digit1.SetActive(false);
			containerX = _oneDigitX;
		}

		HOTween.To(digit2Sprite, 0.233f, _tween4);
		digit2Sprite.color = Color.white;
		digit2Sprite.SetSprite(prefix + (number % 10).ToString());

		gameObject.transform.localScale = _newScale;

		gameObject.transform.position = new Vector3(containerX, height, DIGIT_Z);


		_tween2.Prop("position", new Vector3(containerX, height + 0.5f, DIGIT_Z));
		_tween3.Prop("position", new Vector3(containerX, height + 1.5f, DIGIT_Z)); 

		Sequence sequence = new Sequence();
		sequence.Append(HOTween.To(gameObject.transform, 0.167f, _tween1));
		sequence.Append(HOTween.To(gameObject.transform, 0.333f, _tween2));
		sequence.Append(HOTween.To(gameObject.transform, 0.233f, _tween3.OnComplete(removeDigit)));
		sequence.Play();
	}

	private void removeDigit()
	{
		gameObject.SetActive(false);
		_prefabs.Push(gameObject);
		_curDigits.Remove(gameObject);
	}

}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class Buggun : MonoBehaviour 
{
    public tk2dSprite gunSprite;
    private float _timer;
    private float _shootDelay;
    private SoundController _soundController;
    private Vector2 _horizontalForce;
    private Vector2 _verticalForce;
    private bool _toLeft = true;

    private FruitFactory _fruitFactory;

    public void init(FruitFactory fruitFactory)
    {
        _fruitFactory = fruitFactory;
    }

    void Start()
    {
        _soundController = GameObject.Find("SoundController").GetComponent<SoundController>();
    }

    void Update()
    {
        if (GameManager.GAME_RUNNING == false)
            return;

        if (Time.timeScale == 0.0f)
            return;

        _timer += Time.deltaTime;
        if (_timer >= _shootDelay)
        {
            _shoot();
            _timer = 0;
        }
            
    }

    private void _shoot()
    {
        _fruitFactory.addFruitFromPosition(Bug.INDEX, gameObject.transform.position
            , Random.Range(_horizontalForce.x, _horizontalForce.y) * (_toLeft ? -1 : 1)
            , Random.Range(_verticalForce.x, _verticalForce.y));
        _toLeft = !_toLeft;
        _soundController.playSound(_soundController.buggun);
    }

    public void init(XmlNode settings)
    {
        _timer = 0;
        _shootDelay = float.Parse(settings["shootDelay"].InnerText);
        _horizontalForce = new Vector2(float.Parse(settings["force"]["horizontal"]["min"].InnerText)
            , float.Parse(settings["force"]["horizontal"]["max"].InnerText));
        _verticalForce = new Vector2(float.Parse(settings["force"]["vertical"]["min"].InnerText)
            , float.Parse(settings["force"]["vertical"]["max"].InnerText));
    }

}

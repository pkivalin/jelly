using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class MainMenu : MonoBehaviour {

    public tk2dUIItem recipesText;
    public tk2dUIItem survivalText;
    public tk2dUIItem recipesBtn;
	public tk2dUIItem survivalBtn;
	public tk2dUIItem timeBtn;
	public tk2dUIItem facebookBtn;
    public tk2dUIItem twitterBtn;
    public tk2dUIItem achievementsBtn;
    public tk2dUIItem leaderbordsBtn;
    public tk2dUIItem moreGamesBtn;
    public GameObject titleTimeSprite;
    public GameObject lockTimeSprite;

    private GameManager _gameManager;
    public GameManager gameManager
    {
        get
        {
            if (_gameManager == null)
                _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            return _gameManager;
        }
    }

    private SoundController _soundController;
    public SoundController soundController
    {
        get
        {
            if (_soundController == null)
                _soundController = GameObject.Find("SoundController").GetComponent<SoundController>();
            return _soundController;
        }
    }

    private PluginsProxy _plugins;
    public PluginsProxy plugins
    {
        get
        {
            if (_plugins == null)
            {
                _plugins = GameObject.Find("PluginsController").GetComponent<PluginsProxy>();
            }

            return _plugins;
        }
    }

	// Use this for initialization
	void Start () 
    {
		Time.timeScale = 1.0f;
		survivalBtn.OnClick += startSurvivalGame;
		timeBtn.OnClick += startTimeGame;
        facebookBtn.OnClick += goToFacebook;
        twitterBtn.OnClick += goToTwitter;
        achievementsBtn.OnClick += showAchievements;
        leaderbordsBtn.OnClick += showLeaderboard;
        moreGamesBtn.OnClick += showMoreGames;
	}

    void showAchievements()
    {
        soundController.playSound(soundController.click);
        GameManager.gamecenter.showAchievements();
    }

    void showLeaderboard()
    {
        soundController.playSound(soundController.click);
        GameManager.gamecenter.showLeaderboard();
    }

    void _deleteLocks()
    {
        DestroyObject(lockTimeSprite);
        lockTimeSprite = null;
        titleTimeSprite.SetActive(true);
    }

    void showMoreGames()
    {
        soundController.playSound(soundController.click);
        plugins.moreGames();
    }

    void OnEnable()
    {
        if (lockTimeSprite && GameManager.gamecenter.hasAchievement(GamecenterProxy.TIME_MODE_ACHIEVEMENT))
        {
            _deleteLocks();
        }
        else if (GameManager.gamecenter.hasAchievement(GamecenterProxy.TIME_MODE_ACHIEVEMENT) == false)
        {
            titleTimeSprite.SetActive(false);
        }
    }

    #if UNITY_IPHONE

    void goToTwitter() 
    {
        soundController.playSound(soundController.click);
        float startTime;
        startTime = Time.timeSinceLevelLoad;

        //open the twitter app
        Application.OpenURL("twitter://user?screen_name=KivalinGames");

        if (Time.timeSinceLevelLoad - startTime <= 1f) //fail. Open safari.
        {
            Application.OpenURL("http://twitter.com/KivalinGames");
        }

        plugins.logEvent("twitter");
    }

    void goToFacebook()
    {
        soundController.playSound(soundController.click);
        float startTime;
        startTime = Time.timeSinceLevelLoad;

        //open the facebook app
        Application.OpenURL("fb://profile/473333149452132");

        if (Time.timeSinceLevelLoad - startTime <= 1f) //fail. Open safari.
        {
            Application.OpenURL("http://www.facebook.com/jellyfruitgame");
        }

        plugins.logEvent("facebook");
    }

    #else

    void goToTwitter() 
    {
        soundController.playSound(soundController.click);
        Application.OpenURL("http://twitter.com/KivalinGames");
    }

    void goToFacebook()
    {
        soundController.playSound(soundController.click);
        Application.OpenURL("http://www.facebook.com/jellyfruitgame");
    }

    #endif
	


	void startSurvivalGame ()
	{
        soundController.playSound(soundController.click);
		Game.GAME_TYPE = Game.GAME_TYPE_SURVIVAL;
        gameManager.startGame();
	}

	void startTimeGame ()
	{
        if (GameManager.gamecenter.hasAchievement(GamecenterProxy.TIME_MODE_ACHIEVEMENT) == false)
        {
            return;
        }
        soundController.playSound(soundController.click);
		Game.GAME_TYPE = Game.GAME_TYPE_TIME;
        gameManager.startGame();
	}
	
}
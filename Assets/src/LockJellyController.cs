﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class LockJellyController : MonoBehaviour 
{
    public tk2dUIItem lockBtn;
    public tk2dSprite lockSprite;
    public tk2dSprite lockSpriteBg;
    public tk2dSprite jellySprite;
    public LockJellyFreezer jellyFreezer;
    public tk2dSpriteAnimator unfreezeAnimation;
    public tk2dSpriteAnimator tapTapAnimation;
    private Color _neededColor = Color.white;
    private Color _neededColorBg;
    private XmlNode _settings;
    private float _timer;
    private float _bonusDelay;
    private float _timeScale;
    private int _needClicks;
    private float _alphaDelta;
    private float _alphaDeltaBg;
    private float _scaleDelay;
    private bool _needIncreaseTimeScale;
    private bool _needDecreaseTimeScale;
    private bool _activated;
    private int _curClip;
    private SoundController _soundController;

    void Start()
    {
        lockBtn.OnClick += _btnClick;
        _neededColorBg = new Color(90.0f / 255.0f, 189.0f / 255.0f, 214.0f / 255.0f);
    }

    void Update()
    {
        if (GameManager.GAME_RUNNING == false)
            return;

        if (Time.timeScale == 0.0f)
            return;

        if (_activated == false) 
        {
            _timer += Time.deltaTime;

            if (_timer >= _bonusDelay)
            {
                jellyFreezer.run();
                _timer = 0;
            }
        }
        else
        {
            tapTapAnimation.ClipFps = 15.0f / Time.timeScale;
        }

        if (_needIncreaseTimeScale)
        {
            if (_scaleDelay == 0)
                Time.timeScale = 1;
            else
                Time.timeScale = Mathf.Min(1, Time.timeScale + (1 - _timeScale) * Time.deltaTime / Time.timeScale /_scaleDelay);
            if (Time.timeScale == 1)
            {
                _needIncreaseTimeScale = false;
            }
        }
        else if (_needDecreaseTimeScale)
        {
            if (_scaleDelay == 0)
                Time.timeScale = _timeScale;
            else
                Time.timeScale = Mathf.Max(_timeScale, Time.timeScale - (1 - _timeScale) * Time.deltaTime / Time.timeScale /_scaleDelay);
            if (Time.timeScale == _timeScale)
            {
                _needDecreaseTimeScale = false;
            }
        }
    }

    public void init(XmlNode settings, Game game)
    {
        _settings = settings;
        _timer = 0;
        _bonusDelay = float.Parse(_settings["interval"].InnerText);
        _timeScale = float.Parse(_settings["timeScale"].InnerText);
        _scaleDelay = float.Parse(_settings["scaleDelay"].InnerText);
        lockBtn.gameObject.SetActive(false);
        _neededColor.a = 0.0f;
        lockSprite.color = _neededColor;
        jellySprite.color = _neededColor;
        _neededColorBg.a = 0.0f;
        lockSpriteBg.color = _neededColorBg;
        _activated = false;
        _needIncreaseTimeScale = false;
        _needDecreaseTimeScale = false;

        game.initGameObjectScale(jellySprite.gameObject);

        unfreezeAnimation.gameObject.SetActive(false);
        tapTapAnimation.gameObject.SetActive(false);

        _soundController = GameObject.Find("SoundController").GetComponent<SoundController>();

        jellyFreezer.init(game, float.Parse(_settings["bonusFallTime"].InnerText), int.Parse(_settings["clickCnt"].InnerText));
        jellyFreezer.gameObject.SetActive(false);
    }

    public void activate(int clickCnt)
    {
        _soundController.playSound(_soundController.bonusFreeze);
        _activated = true;
        _needClicks = clickCnt;
        _alphaDelta = 1.0f / clickCnt;
        _alphaDeltaBg = 0.2f / clickCnt;
        _neededColor.a = 1;
        lockSprite.color = _neededColor;
        jellySprite.color = _neededColor;
        _neededColorBg.a = 0.2f;
        lockSpriteBg.color = _neededColorBg;
        lockBtn.gameObject.SetActive(true);
        _needDecreaseTimeScale = true;
        _needIncreaseTimeScale = false;
        _curClip = 0;
        tapTapAnimation.gameObject.SetActive(true);
        tapTapAnimation.Play();
    }

    public void deactivate()
    {
        _activated = false;
        _timer = 0;
        lockBtn.gameObject.SetActive(false);
        _neededColor.a = 0.0f;
        lockSprite.color = _neededColor;
        jellySprite.color = _neededColor;

        _neededColorBg.a = 0.0f;
        lockSpriteBg.color = _neededColorBg;
        _needIncreaseTimeScale = true;
        _needDecreaseTimeScale = false;
        unfreezeAnimation.gameObject.SetActive(false);
        tapTapAnimation.Stop();
        tapTapAnimation.gameObject.SetActive(false);
    }

    private void _playUnfreezeAnimation()
    {
        _curClip++;
        if (_curClip <= 4)
        {
            unfreezeAnimation.gameObject.SetActive(true);
            unfreezeAnimation.Play(_curClip.ToString());
        }
        else
        {
            unfreezeAnimation.gameObject.SetActive(false);
        }
    }

    private void _btnClick()
    {
        _soundController.playSound(_soundController.iceCrack);
        _playUnfreezeAnimation();

        _needClicks--;
        Color curColor = lockSprite.color;
        curColor.a -= _alphaDelta;
        lockSprite.color = curColor;

        if (_needClicks == 1)
        {
            curColor.a = 0.0f;
            jellySprite.color = curColor;
        }

        curColor = lockSpriteBg.color;
        curColor.a -= _alphaDeltaBg;
        lockSpriteBg.color = curColor;


        if (_needClicks <= 0)
        {
            deactivate();
        }
    }
}

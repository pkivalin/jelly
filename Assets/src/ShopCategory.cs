﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class ShopCategory : MonoBehaviour 
{
	public int categoryId;
    public tk2dTextMesh moneyTF;
	public tk2dUIScrollableArea scrollableArea;
    public string title;

	private List<Upgrade> _upgradeControllers;

    public void init() 
	{
		float w = 0;
		float y = 0;
		XmlNode categoryXML = Shop.getCategory(categoryId);
        title = categoryXML["title"].InnerText;

		tk2dUILayout layout = null;
		_upgradeControllers = new List<Upgrade>();

		foreach (XmlNode levelXml in categoryXML["upgrades"])
		{
			layout = Instantiate(Resources.Load("Upgrade", typeof(tk2dUILayout))) as tk2dUILayout;
            layout.transform.parent = GameObject.Find("MenuScreen").transform;

			if (w == 0)
			{
				w = (layout.GetMaxBounds() - layout.GetMinBounds()).y;
				y = w / 4;
			}

			layout.transform.parent = scrollableArea.contentContainer.transform;
			layout.transform.localPosition = new Vector3(0, -y, 0);
			scrollableArea.ContentLength = y + (layout.GetMaxBounds() - layout.GetMinBounds()).y;

			Upgrade upgrade = layout.GetComponent<Upgrade>();
			_upgradeControllers.Add(upgrade);
			upgrade.init(levelXml, this);

			y += w;
		}

		scrollableArea.ContentLength = y;
		// хуйня полная магии
		//scrollableArea.VisibleAreaLength = scrollableArea.ContentLength / (numToAdd + 1) * 3.2f;
	}

	public void updateInfo()
	{
		if (!gameObject.activeSelf)
			return;

		for (int i = 0; i < _upgradeControllers.Count; i++)
		{
			_upgradeControllers[i].updateInfo();
		}

        moneyTF.text = UserData.money.ToString();
        moneyTF.Commit();
	}
}

﻿using UnityEngine;
using System.Collections;

public class FruitArrow : MonoBehaviour {

	public tk2dSprite icon;
	public tk2dTextMesh amountTF;

	public void init(RecipeFruit recipeFruit)
	{
		icon.SetSprite(recipeFruit.fruitName);
		amountTF.text = "x" + recipeFruit.total.ToString();
		amountTF.Commit();
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Xml;
using Holoville.HOTween;

public class LifebarController : MonoBehaviour {

    private const float MIN_LIFEBAR_SCALE = 0.6f;
    private const float MAX_LIFEBAR_SCALE = 1.1f;
    private const int MIN_LIFEBAR_CAPACITY = 50;
    private const int MAX_LIFEBAR_CAPACITY = 100;
    private const string STAR_FULL = "star_ld_full";
    private const string STAR_EMPTY = "star_ld_empty";

    public tk2dSprite barMask;
    public tk2dSprite barTempMask;
    public tk2dSprite[] stars;

    private float _timer;
    private float _tempMaskTimer;
    private int _lastValue;
    private float _feelDelay;
    private float _feelSpeed;
    private Color _green = new Color(17.0f / 255.0f, 189.0f / 255.0f, 15.0f / 255.0f);
    private Color _yellow = new Color(255.0f / 255.0f, 218.0f / 255.0f, 7.0f / 255.0f);

    private int _curValue;
    private int _capacity;
    private float _curDelay;
    private int _curBonus;
    private float _upgradeBonusTime;
    private int _upgradeBonusValue;
    private float _timeModePenaltyTime;
    private int _timeModePenaltyValue;
    private bool _inited = false;
    private Tweener _tweener;
    private SoundController _soundController;

    // Use this for initialization
    void Start () 
    {
    }

    public void init()
    {
        if (!_inited)
        {
            _inited = true;
            XmlNode settings = Settings.getLifebar ();
            _capacity = int.Parse (settings ["capacity"].InnerText);
            _timeModePenaltyTime = float.Parse (settings ["timeModePenaltyTime"].InnerText);
            _timeModePenaltyValue = int.Parse (settings ["timeModePenaltyValue"].InnerText);
            _feelDelay = float.Parse (settings ["feelDelay"].InnerText);
            _feelSpeed = float.Parse (settings ["feelSpeed"].InnerText);
            _soundController = GameObject.Find("SoundController").GetComponent<SoundController>();
        }

        if (Game.GAME_TYPE != Game.GAME_TYPE_TIME && UserData.getUpgradeLevel(Upgrade.THERMOMETER_CAPACITY) != 0) 
        {
            _capacity = (int)UserData.getUpgradeValue(Upgrade.THERMOMETER_CAPACITY);
        }

        _upgradeBonusValue = UserData.getUpgradeLevel(Upgrade.THERMOMETER_REPAIR) * 2; // сжигаем 2 за 1 уровень апгрейда, 4 за второй
        _upgradeBonusTime = UserData.getUpgradeValue(Upgrade.THERMOMETER_REPAIR);
        if (_upgradeBonusTime <= 0)
            _upgradeBonusTime = int.MaxValue;

        if (Game.GAME_TYPE == Game.GAME_TYPE_TIME) 
        {
            _curDelay = _timeModePenaltyTime;
            _curBonus = _timeModePenaltyValue;
        }
        else
        {
            _curDelay = _upgradeBonusTime;
            _curBonus = -_upgradeBonusValue;
        }

        _initStars();

        _tempMaskTimer = float.MinValue;
        _curValue = 0;
        _lastValue = 0;
        _timer = 0;
        _tempMaskTimer = 0;
        barMask.scale = Vector3.one;
        barTempMask.scale = Vector3.one;
        if (_tweener != null)
        {
            _tweener.enabled = false;
            _tweener = null;
        }

        _setScale();
    }

    void _setScale()
    {
        float scaleY = (float)(_capacity - MIN_LIFEBAR_CAPACITY) / (float)(MAX_LIFEBAR_CAPACITY - MIN_LIFEBAR_CAPACITY);
        scaleY = MIN_LIFEBAR_SCALE + (MAX_LIFEBAR_SCALE - MIN_LIFEBAR_SCALE) * scaleY;
        float scaleX = (scaleY - MIN_LIFEBAR_SCALE) / 2 + MIN_LIFEBAR_SCALE;
        gameObject.transform.localScale = new Vector3(scaleX, scaleY, 1.0f);
        Vector3 starScale = new Vector3 (1, scaleX / scaleY, 1);
        for (int i = 0; i < stars.Length; i++)
        {
            stars[i].scale = starScale;
        }
    }

    void _initStars()
    {
        if (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE)
        {
            for (int i = 0; i < stars.Length; i++)
            {
                stars[i].gameObject.SetActive(true);
                stars[i].SetSprite(STAR_FULL);
            }
        }
        else
        {
            for (int i = 0; i < stars.Length; i++)
            {
                stars[i].gameObject.SetActive(false);
            }
        }
    }

    // Update is called once per frame
    void Update () 
    {
        if (Time.timeScale == 0.0f)
            return;
        _timer += Time.deltaTime;
        if (_timer >= _curDelay)
        {
            int left = _capacity - _curValue;
            if (_curBonus > 0 && left < Settings.timeModeClock)
                _soundController.playSound(_soundController.clockLifebar);

            curValue += _curBonus;
            _timer = 0;
        }

        _tempMaskTimer += Time.deltaTime;
        if (_tempMaskTimer >= _feelDelay)
        {
            tk2dSprite tweenFrom;
            tk2dSprite tweenTo;
            if (_curValue < _lastValue)
            {
                tweenFrom = barMask;
                tweenTo = barTempMask;
            }
            else
            {
                tweenFrom = barTempMask;
                tweenTo = barMask;
            }
            TweenParms tweenParams = new TweenParms();
            float duration = Mathf.Abs((barMask.scale.y - barTempMask.scale.y) / _feelSpeed);
            tweenParams.Prop("scale", tweenTo.scale); 
            _tweener = HOTween.To(tweenFrom, duration, tweenParams);
            _tempMaskTimer = float.MinValue;
            _lastValue = _curValue;
        }
    }

    public bool isFull
    {
        get
        {
            return _curValue >= _capacity;// && barTempMask.scale.y <= 0.01;
        }
    }

    private bool _hideForTutorial;
    public bool hideForTutorial
    {
        set
        {
            _hideForTutorial = value;
            gameObject.SetActive(!value);
        }
    }

    public int curValue
    {
        get
        {
            return _curValue;
        }
        set
        {
            if (_curValue == value || GameManager.GAME_RUNNING == false || _hideForTutorial) return;
            int prevValue = _curValue;
            _curValue = Mathf.Min(value, _capacity);
            _curValue = Mathf.Max(_curValue, 0);
            if (_curValue == prevValue) return;

            float height = barTempMask.transform.position.y - barTempMask.scale.y * 7 * gameObject.transform.localScale.y;
            LifebarDigits.addDigits(value - prevValue, height);

            _tempMaskTimer = 0.0f;
            if (_curValue < _lastValue)
            {
                barTempMask.scale = new Vector3(1, 1 - (float)_curValue / (float)_capacity, 1);
                barTempMask.color = _green;
            }
            else
            {
                barMask.scale = new Vector3(1, 1 - (float)_curValue / (float)_capacity, 1);
                barTempMask.color = _yellow;
            }

            _updateStars();
        }
    }

    void _updateStars()
    {
        if (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE)
        {
            int starsCnt = getStarsCnt();
            for (int i = 0; i < stars.Length; i++)
            {
                if (i < starsCnt)
                    stars[i].SetSprite(STAR_FULL);
                else
                    stars[i].SetSprite(STAR_EMPTY);
            }
        }
    }

    public int getStarsCnt()
    {
        if (_curValue == _capacity)
            return 0;
        for (int i = 2; i >= 0; i--)
        {
            if (percent <= Settings.stars[i])
            {
                return i + 1;
            }
        }
        return 0;
    }

    public int percent
    {
        get
        {
            return 100 * _curValue / _capacity;
        }
    }
}

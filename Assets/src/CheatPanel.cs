using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class CheatPanel : MonoBehaviour {

    public tk2dUIItem addMoneyByn;
    public tk2dUIItem openLevelsBtn;
    public tk2dUIItem resetBtn;
    private SoundController _soundController;
    public SoundController soundController
    {
        get
        {
            if (_soundController == null)
                _soundController = GameObject.Find("SoundController").GetComponent<SoundController>();
            return _soundController;
        }
    }

    private PluginsProxy _plugins;
    public PluginsProxy plugins
    {
        get
        {
            if (_plugins == null)
            {
                _plugins = GameObject.Find("PluginsController").GetComponent<PluginsProxy>();
            }

            return _plugins;
        }
    }

	// Use this for initialization
	void Start () 
    {
        if (Settings.cheatsIsEnabled == false)
        {
            gameObject.SetActive(false);
            return;
        }
        addMoneyByn.OnClick += addMoney;
        openLevelsBtn.OnClick += openLevels;
        resetBtn.OnClick += reset;
	}

    void openLevels() 
    {
        soundController.playSound(soundController.click);
        for (int i = 1; i <= Game.PACKS_CNT; i++)
        {
            UserData.setMaxLevel(i, Game.LEVELS_IN_PACK - 1);
        }

        UserData.needSurvivalTutorial = false;
        plugins.endLevel();
    }
   
    void addMoney() 
	{
        soundController.playSound(soundController.click);
        UserData.money += 50000;
	}

    void reset()
	{
        soundController.playSound(soundController.click);
        UserData.restart();
        GameManager.gamecenter.resetAchievements();
	}
	
}
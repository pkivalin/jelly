﻿using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour {

    public HUDInfo[] infos;
    private HUDInfo _score;
    private HUDInfo _money;
    private HUDInfo _mistakes;

	public void init()
	{
        _score = null;
        _money = null;
        _mistakes = null;

        if (Game.GAME_TYPE == Game.GAME_TYPE_SURVIVAL)
        {
            infos[0].gameObject.SetActive(true);
            infos[1].gameObject.SetActive(true);
            infos[2].gameObject.SetActive(false);
            _score = infos[0];
            _money = infos[1];
        }
        else if (Game.GAME_TYPE == Game.GAME_TYPE_TIME)
        {
            infos[0].gameObject.SetActive(true);
            infos[1].gameObject.SetActive(true);
            infos[2].gameObject.SetActive(false);
            _score = infos[0];
            _mistakes = infos[1];
        }
        else
        {
            infos[0].gameObject.SetActive(false);
            infos[1].gameObject.SetActive(false);
            infos[2].gameObject.SetActive(false);
        }

        if (_score != null)
            _score.icon.SetSprite("smallScore");
        if (_money != null)
            _money.icon.SetSprite("smallCoin");
        if (_mistakes != null)
            _mistakes.icon.SetSprite("smallMistakes");
	}

    public int score
    {
        set
        {
            if (_score != null)
                _score.count = value;
        }
    }

    public int money
    {
        set
        {
            if (_money != null)
                _money.count = value;
        }
    }

    public int mistakes
    {
        set
        {
            if (_mistakes != null)
                _mistakes.count = value;
        }
    }

}

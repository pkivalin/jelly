﻿using UnityEngine;
using System.Collections;

public class LooseScreenInfo : MonoBehaviour 
{

	public tk2dSprite icon;
    public tk2dTextMesh countTF;
    public tk2dTextMesh titleTF;

    public int count
    {
        set
        {
            countTF.text = value.ToString();
            countTF.Commit();
        }
    }

    public string title
    {
        set
        {
            titleTF.text = value;
            titleTF.Commit();
        }
    }

}

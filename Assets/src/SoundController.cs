﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {

    public AudioClip click;
	public AudioClip hitFruit;
	public AudioClip hitBug;
	public AudioClip hitBonus;
	public AudioClip jellyFruit;
	public AudioClip jellyBug;
	public AudioClip jellyBonus;
	public AudioClip missFruit;
	public AudioClip collectOneFruit;
	public AudioClip newLevel;
	public AudioClip win;
	public AudioClip loose;
	public AudioClip clockLifebar;
	public AudioClip touchJelly;
	public AudioClip untouchJelly;
	public AudioClip bonusAddFruits;
	public AudioClip bonusAddLive;
	public AudioClip bonusFreeze;
    public AudioClip bonusBorders;
    public AudioClip iceCrack;
    public AudioClip objectExplosion;
    public AudioClip tweenMenu;
    public AudioClip buggun;
    public AudioClip _mainTheme;
    public AudioClip _menuTheme;

    private AudioSource _themeAudio;

	private float _missFruitTimer;
	private float _returnVolumeTimer;
    private int needMissedFruitSounds = 0;
	private System.DateTime _muteBeginTime;

    public bool soundIsEnabled;

    private bool _musicIsEnabled;
    public bool musicIsEnabled
    {
        set
        {
            _musicIsEnabled = value;
            if (_musicIsEnabled)
                _themeAudio.volume = 1.0f;
            else
                _themeAudio.volume = 0.0f;
        }
    }

	public void restart()
	{
		_returnMainVolume();
		needMissedFruitSounds = 0;
	}

    void Awake() 
    {
        _themeAudio = Instantiate(Resources.Load("Audio", typeof(AudioSource))) as AudioSource;
        _themeAudio.loop = true;
    }

    public void playWinLoseSound(AudioClip audioClip)
    {
        if (!_musicIsEnabled)
            return;

        _muteBeginTime = System.DateTime.Now;
        _themeAudio.volume = Settings.muteMainTheme;
        _returnVolumeTimer = audioClip.length * 1000;

        audio.PlayOneShot(audioClip);
    }

	public void playSound(AudioClip audioClip)
	{
        if (!soundIsEnabled)
            return;
		if (audioClip == missFruit)
		{
			if (_missFruitTimer < 0.3)
			{
				return;
			}
            else if (_missFruitTimer < missFruit.length && needMissedFruitSounds < 3)
			{
				needMissedFruitSounds++;
				return;
			}
			else
			{
				_missFruitTimer = 0;
			}
		} 

		audio.PlayOneShot(audioClip);
	}

	void _returnMainVolume()
	{
        if (!_musicIsEnabled)
            return;
		_muteBeginTime = System.DateTime.MinValue;
		_returnVolumeTimer = int.MaxValue;
        _themeAudio.volume = 1.0f;
	}

	void Update()
	{
		if (_returnVolumeTimer <= (System.DateTime.Now - _muteBeginTime).TotalMilliseconds)
			_returnMainVolume();

		_missFruitTimer += Time.deltaTime;
		if (_missFruitTimer > missFruit.length && needMissedFruitSounds > 0)
		{
			playSound(missFruit);
			needMissedFruitSounds--;
		}
    }

    public void playMainTheme()
    {
        _themeAudio.Stop();
        _themeAudio.clip = _mainTheme;
        _themeAudio.Play();
    }

    public void playMenuTheme()
    {
        _themeAudio.Stop();
        _themeAudio.clip = _menuTheme;
        _themeAudio.Play();
    }
}

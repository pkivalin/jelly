using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class Level
{
    public const int MECHANIC_BORDERS = 1;
    public const int MECHANIC_FREEZE = 2;
    public const int MECHANIC_WIND = 3;
    public const int MECHANIC_BUGGUN = 4;
    public const int MECHANIC_FROZEN_FRUIT = 5;

	private int _slots;
	private int _bugChance;
	private int _bugMaxInterval;
	private int _recipeFruitMaxInterval;
	private float _interval;
	private Vector2 _fruitInterval;
	private int _minBugCnt;
	private int _maxBugCnt;
	private float _gravity;
	private Vector2 _horizontalForce;
	private Vector2 _verticalForce;
	private string[] _allowedFruits;
	private Dictionary<string, RecipeFruit> _neededFruits;
    private int _mechanicType;
    private XmlNode _mechanicSettings;
    private int _scoreBonus;
    private int _moneyBonus;
    private int _maxRecipeWrongFruitInterval;

	public Level (XmlNode settings)
	{
        try
        {
            _maxRecipeWrongFruitInterval = int.Parse(settings["fruits"]["maxRecipeWrongFruitInterval"].InnerText);
        }
        catch
        {
            _maxRecipeWrongFruitInterval = Settings.maxRecipeWrongFruitInterval;
        }

		_slots = int.Parse(settings["slots"].InnerText);
		_bugChance = int.Parse(settings["bugs"]["chance"].InnerText);
		_minBugCnt = int.Parse(settings["bugs"]["min"].InnerText);
		_maxBugCnt = int.Parse(settings["bugs"]["max"].InnerText);
		_bugMaxInterval = int.Parse(settings["bugs"]["maxInterval"].InnerText);
		_interval = float.Parse(settings["interval"].InnerText);
		_gravity = float.Parse(settings["gravity"].InnerText);

		_horizontalForce = new Vector2(float.Parse(settings["force"]["horizontal"]["min"].InnerText)
		                               , float.Parse(settings["force"]["horizontal"]["max"].InnerText));
		_verticalForce = new Vector2(float.Parse(settings["force"]["vertical"]["min"].InnerText)
		                             , float.Parse(settings["force"]["vertical"]["max"].InnerText));

		_fruitInterval = new Vector2(float.Parse(settings["fruitInterval"]["min"].InnerText)
		                             , float.Parse(settings["fruitInterval"]["max"].InnerText));

		if (settings["fruits"] != null)
		{
			_sortFruitOrder(settings["fruits"]["allowed"].InnerText.Trim().Split(new Char [] {';'}));
			completeFruitCnt = int.MaxValue;
			_neededFruits = new Dictionary<string, RecipeFruit>();
			RecipeFruit recipeFruit;

			foreach (XmlNode node in settings["fruits"]["needed"].ChildNodes) 
			{
				recipeFruit = new RecipeFruit(node);
				_neededFruits.Add(recipeFruit.fruitName, recipeFruit);
			}

			_recipeFruitMaxInterval = int.Parse(settings["fruits"]["recipeMaxInterval"].InnerText);
		}
		else
		{
			completeFruitCnt = int.Parse(settings["completeFruitCnt"].InnerText);
			_allowedFruits = Fruit.TYPES;
		}

        if (settings["mechanic"] != null)
		{
            _mechanicSettings = settings["mechanic"];
			_mechanicType = int.Parse(settings["mechanic"]["type"].InnerText);
		}

        if (settings["scoreBonus"] != null)
        {
            _scoreBonus = int.Parse(settings["scoreBonus"].InnerText);
        }
        else
        {
            _scoreBonus = 0;
        }

        if (settings["moneyBonus"] != null)
        {
            _moneyBonus = int.Parse(settings["moneyBonus"].InnerText);
        }
        else
        {
            _moneyBonus = 0;
        }
	}

    public int maxRecipeWrongFruitInterval
    {
        get
        {
            return _maxRecipeWrongFruitInterval;
        }
    }

	private void _sortFruitOrder(string[] fruits)
	{
        List<string> allowedFruits = new List<string>();
		int k = 0;
		for (int i = 0; i < Fruit.TYPES.Length; i++)
		{
			for (int j = 0; j < fruits.Length; j++)
			{
				if (Fruit.TYPES[i] == fruits[j])
				{
                    allowedFruits.Add(fruits[j]);
                    k++;
					break;
				}
		    }
		}

        _allowedFruits = allowedFruits.ToArray();
	}

	public string[] allowedFruits
	{
		get 
		{
			return _allowedFruits;
		}
	}

	public Dictionary<string, RecipeFruit> neededFruits
	{
		get 
		{
			return _neededFruits;
		}
	}

	public Vector2 horizontalForce
	{
		get 
		{
			return _horizontalForce;
		}
	}

	public Vector2 verticalForce
	{
		get 
		{
			return _verticalForce;
		}
	}

	public int mechanicType
	{
		get 
		{
			return _mechanicType;
		}
	}

	public int slots
	{
		get 
		{
			return _slots;
		}
	}

    public int scoreBonus
    {
        get 
        {
            return _scoreBonus;
        }
    }

    public int moneyBonus
    {
        get 
        {
            return _moneyBonus;
        }
    }

	public int bugChance
	{
		get 
		{
			return _bugChance;
		}
	}

	public int minBugCnt
	{
		get 
		{
			return _minBugCnt;
		}
	}

	public int bugMaxInterval
	{
		get 
		{
			return _bugMaxInterval;
		}
	}

	public int recipeFruitMaxInterval
	{
		get 
		{
			return _recipeFruitMaxInterval;
		}
	}

	public int maxBugCnt
	{
		get 
		{
			return _maxBugCnt;
		}
	}

	public Vector2 fruitInterval
	{
		get 
		{
			return _fruitInterval;
		}
	}

	public float gravity
	{
		get 
		{
			return _gravity;
		}
	}

	public float interval
	{
		get 
		{
			return _interval;
		}
	}

    public XmlNode mechanicSettings
    {
        get 
        {
            return _mechanicSettings;
        }
    }

	public float completeFruitCnt
	{
		get;
		set;
	}
}
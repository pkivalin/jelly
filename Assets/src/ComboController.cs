﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class ComboController : MonoBehaviour 
{

    public tk2dSprite leftDigit;
    public tk2dSprite rightDigit;
    public tk2dSprite centerDigit;
    private TweenParms _tween1;
    private TweenParms _tween2;
    private TweenParms _tween3;
    private Sequence _sequence;

    private int _loopsLeft;

    void Start()
    {
        _tween1 = new TweenParms();
        _tween1.Prop("localScale", new Vector3(1, 1, 1));
        _tween1.Ease(EaseType.EaseOutBack);


        _tween2 = new TweenParms();
        _tween2.Prop("localScale", new Vector3(1.1f, 1.1f, 1.1f));
        _tween2.Ease(EaseType.Linear);
        _tween3 = new TweenParms();
        _tween3.Prop("localScale", new Vector3(1, 1, 1));
        _tween3.Ease(EaseType.Linear);

        float timeX = 0.13f;

        _sequence = new Sequence();
        _sequence.Append(HOTween.To(gameObject.transform, timeX, _tween2));
        _sequence.Append(HOTween.To(gameObject.transform, timeX * 1.5f, _tween3));
        _sequence.autoKillOnComplete = false;
    }

    public void startPulsation()
    {
        gameObject.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
        HOTween.To(gameObject.transform, 0.2f, _tween1);
    }

    public void changeDigitsPulsation()
    {
        _sequence.Restart();
    }

    private int _combo;
    public int combo
    {
        set
        {
            if (GameManager.HIDE_COMBO)
                return;
            if (_combo == value)
                return;
            if (value == 0)
                gameObject.SetActive(false);
            else
            {
                if (gameObject.activeSelf == false)
                {
                    gameObject.SetActive(true);
                    rightDigit.gameObject.SetActive(false);
                    centerDigit.gameObject.SetActive(false);
                    startPulsation();
                    //run tween
                }
                else if (_combo < value)
                    changeDigitsPulsation();

                if (value < 10)
                {
                    leftDigit.SetSprite("combo" + value.ToString());
                }
                else if (value < 100)
                {
                    centerDigit.gameObject.SetActive(true);
                    leftDigit.SetSprite("combo" + (value / 10).ToString());
                    centerDigit.SetSprite("combo" + (value % 10).ToString());
                }
                else 
                {
                    rightDigit.gameObject.SetActive(true);
                    leftDigit.SetSprite("combo" + (value / 100).ToString());
                    centerDigit.SetSprite("combo" + (value / 10 % 10).ToString());
                    rightDigit.SetSprite("combo" + (value % 10).ToString());
                }
            }

            _combo = value;

        }
    }
}

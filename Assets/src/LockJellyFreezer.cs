using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class LockJellyFreezer : MonoBehaviour 
{
    private float _timer;
    private PositionedSprite _positionedSprite;
    private float _downTime;
    private int _clickCnt;
    private Game _game;
    private bool _active = false;

    void Update()
    {
        if (GameManager.GAME_RUNNING == false)
            return;

        if (!_active)
            return;
        float y = (_timer - _downTime / 2) * 200 / _downTime;
        float x = Mathf.Cos(5 / _downTime * _timer) * 25;
        Vector2 position = new Vector2(x, y);
        _positionedSprite.init(position, position, position);
        _timer -= Time.deltaTime;
    }

    public void init(Game game, float downTime, int clickCnt)
    {
        _game = game;
        _downTime = downTime;
        _clickCnt = clickCnt;
        _positionedSprite = gameObject.GetComponent<PositionedSprite>();
    }

    public void run()
    {
        gameObject.SetActive(true);
        Vector2 position = new Vector2(120, 120);
        _positionedSprite.init(position, position, position);
        _active = true;
        _timer = _downTime;
    }

    public void apply()
    {
        gameObject.SetActive(false);
        _game.lockJelly(_clickCnt);
        _active = false;
    }
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;


public class MultiResolution
{

	static private List<float> _aspectRatios = new List<float>(){3.0f / 2.0f, 4.0f / 3.0f, 16.0f / 9.0f}; 
	static private int _aspectRatioIndex = -1;
	static private Vector2 _resolution;
	static private float _height;

	static public void init(Vector2 resolution)
	{
		_resolution = resolution;
		float curAspectRatio = resolution.x / resolution.y;
		float delta = float.MaxValue;
		for (int i = 0; i < _aspectRatios.Count; i++)
		{
			if (Mathf.Abs(curAspectRatio - _aspectRatios[i]) < delta)
			{
				_aspectRatioIndex = i;
				delta = Mathf.Abs(curAspectRatio - _aspectRatios[i]);
			}
		}
	}

	static public int aspectRatioIndex
	{
		get 
		{
			return _aspectRatioIndex;
		}
	}

	static public Vector2 resolution
	{
		get 
		{
			return _resolution;
		}
	}

	public MultiResolution ()
	{
	}
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Holoville.HOTween;

public class Alert : MonoBehaviour
{
    public const int MISS = 0;
    public const int OOPS = 1;
    public const int COOL = 2;
    public const int NICE = 3;
    private static string[] TYPES = {"miss", "oops", "cool", "nice"};

    private const int PRECACHED_ALERT_CNT = 7;

	static private Color _newColor = Color.white;
    static private Color _lastColor = Color.white;
    static private Vector3 _newScale = new Vector3(0.4f, 0.4f, 0.4f);
	static private float _minY;
	static private float _alertLeftX;
	static private float _alertRightX;
	static private Stack<GameObject> _prefabs;
	static private List<GameObject> _curAlerts = new List<GameObject>();
	static private bool _inited = false;
	private TweenParms _tween1;
	private TweenParms _tween2;
	private TweenParms _tween3;
	private TweenParms _tween4;
    private Sequence _sequence;
    private tk2dSprite _sprite;

    void Awake()
    {
        _sprite = gameObject.GetComponent<tk2dSprite>();

        /*_tween1 = new TweenParms();
        _tween1.Prop("scale", new Vector3(1.1f, 1.1f, 1.1f)); 
        _tween1.Prop("color", Color.white);

        _tween2 = new TweenParms();
        _tween2.Prop("scale", new Vector3(0.95f, 0.95f, 0.95f)); 

        _tween3 = new TweenParms();
        _tween3.Prop("scale", new Vector3(1.0f, 1.0f, 1.0f)); 

        _tween4 = new TweenParms();
        _tween4.Prop("color", _newColor);
        _tween4.OnComplete(removeAlert);*/

        _lastColor.a = 0;

        _tween1 = new TweenParms();
        _tween1.Prop("scale", new Vector3(1.0f, 1.0f, 1.0f)); 
        _tween1.Ease(EaseType.Linear);

        _tween2 = new TweenParms();
        _tween2.Ease(EaseType.Linear);

        _tween3 = new TweenParms();
        _tween3.Ease(EaseType.Linear);
        _tween3.OnComplete(removeAlert);

        _tween4 = new TweenParms();
        _tween4.Prop("color", _lastColor);
        _tween4.Delay(0.5f);
        _tween4.Ease(EaseType.Linear);


        /*Sequence sequence = new Sequence();
        sequence.Append(HOTween.To(_sprite.gameObject.transform, 0.167f, _tween1));
        sequence.Append(HOTween.To(_sprite.gameObject.transform, 0.333f, _tween2));
        sequence.Append(HOTween.To(_sprite.gameObject.transform, 0.233f, _tween3));
        sequence.Play();

        _sequence = new Sequence();
        _sequence.Append(HOTween.To(_sprite, 0.15f, _tween1));
        _sequence.Append(HOTween.To(_sprite, 0.15f, _tween2));
        _sequence.Append(HOTween.To(_sprite, 0.15f, _tween3));
        _sequence.Append(HOTween.To(_sprite, 0.15f, _tween4));*/
        //_sequence.autoKillOnComplete = false;
    }

	static void init ()
	{
		_minY = Camera.main.ScreenToWorldPoint (new Vector3 (0, -70 * MultiResolution.resolution.y / 200 + MultiResolution.resolution.y / 2, 0)).y;

		switch (MultiResolution.aspectRatioIndex) {
			case 0:
			_alertRightX = 89;
			break;
			case 1:
			_alertRightX = 90;
			break;
			default:
			_alertRightX = 91;
			break;
		}

		_alertLeftX = -_alertRightX;
        _alertRightX = Camera.main.ScreenToWorldPoint (new Vector3 (PositionedSprite.getX(_alertRightX), 0, 0)).x;
        _alertLeftX = Camera.main.ScreenToWorldPoint (new Vector3 (PositionedSprite.getX(_alertLeftX), 0, 0)).x;
		_prefabs = new Stack<GameObject>();
        GameObject prefab;
        for (int i = 0; i < PRECACHED_ALERT_CNT; i++)
        {
            prefab = Instantiate(Resources.Load("Alert", typeof(GameObject))) as GameObject;
            prefab.SetActive(false);
            _prefabs.Push(prefab);
        }

		_inited = true;
	}

	static public void gameOver()
	{
		for (int i = 0; i < _curAlerts.Count; i++)
		{
			_curAlerts[i].SetActive(false);
			_prefabs.Push(_curAlerts[i]);
		}

		_curAlerts.Clear();
	}

    static public void addSideAlert(float y, bool isLeft, int type)
	{
		if (!_inited)
		{
			init();
		}
        addAlert(isLeft ? _alertLeftX : _alertRightX, y, type);
	}

    static public void addAlert(float x, float y, int type)
	{
		if (!_inited)
		{
			init();
		}
		GameObject prefab;
		if (_prefabs.Count == 0)
		{
			prefab = Instantiate(Resources.Load("Alert", typeof(GameObject))) as GameObject;
		}
		else 
		{
			prefab = _prefabs.Pop();
			prefab.SetActive(true);
		}

		Alert alert = prefab.GetComponent<Alert>();
        alert.init(x, y, type);
		_curAlerts.Add(prefab);
	}
	
    private void init(float x, float y, int type)
	{
		_sprite.color = _newColor;
		_sprite.scale = _newScale;
        _sprite.SetSprite(TYPES[type]);
		gameObject.transform.position = new Vector3(x, Mathf.Max(_minY, y), -3);

        _tween2.Prop("position", new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 0.5f, -3));
        _tween3.Prop("position", new Vector3(gameObject.transform.position.x, gameObject.transform.position.y + 1.5f, -3)); 

        HOTween.To(_sprite, 0.233f, _tween4);

        Sequence sequence = new Sequence();
        sequence.Append(HOTween.To(_sprite, 0.167f, _tween1));
        sequence.Append(HOTween.To(gameObject.transform, 0.333f, _tween2));
        sequence.Append(HOTween.To(gameObject.transform, 0.233f, _tween3));
        sequence.Play();
	}

	private void removeAlert()
	{
		gameObject.SetActive(false);
		_prefabs.Push(gameObject);
		_curAlerts.Remove(gameObject);
	}

	
}

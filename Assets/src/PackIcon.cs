﻿using UnityEngine;
using System.Collections;

public class PackIcon : MonoBehaviour {

    static private string[] titles = {"Breakfast\r\nRecipes", "Delicious\r\nRecipes", "Exotic\r\nRecipes"};

	public tk2dSprite icon;
    public tk2dTextMesh titleTF;
    public int packNumber;

    void OnEnable()
    {
        updateInfo();
    }

    public void updateInfo()
    {
        string packIconName = "pack_icon_";
        titleTF.text = titles[packNumber - 1];
        titleTF.Commit();
        if (UserData.hasPackAccess(packNumber) == false)
            packIconName += "shadow_";
        icon.SetSprite(packIconName + packNumber.ToString());
    }
}

using System;
using UnityEngine;

static public class UserData
{
	static UserData ()
	{
	}

    static public string userId
    {
        get
        {
            string key = "userid";
            if (PlayerPrefs.HasKey(key) == false)
            {
                byte[] userIdCodes = new byte[50];
                for (int i = 0; i < 50; i++)
                {
                    userIdCodes[i] = (byte)UnityEngine.Random.Range(97, 122);
                }
                string userId = System.Text.ASCIIEncoding.ASCII.GetString(userIdCodes);
                PlayerPrefs.SetString(key, userId);
             }
            return PlayerPrefs.GetString(key);
        }
    }

    static public void save()
    {
        PlayerPrefs.Save();
    }

	static public void setMaxLevel(int pack, int level)
	{
        if (level >= getMaxLevel(pack))
			PlayerPrefs.SetInt("progress/pack" + pack.ToString(), level + 1);
	}

	static public int getMaxLevel(int pack)
	{
		if (PlayerPrefs.HasKey("progress/pack" + pack.ToString()) == false)
		{
			PlayerPrefs.SetInt("progress/pack" + pack.ToString(), 0);
		}
		return PlayerPrefs.GetInt("progress/pack" + pack.ToString());
	}

    static public bool hasPackAccess(int pack)
    {
        if (pack > 1)
            return hasLevelAccess(Game.LEVELS_IN_PACK, pack - 1);
        return true;
    }

	static public bool hasLevelAccess(int level, int pack)
	{
        return hasPackAccess(pack) && level <= UserData.getMaxLevel(pack);
	}

	static public void setLevelStars(int pack, int level, int stars)
	{
		if (stars > getLevelStars(pack, level))
			PlayerPrefs.SetInt("progress/pack" + pack.ToString() + "/level" + level.ToString(), stars);
	}

	static public int getLevelStars(int pack, int level)
	{
		string key = "progress/pack" + pack.ToString() + "/level" + level.ToString();
		if (PlayerPrefs.HasKey(key) == false)
		{
			PlayerPrefs.SetInt(key, 0);
		}
		return PlayerPrefs.GetInt(key);
	}

    static public int getUpgradeLevel(string id)
	{
		string key = "upgrades/" + id + "/level/";
		if (PlayerPrefs.HasKey(key) == false)
		{
			PlayerPrefs.SetInt(key, 0);
		}
		return PlayerPrefs.GetInt(key);
	}

    static public void setUpgradeLevel(string id, int level)
	{
        PlayerPrefs.SetInt("upgrades/" + id + "/level/", level);
	}

    static public float getUpgradeValue(string id)
	{
		string key = "upgrades/" + id + "/value/";
		if (PlayerPrefs.HasKey(key) == false)
		{
			PlayerPrefs.SetFloat(key, 0);
		}
		return PlayerPrefs.GetFloat(key);
	}

    static public void setUpgradeValue(string id, float value)
	{
		PlayerPrefs.SetFloat("upgrades/" + id + "/value/", value);
	}

    static public float getAchievementProgress(string id)
    {
        string key = "achievements/" + id;
        if (PlayerPrefs.HasKey(key) == false)
        {
            PlayerPrefs.SetFloat(key, 0);
        }
        return PlayerPrefs.GetFloat(key);
    }

    static public void setAchievementProgress(string id, float value)
    {
        PlayerPrefs.SetFloat("achievements/" + id, value);
    }

	static public int arcadeBestScore
	{
		get
		{
			string key = "bestScore/survival";
			if (PlayerPrefs.HasKey(key) == false)
			{
				PlayerPrefs.SetInt(key, 0);
			}
			return PlayerPrefs.GetInt(key); 
		}
		set
		{
			if (arcadeBestScore < value)
            {
				PlayerPrefs.SetInt("bestScore/survival", value);
                GameManager.gamecenter.setArcadeBestScore(value);
            } 
		}
	}

	static public int timeBestScore
	{
		get
		{
			string key = "bestScore/time";
			if (PlayerPrefs.HasKey(key) == false)
			{
				PlayerPrefs.SetInt(key, 0);
			}
			return PlayerPrefs.GetInt(key); 
		}
		set
		{
			if (timeBestScore < value)
            {
				PlayerPrefs.SetInt("bestScore/time", value); 
                GameManager.gamecenter.setTimeBestScore(value);
            }
		}
	}

	static public int money
	{
		get
		{
			string key = "money";
			if (PlayerPrefs.HasKey(key) == false)
			{
				PlayerPrefs.SetInt(key, 0);
			}
			return PlayerPrefs.GetInt(key); 
		}
		set
		{
			PlayerPrefs.SetInt("money", value); 
		}
	}

    static public int achievementBonusCnt
    {
        get
        {
            string key = "achievement/bonusCnt";
            if (PlayerPrefs.HasKey(key) == false)
            {
                PlayerPrefs.SetInt(key, 0);
            }
            return PlayerPrefs.GetInt(key); 
        }
        set
        {
            PlayerPrefs.SetInt("achievement/bonusCnt", value); 
        }
    }

    static public string achievementLaunchDate
    {
        get
        {
            string key = "achievement/launchDate";
            if (PlayerPrefs.HasKey(key) == false)
            {
                PlayerPrefs.SetString(key, DateTime.Now.ToString());
            }
            return PlayerPrefs.GetString(key); 
        }
        set
        {
            PlayerPrefs.SetString("achievement/launchDate", value); 
        }
    }

    static public int achievementLoyalDays
    {
        get
        {
            string key = "achievement/loyalDays";
            if (PlayerPrefs.HasKey(key) == false)
            {
                PlayerPrefs.SetInt(key, 0);
            }
            return PlayerPrefs.GetInt(key); 
        }
        set
        {
            PlayerPrefs.SetInt("achievement/loyalDays", value); 
        }
    }

    static public int achievementAvoidBugCnt
    {
        get
        {
            string key = "achievement/avoidBugCnt";
            if (PlayerPrefs.HasKey(key) == false)
            {
                PlayerPrefs.SetInt(key, 0);
            }
            return PlayerPrefs.GetInt(key); 
        }
        set
        {
            PlayerPrefs.SetInt("achievement/avoidBugCnt", value); 
        }
    }

    static public int achievementUpgradesBuyed
    {
        get
        {
            string key = "achievement/upgradesBuyed";
            if (PlayerPrefs.HasKey(key) == false)
            {
                PlayerPrefs.SetInt(key, 0);
            }
            return PlayerPrefs.GetInt(key); 
        }
        set
        {
            PlayerPrefs.SetInt("achievement/upgradesBuyed", value); 
        }
    }

    static public int achievementMoneyEarned
    {
        get
        {
            string key = "achievement/moneyEarned";
            if (PlayerPrefs.HasKey(key) == false)
            {
                PlayerPrefs.SetInt(key, 0);
            }
            return PlayerPrefs.GetInt(key); 
        }
        set
        {
            PlayerPrefs.SetInt("achievement/moneyEarned", value); 
        }
    }

	static public void restart()
	{
		PlayerPrefs.DeleteAll();
	}

    static public bool musicIsOn
    {
        get
        {
            string key = "music";
            if (PlayerPrefs.HasKey(key) == false)
            {
                PlayerPrefs.SetInt(key, 1);
            }
            return PlayerPrefs.GetInt(key) == 1; 
        }
        set
        {
            PlayerPrefs.SetInt("music", value ? 1 : 0);
        }
    }

    static public bool soundIsOn
    {
        get
        {
            string key = "sound";
            if (PlayerPrefs.HasKey(key) == false)
            {
                PlayerPrefs.SetInt(key, 1);
            }
            return PlayerPrefs.GetInt(key) == 1; 
        }
        set
        {
            PlayerPrefs.SetInt("sound", value ? 1 : 0); 
        }
    }

    static public bool needSurvivalTutorial
    {
        get
        {
            string key = "survivalTutorial";
            if (PlayerPrefs.HasKey(key) == false)
            {
                PlayerPrefs.SetInt(key, 1);
            }
            return PlayerPrefs.GetInt(key) == 1; 
        }
        set
        {
            PlayerPrefs.SetInt("survivalTutorial", value ? 1 : 0);
        }
    }

    static public int playsCnt
    {
        get
        {
            string key = "playsCnt";
            if (PlayerPrefs.HasKey(key) == false)
            {
                PlayerPrefs.SetInt(key, 0);
            }
            return PlayerPrefs.GetInt(key); 
        }
        set
        {
            PlayerPrefs.SetInt("playsCnt", value); 
        }
    }

    static public bool arcadeIsFirst
    {
        get
        {
            string key = "ab/arcadeFirst";
            if (PlayerPrefs.HasKey(key) == false)
            {
                PlayerPrefs.SetInt(key, UnityEngine.Random.Range(0, 2));
            }
            return PlayerPrefs.GetInt(key) == 0; 
        }
    }

    static public bool isFirstPlay
    {
        get
        {
            string key = "isFirstPlay";
            bool result = PlayerPrefs.HasKey(key) == false;
            if (result)
                PlayerPrefs.SetInt("isFirstPlay", 1);

            return result;
        }
    }

    static public bool gamecenterLogged
    {
        get
        {
            string key = "gamecenterLogged";
            if (PlayerPrefs.HasKey(key) == false)
            {
                PlayerPrefs.SetInt(key, 0);
            }
            return PlayerPrefs.GetInt(key) == 1; 
        }
        set
        {
            PlayerPrefs.SetInt("gamecenterLogged", value ? 1 : 0);
        }
    }

}


﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class LevelUpAnimator : MonoBehaviour 
{
    private const float FIRST_TWEEN_DURATION = 0.27f;
    private const float SECOND_TWEEN_DURATION = 0.4f;
    private const float THIRD_TWEEN_DURATION = 0.4f;

    public tk2dSprite textSprite;
    private TweenParms _tween1;
    private TweenParms _tween2;
    private TweenParms _tween3;
    private Color _lastColor = Color.white;
    private Vector3 _newScale = new Vector3(0.0f, 0.0f, 0.0f);
    private Sequence _sequence;

    void Awake()
    {
        _lastColor.a = 0.0f;

        _tween1 = new TweenParms();
        _tween1.Prop("scale", new Vector3(1.0f, 1.0f, 1.0f)); 
        _tween1.Ease(EaseType.Linear);

        _tween2 = new TweenParms();
        _tween2.Prop("scale", new Vector3(1.2f, 1.2f, 1.2f));
        _tween2.Ease(EaseType.Linear);

        _tween3 = new TweenParms();
        _tween3.Prop("color", _lastColor);
        _tween3.Prop("scale", new Vector3(1.3f, 1.3f, 1.3f)); 
        _tween3.Ease(EaseType.Linear);
        _tween3.OnComplete(onComplete);

        _sequence = new Sequence();
        _sequence.Append(HOTween.To(textSprite, FIRST_TWEEN_DURATION, _tween1));
        _sequence.Append(HOTween.To(textSprite, SECOND_TWEEN_DURATION, _tween2));
        _sequence.Append(HOTween.To(textSprite, THIRD_TWEEN_DURATION, _tween3));
        _sequence.autoKillOnComplete = false;
    }

    void onComplete()
    {
        gameObject.SetActive(false);
    }

    public void start()
    {
        gameObject.SetActive(true);
        textSprite.color = Color.white;
        textSprite.scale = _newScale;

        _sequence.Restart();

    }

}

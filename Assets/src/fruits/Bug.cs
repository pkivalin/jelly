using UnityEngine;
using System.Collections;
using System.Xml;

public class Bug : Fruit {

    public const int INDEX = 2;
	
	protected override void Update () 
	{
        if (GameManager.GAME_RUNNING == false)
            return;

		base.Update();

		if (Time.timeScale == 0.0f || success)
			return;

		gameObject.rigidbody.AddForce(Vector3.up * Settings.bugForce * Time.deltaTime, ForceMode.Force);
	}
        
    public override void startFromSide(Game game, float horizontalForce, float verticalForce, FruitFactory factory, string name, int z)
    {
        float newHorizontalForce = Random.Range(Settings.bugHorizontalForce.x, Settings.bugHorizontalForce.y);
        if (horizontalForce < 0)
            newHorizontalForce *= -1;

        float newVerticalForce = Random.Range(Settings.bugVerticalForce.x, Settings.bugVerticalForce.y);
        if (verticalForce < 0)
            newVerticalForce *= -1;

        base.startFromSide(game, newHorizontalForce, newVerticalForce, factory, name, z);
    }
}

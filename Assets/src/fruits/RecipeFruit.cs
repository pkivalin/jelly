using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class RecipeFruit
{

    private HUDRecipeFruit _view;

	private string _fruitName;
	private int _cntLeft;
	private int _current;

	public RecipeFruit (XmlNode settings)
	{
		_fruitName = settings.Name;
		_cntLeft = int.Parse(settings.InnerText);
	}

    public void init(HUDRecipeFruit view)
	{
        _view = view;
        view.reset(_fruitName, _cntLeft);
	}

	public string fruitName
	{
		get
		{
			return _fruitName;
		}
	}

	public int total
	{
		get
		{
			return _cntLeft;
		}
		set
		{
			_cntLeft = value;
            _view.count = _cntLeft;
		}
	}
	
	public bool complete
	{
		get
		{
			return _cntLeft == 0;
		}
	}
}
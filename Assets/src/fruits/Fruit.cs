using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class Fruit : BaseObject {

	
    static public string[] TYPES = {
		"Grape"
		, "Strawberry"
		, "Bug"
		, "Lemon"
		, "Kiwi"
		, "Orange"
		, "Apple"
		, "Pear"
        , "Pomegranate"
        , "Pineapple"
		, "Watermelon"
	};

    static public int[] GAPS = {
        5
        , 5
        , 15
        , 10
        , 10
        , 10
        , 10
        , 10
        , 12
        , 12
        , 12
    };

    static public int[] LAYER_GAPS = {
        0
        , 25
        , 50
        , 150
        , 260
        , 320
        , 380
        , 440
        , 500
        , 570
        , 650
    };

	static Fruit()
	{
		for (int i = 0; i < Fruit.TYPES.Length; i++)
		{
			_settings[i] = Settings.getFruit(TYPES[i]);
		}
	}


	public Fruit()
	{
	}

	static protected XmlNode[] _settings = new XmlNode[TYPES.Length];

	private const int MAX_FROM_SIDE = 2;

	static private int _fromLeftCnt = 0;
	static private int _fromRightCnt = 0;

    static protected float ANGULAR_VELOCITY = 6.0f;

	private bool _inited = false;
	private FruitFactory _factory;
	private string _fruitName;

    private GameObject _frozenModel;
    public GameObject frozenModel
    {
        get
        {
            if (_frozenModel == null)
            {
                if (_frozenPrefabs.Count == 0)
                {
                    _frozenModel = Instantiate(Resources.Load("fruits/FrozenFruit", typeof(GameObject))) as GameObject;
                }
                else
                {
                    _frozenModel = _frozenPrefabs.Pop();
                    _frozenModel.SetActive(true);
                }
            }

            return _frozenModel;
        }
    }

    public virtual void startFromSide(Game game, float horizontalForce, float verticalForce, FruitFactory factory, string name, int z)
    {
        if (_fromLeftCnt >= MAX_FROM_SIDE)
            fromLeft = false;
        else if (_fromRightCnt >= MAX_FROM_SIDE)
            fromLeft = true;
        else
            fromLeft = Random.Range(0, 2) == 0 ? true : false;

        _fromLeftCnt = fromLeft ? _fromLeftCnt + 1 : 0;
        _fromRightCnt = fromLeft ? 0 : _fromRightCnt + 1;

        gameObject.transform.localPosition = new Vector3(fromLeft ? Game.START_MIN_X : Game.START_MAX_X, Random.Range(Settings.startHeight.x, Settings.startHeight.y), z);
        horizontalForce *= (fromLeft ? 1 : - 1);

        _start(game, horizontalForce, verticalForce, factory, name, z);
    }

    public void startFromPosition(Game game, Vector3 startPosition, float horizontalForce, float verticalForce, FruitFactory factory, string name, int z)
    {
        gameObject.transform.localPosition = startPosition;

        _start(game, horizontalForce, verticalForce, factory, name, z);
    }

    public bool isFrozen = false;
    public void setIsFrozen(bool value, bool needExplosion)
    {
        if (isFrozen == value)
            return;
        isFrozen = value;
        Renderer[] rs = GetComponentsInChildren<Renderer>();
        foreach(Renderer r in rs)
            r.enabled = !isFrozen;

        if (value)
        {
            frozenModel.transform.position = gameObject.transform.position;
            frozenModel.transform.parent = gameObject.transform;
            frozenModel.transform.localScale = Vector3.one;
        }
        else if (_frozenModel != null)
        {
            _frozenPrefabs.Push(_frozenModel);
            _frozenModel.SetActive(isFrozen);
            _frozenModel.transform.parent = null;
            _frozenModel = null;
        }

        if (needExplosion)
            playExplosion();
    }

    protected void _start(Game game, float horizontalForce, float verticalForce, FruitFactory factory, string name, int z)
	{
        base.start();
		_game = game;
		gameObject.rigidbody.isKinematic = false;
		if (_inited == false)
		{
			_factory = factory;
            _index = System.Array.IndexOf(TYPES, name);
			XmlNode settings = Settings.getFruit(name);

			_lifebar = new FruitLifebar(settings["lifebar"]);
			_penalty = int.Parse(settings["penalty"].InnerText);
			_chance = int.Parse(settings["chance"].InnerText);
			_points = int.Parse(settings["points"].InnerText);
			_bounciness = float.Parse(settings["bounciness"].InnerText);
			_scale = float.Parse(settings["scale"].InnerText);
			gameObject.transform.localScale = new Vector3(_scale, _scale, _scale);
			_inited = true;
			_fruitName = name;
		}
		else
		{
			gameObject.rigidbody.velocity = Vector3.zero;
			gameObject.rigidbody.angularVelocity = Vector3.zero;
			gameObject.collider.enabled = true;
		}

        if (isFrozen)
            setIsFrozen(false, false);

		canDestroy = false;
		success = false;
        gameObject.collider.material.bounciness = _bounciness;



        _velocity = horizontalForce * Vector3.right * Settings.forceMultiplyHorizontal;
        gameObject.rigidbody.AddForce(_velocity, ForceMode.Impulse);
        gameObject.rigidbody.AddForce(Vector3.up * verticalForce, ForceMode.Impulse);

        _addAngularVelocity();

        initCollisions();
	}

    protected virtual void _addAngularVelocity()
    {
        Vector3 angularVelocity = new Vector3();
        angularVelocity.x = Random.Range(ANGULAR_VELOCITY / 2, ANGULAR_VELOCITY);
        angularVelocity.x *= Random.Range(0, 2) == 0 ? 1 : -1;
        angularVelocity.y = Random.Range(ANGULAR_VELOCITY / 2, ANGULAR_VELOCITY);
        angularVelocity.y *= Random.Range(0, 2) == 0 ? 1 : -1;
        angularVelocity.z = Random.Range(ANGULAR_VELOCITY / 2, ANGULAR_VELOCITY);
        angularVelocity.z *= Random.Range(0, 2) == 0 ? 1 : -1;
        gameObject.rigidbody.angularVelocity = angularVelocity;
    }

	void Start () 
	{
	}
	
	public string fruitName
	{
		get
		{
			return _fruitName;
		}
	}

	protected override void apply () 
	{
		base.apply();
		_factory.addSuccessFruit(this);
	}

	protected int _index;
	protected int _penalty;
	protected int _chance;
	protected int _points;
	protected float _bounciness;
	protected float _scale;
	protected FruitLifebar _lifebar;

	public virtual int getLifebarPenalty()
	{
		return _lifebar.getPenalty(success, _factory.needInRecipe(this));
	}

	public int index
	{
		get 
		{
			return _index;
		}
	}

	public int penalty
	{
		get 
		{
			return _penalty;
		}
	}

	public int points
	{
		get 
		{
			return _points;
		}
	}

	public int chance
	{
		get 
		{
			return _chance;
		}
	}

}

using UnityEngine;
using System.Collections;
using System.Xml;

public class FruitLifebar
{
	public FruitLifebar (XmlNode settings)
	{
		_survivalMiss = int.Parse(settings["survivalMiss"].InnerText);
		_timeMiss = int.Parse(settings["timeMiss"].InnerText);
		_recipeMiss = int.Parse(settings["recipeMiss"].InnerText);
		_recipeError = int.Parse(settings["recipeError"].InnerText);
	}

	protected int _survivalMiss;
	protected int _timeMiss;
	protected int _recipeMiss;
	protected int _recipeError;

	public int getPenalty(bool success, bool needInRecipe)
	{
		if (Game.GAME_TYPE == Game.GAME_TYPE_SURVIVAL)
			return _survivalMiss;
		else if (Game.GAME_TYPE == Game.GAME_TYPE_TIME)
			return _timeMiss;
		else if (!success && needInRecipe)
			return _recipeMiss;
		else if (success && !needInRecipe)
			return _recipeError;
		else
			return 0;
	}
}


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class FruitFactory : MonoBehaviour
{
    private const int PRECACHED_ITEMS = 5;
    static private int[] COMBO_SCORES = {0, 0, 10, 11, 11, 12, 12, 13, 13, 14, 14, 15, 15
        , 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 23, 24, 24, 25
        , 25, 26, 26, 27, 27, 28, 28, 29, 29, 30, 30, 31, 31, 32, 32, 33, 33, 34, 34
        , 35, 35, 36, 36, 37, 37, 38, 38, 39, 39, 40, 40, 41, 41, 42, 42, 43, 43, 44
        , 44, 45, 45, 46, 46, 47, 47, 48, 48, 49, 49, 50};

    static private int MAX_COMBO_SCORE_BONUS = COMBO_SCORES[COMBO_SCORES.Length - 1];
    static private int COMBO_SCORES_CNT = COMBO_SCORES.Length;

    public tk2dUIItem addFruits;
    public tk2dTextMesh fruitsCntTxt;

	private Game _game;
	private List<List<GameObject>> _fruits;
	private List<Stack<GameObject>> _fruitsPool;
	private List<QueuedFruit> _fruitDelays = new List<QueuedFruit>();
	private float _fruitTimer;
	private int _sumFruitChances;
	private int[] _fruitChances;
	private float _lastComboFruit;
	private int _notBugsCnt;
    private int _notRecipetFruitCnt;
    private int _notWrongFruitCnt;
    private bool _started = false;
	private SoundController _soundController;
    private float _previousHorizontalForce;
    private float _previousVerticalForce;

	private int _fruitCnt;
	public int fruitCnt
	{
		get
		{
			return _fruitCnt;
		}
	}

	private int _bugsCnt;
	public int bugsCnt
	{
		get
		{
			return _bugsCnt;
		}
	}

	private int _totalCombo;
	public int totalCombo
	{
		get
		{
            _totalCombo = Mathf.Max(_totalCombo, comboCnt);
			return _totalCombo;
		}
	}

	private int _comboCnt;
	public int comboCnt 
	{
		get 
		{
			return _comboCnt;
		}
        set 
        {
            _comboCnt = value;
            GameManager.gamecenter.combo = _comboCnt;
        }
	}

    public void precachedInit()
    {
        GameObject fruit;

        _fruits = new List<List<GameObject>> ();
        _fruitsPool = new List<Stack<GameObject>> ();
        for (int i = 0; i < Fruit.TYPES.Length; i++)
        {
            _fruits.Add(new List<GameObject> ());
            _fruitsPool.Add(new Stack<GameObject> ());
            for (int j = 0; j < PRECACHED_ITEMS; j++)
            {
                fruit = Instantiate(Resources.Load("fruits/" + Fruit.TYPES[i], typeof(GameObject))) as GameObject;
                fruit.SetActive(false);
                _fruitsPool[i].Push(fruit);
            }
        }

        _soundController = GameObject.Find("SoundController").GetComponent<SoundController>();

        if (Settings.cheatsIsEnabled)
            addFruits.OnClick += _addTestFruits;
        else
        {
            addFruits.gameObject.SetActive(false);
            fruitsCntTxt.gameObject.SetActive(false);
        }
    }

    public void init(Game game)
	{
        StartCoroutine(startDelay());
		_game = game;

		_lastComboFruit = Settings.comboMaxDelay;
		_initChances();
		_fruitTimer = 0;
		_notBugsCnt = 0;
		_notRecipetFruitCnt = 0;
        _notWrongFruitCnt = 0;
		_fruitCnt = 0;
		_bugsCnt = 0;
		_totalCombo = 0;
		comboCnt = 0;
        GameManager.gamecenter.fruitInRow = 0;
        _previousVerticalForce = 0;
        _previousHorizontalForce = 0;
		_fruitDelays.Clear();
	}

    IEnumerator startDelay()
    {
        _started = false;
        yield return new WaitForSeconds(0.5f);
        _started = true;
    }

	void Update()
	{
        if (GameManager.GAME_RUNNING == false)
            return;

        if (Time.timeScale == 0.0f || !_started)
			return;

		_fruitTimer -= Time.deltaTime;
		_lastComboFruit += Time.deltaTime;
		if (_lastComboFruit > Settings.comboMaxDelay)
		{
			resetCombo();
		}

		int curFruitCnt = 0;

		GameObject fruit;
        Fruit fruitController;

        for (int i = 0; i < Fruit.TYPES.Length; i++) 
		{
			for (int j = 0; j < _fruits[i].Count; j++) 
			{
				fruit = _fruits[i][j];
				fruitController = fruit.GetComponent<Fruit> ();
                if (fruitController.canDestroy || fruit.transform.position.y < Game.MIN_Y || fruitController.forceDestroy)
				{
					_fruits[i].RemoveAt(j);
					_destroyFruit(fruit);
					j--;
					continue;
				}

				if (!fruitController.success) // обрабатывать только активные ягоды
					curFruitCnt++;
			}
		}

        if (GameManager.TUTORIAL_RUNNING)
            return; // не добавляем новые ягоды в туториале

		if (_fruitTimer <= 0 || (curFruitCnt == 0 && _fruitDelays.Count == 0))
		{
			addFruitsToQueue(_game.curLevel.slots, _game.curLevel.fruitInterval, true, _game.curLevel.horizontalForce, _game.curLevel.verticalForce);
		}

		for (int i = 0; i < _fruitDelays.Count; i++)
		{
			_fruitDelays[i].delay -= Time.deltaTime;
			if (_fruitDelays[i].delay <= 0)
			{
				addFruitFromPosition(_fruitDelays[i]);
				_fruitDelays.RemoveAt(i);
				i--;
			}
		}

        if (Settings.cheatsIsEnabled)
        {
            fruitsCntTxt.text = curFruitCnt.ToString();
            fruitsCntTxt.Commit();
        }
	}

    public int gameOver()
    {
        int cnt = 0;
        for (int i = 0; i < Fruit.TYPES.Length; i++) 
            for (int j = 0; j < _fruits[i].Count; j++) 
                cnt++;
        StartCoroutine(_gameOver());

        return cnt;
    }

    public void reset()
    {
        for (int i = 0; i < Fruit.TYPES.Length; i++) 
        {
            for (int j = 0; j < _fruits[i].Count; j++) 
            {
                _fruits[i][j].SetActive(false);
                _fruitsPool[i].Push(_fruits[i][j]);
            }
            _fruits[i].Clear();
        }
    }

    public IEnumerator _gameOver()
	{
        for (int i = 0; i < Fruit.TYPES.Length; i++) 
		{
			for (int j = 0; j < _fruits[i].Count; j++) 
			{
                yield return new WaitForSeconds(0.1f);
                if (j >= _fruits[i].Count)
                    break;
                _fruits[i][j].GetComponent<BaseObject>().playExplosion();
				_fruits[i][j].SetActive(false);
				_fruitsPool[i].Push(_fruits[i][j]);
			}
			_fruits[i].Clear();
		}
	}

	public bool needInRecipe(Fruit fruitController)
	{
		if (Game.GAME_TYPE != Game.GAME_TYPE_RECIPE)
			return false;
        return fruitController.isFrozen == false && _game.curLevel.neededFruits.ContainsKey(fruitController.fruitName);
	}

	public void addSuccessFruit(Fruit fruitController)
	{
        bool fruitNeedInRecipe = needInRecipe(fruitController);
        _game.catchFruit(fruitController, true, fruitNeedInRecipe);
        if (fruitController is Bug || (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE && !fruitNeedInRecipe)) // если жук или режим рецепта и ягода там не нужна 
		{
            GameManager.gamecenter.fruitInRow = 0;
			_soundController.playSound(_soundController.jellyBug);
            if (Game.GAME_TYPE != Game.GAME_TYPE_TIME)
			    _game.lifebar.curValue += fruitController.getLifebarPenalty();
			if (fruitController is Bug)
				_bugsCnt++;

            if (fruitController.needAlert)
                Alert.addAlert(fruitController.transform.position.x, Game.JELLY_ALERT_Y, Alert.OOPS);
			_game.score -= fruitController.penalty;
            _game.fails++;
			resetCombo();
		}
		else 
		{
            GameManager.gamecenter.fruitInRow++;
			if (_lastComboFruit < Settings.comboMaxDelay)
			{
				if (comboCnt == 0)
				{
					comboCnt++;

				}
				comboCnt++;
                if (_comboCnt >= COMBO_SCORES_CNT)
                    _game.score += MAX_COMBO_SCORE_BONUS;
                else
                    _game.score += COMBO_SCORES[_comboCnt];
			}
			_soundController.playSound(_soundController.jellyFruit);
			_lastComboFruit = 0;
            if (GameManager.TUTORIAL_RUNNING == false)
			    _game.curLevel.completeFruitCnt--;
			_fruitCnt++;
            if (Game.GAME_TYPE == Game.GAME_TYPE_SURVIVAL)
            {
                _game.money++;
            }

            // для режима рецептов умножаем очки за ягоду на константу
            _game.score += Game.GAME_TYPE == Game.GAME_TYPE_RECIPE ? fruitController.points * Settings.recipeFruitScoreMulti : fruitController.points;
			if (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE)
			{
				_game.curLevel.neededFruits[fruitController.fruitName].total--;
				if (_game.curLevel.neededFruits[fruitController.fruitName].complete)
				{

					_game.curLevel.neededFruits.Remove(fruitController.fruitName);
					if (_game.curLevel.neededFruits.Count == 0)
					{
						_game.winRecipeGame();
					}
					else
					{
						_soundController.playSound(_soundController.collectOneFruit);
					}
						
				}
			}
		}
	}

	void addMissedFruit(GameObject fruit)
	{
		Fruit fruitController = fruit.GetComponent<Fruit> ();
        bool fruitNeedInRecipe = needInRecipe(fruitController);
        _game.catchFruit(fruitController, false, fruitNeedInRecipe);
        if (fruitController is Bug || (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE && !fruitNeedInRecipe)) // если жук или режим рецепта и ягода там не нужна 
		{
			if (fruitController is Bug)
            {
				_bugsCnt++;
                GameManager.gamecenter.avoidBugCnt++;
            }
            //_game.score += fruitController.points;
		}
		else {
            GameManager.gamecenter.fruitInRow = 0;
			_soundController.playSound(_soundController.missFruit);
            if (fruitController.needAlert)
                Alert.addSideAlert(fruit.gameObject.transform.position.y, !fruitController.fromLeft, Alert.MISS);
			resetCombo();
			_game.score -= fruitController.penalty;
            _game.fails++;
            if (Game.GAME_TYPE != Game.GAME_TYPE_TIME)
			    _game.lifebar.curValue += fruitController.getLifebarPenalty();
		}
	}

	void _destroyFruit(GameObject fruit)
	{
		Fruit fruitController = fruit.GetComponent<Fruit> ();
		if (!fruitController.success)
		{
			addMissedFruit(fruit);
		}

		fruit.SetActive(false);
		_fruitsPool[fruitController.index].Push(fruit);
	}

	public void addFruitsToQueue(int cnt, Vector2 fruitInterval, bool canBeBug, Vector2 horizontalForce, Vector2 verticalForce)
	{
		for (int i = 0; i < cnt; i++)
		{
			_fruitDelays.Add(new QueuedFruit(i * Random.Range(fruitInterval.x, fruitInterval.y), canBeBug, horizontalForce, verticalForce));
		}
		resetFruitTimer();
	}

	private void _initChances()
	{
		_fruitChances = new int[_game.curLevel.allowedFruits.Length];
		_fruitChances[0] = int.Parse(Settings.getFruit(_game.curLevel.allowedFruits[0])["chance"].InnerText);

		for (int i = 1; i < _game.curLevel.allowedFruits.Length; i++)
		{
			_fruitChances[i] = int.Parse(Settings.getFruit(_game.curLevel.allowedFruits[i])["chance"].InnerText);
			_fruitChances[i] += _fruitChances[i - 1];
		}
		
		_sumFruitChances = _fruitChances[_fruitChances.Length - 1];
	}

	public void resetFruitTimer()
	{
		_fruitTimer = _game.curLevel.interval + _fruitDelays[_fruitDelays.Count - 1].delay;
	}

	public void addFruitFromPosition(QueuedFruit queuedFruit)
	{
		int fruitIndex = -1;

		if (queuedFruit.canBeBug && (_fruits[Bug.INDEX].Count < _game.curLevel.minBugCnt || _notBugsCnt >= _game.curLevel.bugMaxInterval))
		{
			fruitIndex = Bug.INDEX;
		}
		else if (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE && _notRecipetFruitCnt >= _game.curLevel.recipeFruitMaxInterval)
		{
			int fruitRandom = Random.Range(0, _game.curLevel.neededFruits.Count);
			int k = -1;

			for (int i = 0; i < _game.curLevel.allowedFruits.Length; i++)
			{
				if (_game.curLevel.neededFruits.ContainsKey(_game.curLevel.allowedFruits[i]))
				{
					k++;
				}
				if (fruitRandom == k)
				{
					fruitIndex = i;
					break;
				}
			}
		}
        else if (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE && _notWrongFruitCnt >= _game.curLevel.maxRecipeWrongFruitInterval)
        {
            int maxRandom = _game.curLevel.allowedFruits.Length - _game.curLevel.neededFruits.Count;
            if (_game.curLevel.bugChance == 0)
                maxRandom--; // вычитаем вероятность жука

            int fruitRandom = Random.Range(0, maxRandom);
            int k = -1;

            for (int i = 0; i < _game.curLevel.allowedFruits.Length; i++)
            {
                // проверяем что ненужная ягода
                if (_game.curLevel.neededFruits.ContainsKey(_game.curLevel.allowedFruits[i]) == false)
                {
                    // проверяем что это не жук или что шанс жука больше 0
                    if ( _game.curLevel.allowedFruits[i] != "Bug" || _game.curLevel.bugChance > 0)
                        k++;
                }
                if (fruitRandom == k)
                {
                    fruitIndex = i;
                    break;
                }
            }
        }
		else 
		{
			int fruitRandom;

			if (!queuedFruit.canBeBug || _fruits[Bug.INDEX].Count >= _game.curLevel.maxBugCnt)
			{
				fruitRandom = Random.Range(0, _sumFruitChances);
			}
			else
			{
				fruitRandom = Random.Range(0, _sumFruitChances + _game.curLevel.bugChance);
			}

			for (int i = 0; i < _game.curLevel.allowedFruits.Length; i++)
			{
				if (fruitRandom < _fruitChances[i])
				{
					fruitIndex = i;
					break;
				}
			}

			if (fruitIndex == -1)
			{
				fruitIndex = Bug.INDEX;
			}
		}

		if (fruitIndex == Bug.INDEX) 
		{
			_notBugsCnt = 0;
		}
		else
		{
			_notBugsCnt++;
		}

		if (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE && _game.curLevel.neededFruits.ContainsKey(_game.curLevel.allowedFruits[fruitIndex]))
		{
			_notRecipetFruitCnt = 0;
            _notWrongFruitCnt++;
		}
		else
		{
			_notRecipetFruitCnt++;
            _notWrongFruitCnt = 0;
		}

        fruitIndex = System.Array.IndexOf(Fruit.TYPES, _game.curLevel.allowedFruits[fruitIndex]);

        GameObject fruit = _getFruitGO(fruitIndex);

		Fruit fruitController = fruit.GetComponent<Fruit>();
        string fruitName = Fruit.TYPES[fruitIndex];
        int fruitZ = Fruit.LAYER_GAPS[fruitIndex] + _fruits[fruitIndex].Count * Fruit.GAPS[fruitIndex];

        //разброс ягод
        float horizontalForce = Random.Range(queuedFruit.horizontalForce.x, queuedFruit.horizontalForce.y);
        if (horizontalForce >= _previousHorizontalForce)
            horizontalForce = Mathf.Max(_previousHorizontalForce + Settings.forceDeltaHorizontal, horizontalForce);
        else
            horizontalForce = Mathf.Min(_previousHorizontalForce - Settings.forceDeltaHorizontal, horizontalForce);
        _previousHorizontalForce = horizontalForce;

        float verticalForce = Random.Range(queuedFruit.verticalForce.x, queuedFruit.verticalForce.y);
        if (verticalForce >= _previousVerticalForce)
            verticalForce = Mathf.Max(_previousVerticalForce + Settings.forceDeltaVertical, verticalForce);
        else
            verticalForce = Mathf.Min(_previousVerticalForce - Settings.forceDeltaVertical, verticalForce);
        _previousVerticalForce = verticalForce;
        // end разброс ягод

        fruitController.startFromSide(_game, horizontalForce, verticalForce, this, fruitName, fruitZ);
        if (Game.MECHANIC == Level.MECHANIC_FROZEN_FRUIT && needInRecipe(fruitController))
            fruitController.setIsFrozen(true, false);
		_fruits[fruitIndex].Add(fruit);
	}

    private void _addTestFruits()
    {
        addFruitsToQueue(2, new Vector2(0.1f, 0.1f), false, new Vector2(5, 10), new Vector2(4, 8));
    }

    private GameObject _getFruitGO(int fruitIndex)
    {
        GameObject fruit;
        if (_fruitsPool[fruitIndex].Count != 0)
        {
            fruit = _fruitsPool[fruitIndex].Pop();
            fruit.SetActive(true);
        }
        else
        {
            fruit = Instantiate(Resources.Load("fruits/" + Fruit.TYPES[fruitIndex], typeof(GameObject))) as GameObject;
        }

        return fruit;
    }

    public void addFruitFromPosition(int fruitIndex, Vector3 startPosition, float horizontalForce, float verticalForce)
    {
        string fruitName = Fruit.TYPES[fruitIndex];
        int fruitZ = Fruit.LAYER_GAPS[fruitIndex] + _fruits[fruitIndex].Count * Fruit.GAPS[fruitIndex];

        GameObject fruit = _getFruitGO(fruitIndex);
        Fruit fruitController = fruit.GetComponent<Fruit>();
        fruitController.startFromPosition(_game, startPosition, horizontalForce, verticalForce, this, fruitName, fruitZ);
        _fruits[fruitIndex].Add(fruit);
    }

    public void addFruitFromSide(int fruitIndex, float horizontalForce, float verticalForce)
    {
        string fruitName = Fruit.TYPES[fruitIndex];
        int fruitZ = Fruit.LAYER_GAPS[fruitIndex] + _fruits[fruitIndex].Count * Fruit.GAPS[fruitIndex];

        GameObject fruit = _getFruitGO(fruitIndex);
        Fruit fruitController = fruit.GetComponent<Fruit>();
        fruitController.startFromSide(_game, horizontalForce, verticalForce, this, fruitName, fruitZ);
        _fruits[fruitIndex].Add(fruit);
    }

	public void activateBorders()
	{
		if (_fruits == null)
			return;

		GameObject fruit;

        for (int i = 0; i <Fruit.TYPES.Length; i++) 
		{
			for (int j = 0; j < _fruits[i].Count; j++) 
			{
				fruit = _fruits[i][j];
				BaseObject baseObject = fruit.GetComponent<BaseObject> ();
				baseObject.initCollisions();
			}
		}
	}

	void resetCombo()
	{
		_lastComboFruit = Settings.comboMaxDelay;
        _totalCombo = Mathf.Max(_totalCombo, comboCnt);
		comboCnt = 0;
	}

	public void removeAllBugs()
	{
		for (int i = 0; i < _fruitDelays.Count; i++)
			_fruitDelays[i].canBeBug = false;

        for (int i = 0; i < _fruits[Bug.INDEX].Count; i++)
        {
            _fruits[Bug.INDEX][i].GetComponent<BaseObject>().forceDestroy = true;
        }
	}

}


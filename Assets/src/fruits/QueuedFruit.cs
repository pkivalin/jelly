using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QueuedFruit
{
	public QueuedFruit (float delay, bool canBeBug, Vector2 horizontalForce, Vector2 verticalForce)
	{
		this.delay = delay;
		this.canBeBug = canBeBug;
		this.horizontalForce = horizontalForce;
		this.verticalForce = verticalForce;
	}

	public float delay;
	public bool canBeBug;
	public Vector2 horizontalForce;
	public Vector2 verticalForce;

}


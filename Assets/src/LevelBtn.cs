﻿using UnityEngine;
using System.Collections;

public class LevelBtn : MonoBehaviour 
{

    public tk2dSprite icon;
    public tk2dSprite startsSprite;
    public tk2dUIItem button;
    public tk2dTextMesh numberTF;

    private SoundController _soundController;
    public SoundController soundController
    {
        get
        {
            if (_soundController == null)
                _soundController = GameObject.Find("SoundController").GetComponent<SoundController>();
            return _soundController;
        }
    }

    private MainMenuController _mainMenuController;
    public MainMenuController mainMenuController
    {
        get
        {
            if (_mainMenuController == null)
                _mainMenuController = GameObject.Find ("MenuController").GetComponent<MainMenuController>();
            return _mainMenuController;
        }
    }

    void Start()
    {
        button.OnClick += startRecipeGame;
    }

    private int _number;
    public int number
    {
        set
        {
            numberTF.text = value.ToString();
            numberTF.Commit();
            _number = value;
        }
    }

    void startRecipeGame()
    {
        soundController.playSound(soundController.click);
        if (UserData.hasLevelAccess(_number - 1, Game.PACK_NUMBER))
            mainMenuController.startLevel(_number - 1);
    }
}

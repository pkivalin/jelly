using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlaneController : MonoBehaviour {

	public tk2dUIItem jelleBtn;
	public tk2dSprite jellySprite;
    private JellyColor _jellyColor;
	private Vector3 _defaultScale;
	public tk2dSpriteAnimator tappedAnimation;
	public tk2dCamera gameCamera;
	public tk2dCamera guiCamera;
    private List<tk2dSpriteAnimator> _penetrations;
    private Stack<tk2dSpriteAnimator> _penetrationsPool;
	private SoundController _soundController;

	void Start() 
	{
		_soundController = GameObject.Find("SoundController").GetComponent<SoundController>();
	}

	public void init()
	{
		jelleBtn.OnDown += onJelleDown;
		jelleBtn.OnUp += onJelleUp;
		_defaultScale = gameObject.transform.localScale;
		_penetrations = new List<tk2dSpriteAnimator>();
        _jellyColor = GameObject.Find ("Jelly").GetComponent<JellyColor>();
        _penetrationsPool = new Stack<tk2dSpriteAnimator>();

        tk2dSpriteAnimator penetration;

        for (int i = 0; i < 10; i++)
        {
            penetration = Instantiate(Resources.Load("PenetrationAnimation", typeof(tk2dSpriteAnimator))) as tk2dSpriteAnimator;
            penetration.gameObject.SetActive(false);
            _penetrationsPool.Push(penetration);
        }

        Settings.forceMultiplyHorizontal = _defaultScale.x / 23.4f;
	}

	public void reset()
	{
		for (int i = 0; i < _penetrations.Count; i++)
		{
			_penetrations[i].Stop();
			DestroyObject(_penetrations[i].gameObject);
		}

		_penetrations.Clear();
	}

	void onJelleUp ()
	{
		_soundController.playSound(_soundController.untouchJelly);
        _jellyColor.setAlpha(1);
		gameObject.collider.isTrigger = false;
		gameObject.transform.localScale = new Vector3 (_defaultScale.x, _defaultScale.y, _defaultScale.z);
		tappedAnimation.Play("untapped");

	}

	private void onJelleDown()
	{
		_soundController.playSound(_soundController.touchJelly);
        _jellyColor.setAlpha(Settings.jellyAlpha);
		gameObject.collider.isTrigger = true;
		gameObject.transform.localScale = new Vector3 (_defaultScale.x * Settings.jellyScale, _defaultScale.y, _defaultScale.z);
		tappedAnimation.Play("tapped");
	}

	void OnCollisionEnter(Collision collision)
	{
        LockJellyFreezer freezer = collision.gameObject.GetComponent<LockJellyFreezer>();
        if (freezer != null)
        {
            freezer.apply();
            return;
        }

		BaseObject baseObject = collision.gameObject.GetComponent<BaseObject>();

        if (Game.MECHANIC == Level.MECHANIC_BORDERS)
        {
            baseObject.bounceCnt++;
            if (baseObject.bounceCnt == Settings.maxFruitBounceCnt)
            {
                baseObject.forceDestroy = true;
                baseObject.needAlert = false;
                return;
            }
        }

        if (Game.MECHANIC == Level.MECHANIC_FROZEN_FRUIT && baseObject is Fruit)
        {
            (baseObject as Fruit).setIsFrozen(false, true);
        }

		if (baseObject is Bug)
		{
			_soundController.playSound(_soundController.hitBug);
		}
		else if (baseObject is Fruit)
		{
			_soundController.playSound(_soundController.hitFruit);
		}
		else if (baseObject is Bonus)
		{
            if (baseObject is LockJelly)
            {
                OnTriggerEnter(baseObject.collider);
            }
            else
            {
                _soundController.playSound(_soundController.hitBonus);
            }
		}

        if (collision.relativeVelocity.y > - Settings.minVelocityVertical)
            baseObject.needUpForce = true;
	}

	void OnTriggerEnter(Collider flyingObject)
	{
        if (GameManager.GAME_RUNNING == false)
            return;
        LockJellyFreezer freezer = flyingObject.GetComponent<LockJellyFreezer>();
        if (freezer != null)
        {
            freezer.apply();
            return;
        }


		BaseObject baseObject = flyingObject.GetComponent<BaseObject>();
		baseObject.success = true;

		if (baseObject is Bonus)
		{
			_soundController.playSound(_soundController.jellyBonus);
		}

        tk2dSpriteAnimator penetration;
        if (_penetrationsPool.Count > 0)
        {
            penetration = _penetrationsPool.Pop();
            penetration.gameObject.SetActive(true);
        }
        else
        {
            penetration = Instantiate(Resources.Load("PenetrationAnimation", typeof(tk2dSpriteAnimator))) as tk2dSpriteAnimator;
        }

        penetration.transform.parent = GameObject.Find("GameScreen").transform;
        _jellyColor.setSpriteColor(penetration.Sprite);

		float neededX = flyingObject.gameObject.transform.position.x;
		neededX = Mathf.Max(Game.PENETRATION_MIN_X, neededX);
		neededX = Mathf.Min(Game.PENETRATION_MAX_X, neededX);
		penetration.transform.position = new Vector3(neededX, tappedAnimation.transform.position.y, 0.5f);
		penetration.Play(Random.Range(0, 2) == 0 ? "penetration" : "penetration2");
		penetration.AnimationCompleted = penetrationCompleteDelegate;
		_penetrations.Add(penetration);
	}

	void penetrationCompleteDelegate(tk2dSpriteAnimator sprite, tk2dSpriteAnimationClip clip) 
	{
        _penetrationsPool.Push(sprite);
        sprite.gameObject.SetActive(false);
		_penetrations.Remove(sprite);
	}
}

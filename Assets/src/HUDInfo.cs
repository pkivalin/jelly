﻿using UnityEngine;
using System.Collections;

public class HUDInfo : MonoBehaviour 
{

	public tk2dSprite icon;
    public tk2dTextMesh countTF;

    public int count
    {
        set
        {
            countTF.text = value.ToString();
            countTF.Commit();
        }
    }

}

﻿using UnityEngine;
using System.Collections;

public class Clock : MonoBehaviour {
   
    public tk2dSprite leftDigit;
    public tk2dSprite rightDigit;
    public PulsationLifebarAnimator clockPulsation;
    private int _timeLeft;

    public void setTime(int seconds)
    {
        if (_timeLeft == seconds)
            return;
        _timeLeft = seconds;
        leftDigit.SetSprite("cl" + (seconds / 10).ToString());
        rightDigit.SetSprite("cl" + (seconds % 10).ToString());
        if (seconds <= 3 && seconds > 0)
            clockPulsation.startPulsation(1);
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class Upgrade : MonoBehaviour
{
    static public Dictionary<string, Upgrade> REAL_MONEY_UPGRADES = new Dictionary<string, Upgrade>();

    public const string THERMOMETER_REPAIR = "net.kivalin.jelly.autocooling";
    public const string THERMOMETER_CAPACITY = "upgrade_thermometr_capacity";
    public const string BONUS_DELAY = "upgrade_bonus_delay";
    public const string ADD_FRUIT_BONUS = "upgrade_add_fruit_bonus";
    public const string ADD_LIVE_BONUS = "upgrade_add_live_bonus";
    public const string ADD_TIME_BONUS = "upgrade_add_time_bonus";
    public const string ADD_FREEZE_BONUS = "upgrade_add_freeze_bonus";
    public const string ADD_BORDERS_BONUS = "upgrade_add_borders_bonus";
    public const string REMOVE_ADS = "net.kivalin.jelly.removeads";
    public const string DOUBLE_COINS = "net.kivalin.jelly.doublecoins";
    public const string VIDEO_AD = "video_ad";


	const string EMPTY_UPGRADE = "upgradeLevelEmpty";
	const string FULL_UPGRADE = "upgradeLevelFull";

    public tk2dSprite iconSprite;
    public tk2dSprite coinIcon;
	public tk2dTextMesh titleTF;
	public tk2dTextMesh descriptionTF;
	public tk2dTextMesh priceTF;
	public tk2dUIItem buyBtn;
    public tk2dSprite buttonSprite;
	public tk2dSprite[] levels;

	private int[] _prices;
	private float[] _values;
	private int _curLevel;
	private ShopCategory _category;
    private string _description;
    private string _descriptionFristLevel;
    private string _descriptionMaxLevel;
    private int _maxLevel;
    private bool _isForRealMoney;
    private int _reward;
    private string _realPrice;

    private string _id;
    public string id
    {
        get
        {
            return _id;
        }
    }

    private PluginsProxy _plugins;
    public PluginsProxy plugins
    {
        get
        {
            if (_plugins == null)
            {
                _plugins = GameObject.Find("PluginsController").GetComponent<PluginsProxy>();
            }

            return _plugins;
        }
    }

    private SoundController _soundController;
    public SoundController soundController
    {
        get
        {
            if (_soundController == null)
                _soundController = GameObject.Find("SoundController").GetComponent<SoundController>();
            return _soundController;
        }
    }

	public void init(XmlNode settings, ShopCategory category)
	{
		_category = category;
		_id = settings["id"].InnerText;
        _isForRealMoney = settings["realMoney"] != null;
        if (_isForRealMoney)
        {
            if (_id == THERMOMETER_REPAIR)
            {
                REAL_MONEY_UPGRADES.Add(_id + "1", this);
                REAL_MONEY_UPGRADES.Add(_id + "2", this);
            }
            else
            {
                REAL_MONEY_UPGRADES.Add(_id, this);
            }
        }
            

		_curLevel = UserData.getUpgradeLevel(_id);

		int i = 0;

        _maxLevel = settings["levels"].ChildNodes.Count;
        _prices = new int[_maxLevel];
		_values = new float[_maxLevel];
        if (settings["reward"] != null)
            _reward = int.Parse(settings["reward"].InnerText);

		foreach (XmlNode levelXml in settings["levels"])
		{
			_prices[i] = int.Parse(levelXml["price"].InnerText);
			_values[i] = float.Parse(levelXml["value"].InnerText);
			i++;
		}

		if (_curLevel > 0)
		{
			UserData.setUpgradeValue(_id, _values[_curLevel - 1]);
		}

		titleTF.text = settings["title"].InnerText;
		titleTF.Commit();

        _description = settings["description"].InnerText;
        _descriptionFristLevel = settings["firstLevelDescription"].InnerText;
        _descriptionMaxLevel = settings["maxLevelDescription"].InnerText;
        if (settings["price"] != null)
            _realPrice = settings["price"].InnerText;
  
		iconSprite.SetSprite(settings["icon"].InnerText);

		buyBtn.OnClick += buyUpgrade;

        for (i = 0; i < levels.Length; i++)
        {
            if (i >= _values.Length)
                levels[i].gameObject.SetActive(false);
            else
                levels[i].gameObject.SetActive(true);
            if (i < _curLevel)
                levels[i].SetSprite(FULL_UPGRADE);
            else
                levels[i].SetSprite(EMPTY_UPGRADE);
        }

        if (_isForRealMoney)
        {
            coinIcon.gameObject.SetActive(false);
            if (_id == VIDEO_AD)
                buttonSprite.SetSprite("buyFreeBtn");
            else
                buttonSprite.SetSprite("buyBtnBux");
        }
	}

	public void updateInfo()
	{
		_curLevel = UserData.getUpgradeLevel(_id);
        if (_maxLevel != 0 && _curLevel >= _maxLevel)
		{
			buyBtn.gameObject.SetActive(false);
		}
		else
		{
			buyBtn.gameObject.SetActive(true);
		}

        for (int i = 0; i < _values.Length; i++)
		{
			if (i < _curLevel)
				levels[i].SetSprite(FULL_UPGRADE);
			else
				levels[i].SetSprite(EMPTY_UPGRADE);
		}


        if (_values.Length != 0)
        {
            if (_curLevel == _maxLevel)
            {
                descriptionTF.text = _descriptionMaxLevel.Replace("{to}", _values[_curLevel - 1].ToString());
                priceTF.text = "";
            }
            else if (_curLevel == 0)
            {
                descriptionTF.text = _descriptionFristLevel.Replace("{to}", _values[_curLevel].ToString());
                priceTF.text = _prices[_curLevel].ToString();
            }
            else
            {
                string description = _description.Replace("{to}", _values[_curLevel].ToString());
                description = description.Replace("{from}", _values[_curLevel - 1].ToString());
                descriptionTF.text = description;
                priceTF.text = _prices[_curLevel].ToString();
            }
        }
        else
        {
            descriptionTF.text = _description;
        }


        if (_isForRealMoney)
        {
            priceTF.text = _realPrice;
            if (_id == VIDEO_AD)
            {
                if (plugins.videoAdEnabled())
                    buttonSprite.SetSprite("buyFreeBtn");
                else
                    buttonSprite.SetSprite("buyFreeNaBtn");
            }
        }
        else if (_curLevel < _maxLevel)
        {
            if (_prices[_curLevel] <= UserData.money)
                buttonSprite.SetSprite("buyBtn");
            else
                buttonSprite.SetSprite("buyNaBtn");
        }

		priceTF.Commit();
		descriptionTF.Commit();


            
	}

    public void apply()
    {
        Debug.Log("c# apply " + _id.ToString());
        if (_maxLevel == 0) // пак или видео
        {
            UserData.money += _reward;
        }
        else if (_curLevel < _maxLevel) 
        {
            UserData.setUpgradeValue(_id, _values[_curLevel]);
            _curLevel++;
            UserData.setUpgradeLevel(_id, _curLevel);
            GameManager.gamecenter.upgradesBuyed++;
        }
        else
        {
            Debug.Log("c# apply wrong item! " + _id.ToString());
        }

        plugins.flurryParams.Add("id", _id.ToString());
        plugins.flurryParams.Add("level", _curLevel.ToString());
        plugins.logEvent("success buy real upgrade", plugins.flurryParams);

        _category.updateInfo();
    }

	void buyUpgrade()
	{
        soundController.playSound(soundController.click);
        if (_isForRealMoney)
        {
            string id = _id;
            if (id == THERMOMETER_REPAIR)
                id += (UserData.getUpgradeLevel(id) + 1).ToString();

            if (id == VIDEO_AD)
                plugins.videoAds(this);
            else 
                plugins.buyMarketItem(id);

            plugins.flurryParams.Add("id", id.ToString());
            plugins.flurryParams.Add("level", _curLevel.ToString());
            plugins.logEvent("try buy real upgrade", plugins.flurryParams);
        }
        else
        {
            if (_curLevel < _maxLevel && _prices[_curLevel] <= UserData.money)
            {
                UserData.money -= _prices[_curLevel];
                UserData.setUpgradeValue(_id, _values[_curLevel]);
                _curLevel++;
                UserData.setUpgradeLevel(_id, _curLevel);
                _category.updateInfo();
                plugins.flurryParams.Add("id", _id.ToString());
                plugins.flurryParams.Add("level", _curLevel.ToString());
                plugins.logEvent("buy upgrade", plugins.flurryParams);
                GameManager.gamecenter.upgradesBuyed++;
            }
        }
		
	}
	
}
using UnityEngine;
using System.Collections;

public class PauseScreen : MonoBehaviour {

	public tk2dUIItem resumeBtn;
	public tk2dUIItem menuBtn;
	public tk2dUIItem restartBtn;

    private GameManager _gameManager;
    public GameManager gameManager
    {
        get
        {
            if (_gameManager == null)
                _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            return _gameManager;
        }
    }

    private SoundController _soundController;
    public SoundController soundController
    {
        get
        {
            if (_soundController == null)
                _soundController = GameObject.Find("SoundController").GetComponent<SoundController>();
            return _soundController;
        }
    }

	private Game _game;

	// Use this for initialization
	void Start () 
	{
		resumeBtn.OnClick += _hide;
		menuBtn.OnClick += _menu;
		restartBtn.OnClick += _restart;
	}

	public void init(Game game)
	{
		_game = game;
	}
	
	private void _hide()
	{
        soundController.playSound(soundController.click);
		Time.timeScale = 1.0f;
		gameObject.SetActive(false);
	}

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape)) 
        {
            _hide();
        }
    }

	private void _menu()
	{
        _game.resetFabrics();
        _game.closeGame();
        _hide();
        gameManager.showMenu();
	}


	private void _restart()
	{
        _hide();
        _game.resetFabrics();
        _game.resetLevel();
        _game.startGame(true);
	}

	public void show()
	{
		Time.timeScale = 0.0f;
		gameObject.SetActive(true);
	}

}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class Game : MonoBehaviour {

	static public float START_MIN_X = 0;
	static public float START_MAX_X;
	static public float END_MIN_X = 0;
	static public float END_MAX_X;
	static public float PENETRATION_MIN_X = 0;
	static public float PENETRATION_MAX_X;
	static public float JELLY_ALERT_Y;
	static public int MAX_LIVES = 3;
	static public int GAME_TYPE;
	static public int PACK_NUMBER = 1;
    static public int LEVEL_NUMBER;
    static public int MECHANIC;
	static public bool LEVEL_CHANGED = false;
    static public float MIN_Y = -40;
    public const float TIME_MODE_LEVEL_DURATION = 5;

    public const int GAME_TYPE_RECIPE = 0;
    public const int GAME_TYPE_SURVIVAL = 1;
    public const int GAME_TYPE_TIME = 2;

	

    static public string[] TYPE_NAMES = {"recipe game", "arcade game", "time game"};

    public const int LEVELS_IN_PACK = 16;
    public const int PACKS_CNT = 3;

    public tk2dUIItem pauseBtn;
    public tk2dUIItem nextLevelBtn;
	public tk2dCamera guiCamera;
	public tk2dUIItem tapToStartBtn;
	public Camera gameCamera;
	public tk2dSprite[] recipeFruitIcons;
	public tk2dSprite[] recipeFruitCompletes;
	public tk2dTextMesh[] recipeFruitAmountss;
    public BorderCollider leftCollider;
    public BorderCollider rightCollider;
    public HUD hud;
    public HUDRecipeFruits hudRecipes;
    public tk2dSprite recipeFruitsBG;
    public InteractiveTutorial _tutorial;
    public tk2dSprite mechanicIcon;
    public ComboController comboController;

	private List<Level> _levels;
	private int _curLevelIndex;
	private Level _curLevel;
	private bool _isGameOver;
    private float _timeLeft;
    private float _changeLevelTimeLeft;
    private float _gameDuration;
	private BonusFactory _bonusFactory;
    private PlaneController _planeController;
	public GameObject tapToStartGO;
	private SoundController _soundController;
    private Clock _clock;
    private JellyBorders _jellyBorders;
    private JellyColor _jellyColor;
    private bool _isStarted = false;
    private bool _hasContinueGame = false;
    private float _timeScaleBeforeContinue;
    public LevelUpAnimator levelUpAnimator;

    private GameOverAnimation _gameOverAnimation;
    public GameOverAnimation gameOverAnimation
    {
        get
        {
            if (_gameOverAnimation == null)
            {
                _gameOverAnimation = Instantiate(Resources.Load("GameOverAnimation", typeof(GameOverAnimation))) as GameOverAnimation;
                _gameOverAnimation.transform.parent = gameObject.transform;
                _gameOverAnimation.init(this, _fruitFactory, _bonusFactory
                    , lifebar.gameObject.GetComponent<PulsationLifebarAnimator>()
                    , _clock.gameObject.GetComponent<PulsationLifebarAnimator>());
            }

            return _gameOverAnimation;
        }
    }

    private PluginsProxy _plugins;
    public PluginsProxy plugins
    {
        get
        {
            if (_plugins == null)
            {
                _plugins = GameObject.Find("PluginsController").GetComponent<PluginsProxy>();
            }

            return _plugins;
        }
    }

    private LockJellyController _jellyLocker;
    public LockJellyController jellyLocker
    {
        get
        {
            if (_jellyLocker == null)
            {
                _jellyLocker = Instantiate(Resources.Load("JellyLocker", typeof(LockJellyController))) as LockJellyController;
                _jellyLocker.transform.parent = gameObject.transform;
            }

            return _jellyLocker;
        }
    }

    private Buggun _buggun;
    public Buggun buggun
    {
        get
        {
            if (_buggun == null)
            {
                _buggun = Instantiate(Resources.Load("Buggun", typeof(Buggun))) as Buggun;
                _buggun.transform.parent = gameObject.transform;
                _buggun.init(fruitFactory);
            }

            return _buggun;
        }
    }

    private ContinueWindow _continueWindow;
    public ContinueWindow continueWindow
    {
        get
        {
            if (_continueWindow == null)
            {
                _continueWindow = Instantiate(Resources.Load("ContinueWindow", typeof(ContinueWindow))) as ContinueWindow;
                _continueWindow.transform.parent = gameObject.transform;
                _continueWindow.init(this);
            }

            return _continueWindow;
        }
    }

    private PauseScreen _pauseScreen;
    public PauseScreen pauseScreen
    {
        get
        {
            if (_pauseScreen == null)
            {
                _pauseScreen = Instantiate(Resources.Load("PauseScreen", typeof(PauseScreen))) as PauseScreen;
                _pauseScreen.transform.parent = gameObject.transform;
                _pauseScreen.init(this);
            }

            return _pauseScreen;
        }
    }

	private FruitFactory _fruitFactory;
	public FruitFactory fruitFactory 
	{
		get 
		{
			return _fruitFactory;
		}
	}

	private LifebarController _lifebar;
	public LifebarController lifebar 
	{
		get 
		{
			return _lifebar;
		}
	}

	private int _score;
	public int score 
	{
		get 
		{
			return _score;
		}
		set 
		{
            _score = Mathf.Max(0, value);
            GameManager.gamecenter.score = _score;
		}
	}

    private int _money;
    public int money 
    {
        get 
        {
            return _money;
        }
        set 
        {
            _money = value;
        }
    }

    private int _fails;
    public int fails 
    {
        get 
        {
            return _fails;
        }
        set 
        {
            _fails = value;
        }
    }

    private RecipeTutorial _recipeTutorial;
    public RecipeTutorial recipeTutorial
    {
        get
        {
            if (_recipeTutorial == null)
            {
                _recipeTutorial = Instantiate(Resources.Load("RecipeTutorial", typeof(RecipeTutorial))) as RecipeTutorial;
                _recipeTutorial.init(Settings.getRecipeTutorial(), _fruitFactory, this);
            }

            return _recipeTutorial;
        }
    }

    private SurvivalTutorial _survivalTutorial;
    public SurvivalTutorial survivalTutorial
    {
        get
        {
            if (_survivalTutorial == null)
            {
                _survivalTutorial = Instantiate(Resources.Load("SurvivalTutorial", typeof(SurvivalTutorial))) as SurvivalTutorial;
                _survivalTutorial.init(Settings.getSurvivalTutorial(), _fruitFactory, this);
            }

            return _survivalTutorial;
        }
    }

    public void init()
    {
        MIN_Y = gameCamera.ScreenToWorldPoint(new Vector3(0, MIN_Y, 0)).y;
        START_MIN_X = gameCamera.ScreenToWorldPoint(new Vector3(START_MIN_X, 0, 0)).x;
        float endGap = (int)guiCamera.TargetResolution.x / 4.0f;
        END_MIN_X = gameCamera.ScreenToWorldPoint(new Vector3(endGap, 0, 0)).x;
        START_MAX_X = (int)guiCamera.TargetResolution.x;
        START_MAX_X = gameCamera.ScreenToWorldPoint(new Vector3(START_MAX_X, 0, 0)).x;
        END_MAX_X = gameCamera.ScreenToWorldPoint(new Vector3((int)guiCamera.TargetResolution.x - endGap, 0, 0)).x;
        float penetrationGap = (int)guiCamera.TargetResolution.x / 10.0f;
        PENETRATION_MIN_X = gameCamera.ScreenToWorldPoint(new Vector3(penetrationGap, 0, 0)).x;
        PENETRATION_MAX_X = gameCamera.ScreenToWorldPoint(new Vector3((int)guiCamera.TargetResolution.x - penetrationGap, 0, 0)).x;
        float jellyAlertGap = (int)guiCamera.TargetResolution.y / 5.0f;
        JELLY_ALERT_Y = gameCamera.ScreenToWorldPoint(new Vector3(0, jellyAlertGap, 0)).y;

        _jellyColor = GameObject.Find ("Jelly").GetComponent<JellyColor>();
        _jellyBorders = GameObject.Find ("Jelly").GetComponent<JellyBorders>();
        _jellyBorders.init(this);
        _planeController = GameObject.Find ("jellyCube").GetComponent<PlaneController>();
        _planeController.init();
        _clock = Instantiate(Resources.Load("Clock", typeof(Clock))) as Clock;
        _clock.transform.parent = gameObject.transform;
        _lifebar = Instantiate(Resources.Load("Lifebar", typeof(LifebarController))) as LifebarController;
        _lifebar.transform.parent = gameObject.transform;

        _fruitFactory = gameObject.GetComponent<FruitFactory>();
        _fruitFactory.precachedInit();
        _bonusFactory = gameObject.GetComponent<BonusFactory>();
        _bonusFactory.init(this);
        _initPhysics();
        pauseBtn.OnClick += showPause;

        if (Settings.cheatsIsEnabled)
            nextLevelBtn.OnClick += _changeLevel;
        else
            nextLevelBtn.gameObject.SetActive(false);

        tapToStartBtn.OnClick += _startRecipe;

        _soundController = GameObject.Find("SoundController").GetComponent<SoundController>();

        rightCollider.init(_fruitFactory);
        leftCollider.init(_fruitFactory);
    }

    public void startGame(bool restart = false)
    {
        levelUpAnimator.gameObject.SetActive(false);
        GameManager.GAME_RUNNING = false;
        _initLevels();
        _soundController.restart();

        _hasContinueGame = false;

        Bonus.bonusInfo.reset();
        leftCollider.reset();
        rightCollider.reset();

        _fails = 0;
        _gameDuration = 0.0f;
        if (GAME_TYPE == GAME_TYPE_TIME)
        {
            _money = Settings.timeModeBonus;
            _clock.gameObject.SetActive(true);
            _clock.setTime(Settings.timeModeDuration);
            _timeLeft = Settings.timeModeDuration;
            _changeLevelTimeLeft = TIME_MODE_LEVEL_DURATION;
        }
        else
        {
            _money = 0;
            _clock.gameObject.SetActive(false);
            _lifebar.gameObject.SetActive(true);
            _lifebar.init();
        }

        hud.money = _money;

        _planeController.reset();
        _fruitFactory.init(this);
        hud.init();
        comboController.gameObject.SetActive(false);

        _jellyColor.init();

        _isGameOver = false;

        score = 0;
        _initNeededFruits();

        LEVEL_CHANGED = false;

        _initMechanic();

        GameManager.HIDE_COMBO = false;

        if (GAME_TYPE == GAME_TYPE_RECIPE)
        {
            tapToStartGO.SetActive(true);
            mechanicIcon.SetSprite(LevelMapController.MECHANIC_ICONS[MECHANIC]);
            recipeFruitsBG.gameObject.SetActive(true);
        }
        else
        {
            tapToStartGO.SetActive(false);
            GameManager.GAME_RUNNING = true;
            recipeFruitsBG.gameObject.SetActive(false);
        }

        plugins.flurryParams.Add("level", GAME_TYPE == GAME_TYPE_RECIPE ? PACK_NUMBER.ToString() + "_" + LEVEL_NUMBER.ToString() : "0");
        plugins.flurryParams.Add("restart", restart.ToString());
        plugins.logEvent(TYPE_NAMES[GAME_TYPE], plugins.flurryParams, true);

        _isStarted = true;

        _bonusFactory.startGame();

        _lifebar.hideForTutorial = false;
        if (_tutorial != null)
            _tutorial.closeTutorial();

        if (Game.GAME_TYPE == Game.GAME_TYPE_SURVIVAL && UserData.needSurvivalTutorial)
            _tutorial = survivalTutorial;
        else if (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE && Game.LEVEL_NUMBER == 0 && Game.PACK_NUMBER == 1)
            _tutorial = recipeTutorial;

        if (_tutorial != null)
        {
            _lifebar.hideForTutorial = true;
            if (GAME_TYPE != GAME_TYPE_RECIPE)
                _tutorial.start();
        }

        if (GameManager.isFirstPlay)
        {
            plugins.flurryParams.Add("gameType", TYPE_NAMES[GAME_TYPE]);
            plugins.logEvent("firtstPlay", plugins.flurryParams);
        }

        if (GAME_TYPE == GAME_TYPE_TIME)
            _lifebar.gameObject.SetActive(false);
    }

    public void initGameObjectScale(GameObject gameObject)
    {
        Vector3 posStart = gameCamera.WorldToScreenPoint(new Vector3(gameObject.renderer.bounds.min.x, gameObject.renderer.bounds.min.y, gameObject.renderer.bounds.min.z));
        Vector3 posEnd = gameCamera.WorldToScreenPoint(new Vector3(gameObject.renderer.bounds.max.x, gameObject.renderer.bounds.max.y, gameObject.renderer.bounds.min.z));

        int widthX = (int)(posEnd.x - posStart.x);
        float neededScaleX = gameObject.transform.localScale.x * guiCamera.TargetResolution.x * (1 - Settings.jellyGap / 100) /  (float)widthX;

        gameObject.transform.localScale = new Vector3(neededScaleX, gameObject.transform.localScale.y, gameObject.transform.localScale.z);
    }

    private void _initMechanic()
    {
        if (_curLevel.mechanicType == Level.MECHANIC_BORDERS || (Settings.cheatsIsEnabled && GAME_TYPE == GAME_TYPE_TIME))
        {
            _jellyBorders.setBorders(true);
            activateBorders(); 
        }
        else
        {
            _jellyBorders.setBorders(false);
            leftCollider.gameObject.SetActive(false);
            rightCollider.gameObject.SetActive(false);
        }

        if (_curLevel.mechanicType == Level.MECHANIC_FREEZE)
        {
            jellyLocker.gameObject.SetActive(true);
            jellyLocker.init(_curLevel.mechanicSettings, this);
        }
        else if (_jellyLocker != null)
        {
            _jellyLocker.gameObject.SetActive(false);
        }

        if (_buggun != null)
        {
            _buggun.gameObject.SetActive(false);
        }

        MECHANIC = _curLevel.mechanicType;
    }

    public void lockJelly(int clickCnt)
	{
        jellyLocker.activate(clickCnt);
	}

    public void activateBorders(bool needDestroyBug = false)
	{
        leftCollider.show(needDestroyBug);
        rightCollider.show(needDestroyBug);
		_fruitFactory.activateBorders();
	}

	public void deactivateBorders()
	{
        leftCollider.hide();
        rightCollider.hide();
	}

	void _startRecipe()
	{
		tapToStartGO.SetActive(false);

        if (_curLevel.mechanicType == Level.MECHANIC_BUGGUN)
        {
            buggun.gameObject.SetActive(true);
            buggun.init(_curLevel.mechanicSettings);
        }

        hudRecipes.tween();
        if (_tutorial != null)
            _tutorial.start();
        GameManager.GAME_RUNNING = true;
	}

    public void resetTutorial()
    {
        _tutorial = null;
    }

    public void catchFruit(Fruit fruitController, bool success, bool needInRecipe)
    {
        if (_tutorial != null)
            _tutorial.catchFruit(fruitController, success, needInRecipe);
    }

	void _initNeededFruits()
	{
		int k = 0;
		if (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE) 
		{
            hudRecipes.resetPosition();

			foreach (KeyValuePair<string, RecipeFruit> recipeFruit in _curLevel.neededFruits)
			{
                recipeFruit.Value.init(hudRecipes.fruits[k]);
				k++;
			}

            hudRecipes.show(k);
		}
		for (int i = k; i < recipeFruitIcons.Length; i++)// убираем лишние лейблы
		{
			recipeFruitIcons[i].gameObject.SetActive(false);
			recipeFruitAmountss[i].gameObject.SetActive(false);
			recipeFruitCompletes[i].gameObject.SetActive(false);
		}
	}

	private void _changeLevel ()
	{
        if (_curLevelIndex >= _levels.Count - 1)
            return;

        if (_curLevelIndex > 0 || GAME_TYPE == GAME_TYPE_TIME)
		{
			_soundController.playSound(_soundController.newLevel);
            _jellyColor.setIndex(_curLevelIndex);
            levelUpAnimator.start();
		}

		_curLevelIndex++;
		_curLevel = _levels[_curLevelIndex];
        score += _curLevel.scoreBonus;
        _money += _curLevel.moneyBonus;
		Physics.gravity = new Vector3(0, -_curLevel.gravity);

        plugins.flurryParams.Add("game type", GAME_TYPE.ToString());
        plugins.flurryParams.Add("level", _curLevelIndex.ToString());
        plugins.logEvent("change level", plugins.flurryParams);
	}

	void _initPhysics()
	{
        Physics.IgnoreLayerCollision (8, 8, true);// ягоды
        Physics.IgnoreLayerCollision (8, 16, true);// ягоды с Oculling Colider'ми
	}
	// бонусы и границы

	private void _initLevels()
	{
		_levels = new List<Level>();
		XmlNode xml = Settings.getLevels();

		if (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE)
		{
			_levels.Add(new Level(xml));
		}
		else
		{
			foreach (XmlNode levelXml in xml)
			{
				_levels.Add(new Level(levelXml));
			}
		}

		_curLevelIndex = 0;
		_curLevel = _levels[_curLevelIndex];
		Physics.gravity = new Vector3(0, -_curLevel.gravity);
	}
	
	void showPause ()
    {
        if (GameManager.GAME_RUNNING == false)
            return;

        _soundController.playSound(_soundController.click);
		pauseScreen.show();
	}

	// Update is called once per frame
	void Update() 
	{
        if (GameManager.GAME_RUNNING == false)
            return;

        if (_isStarted == false || _isGameOver || Time.timeScale == 0.0f)
			return;

        if (Input.GetKeyUp(KeyCode.Menu)) 
        {
            showPause();
        }

        _gameDuration += Time.deltaTime;

        if (GAME_TYPE == GAME_TYPE_TIME)
        {
            _timeLeft -= Time.deltaTime;
            _changeLevelTimeLeft -= Time.deltaTime;
            if (_timeLeft <= 0)
            {
                gameOver();
            }
            else if (_changeLevelTimeLeft <= 0)
            {
                _changeLevelTimeLeft = TIME_MODE_LEVEL_DURATION;
                _changeLevel();
            }
            _clock.setTime((int)(_timeLeft + 0.9f)); // 0.9 чтобы юзер успел увидеть 0 когда время кончится
        }

        hud.score = score;
        hud.money = _money;
        comboController.combo = _fruitFactory.comboCnt;
        hud.mistakes = _fails;

        if (GAME_TYPE != GAME_TYPE_TIME && _lifebar.isFull)
		{
            if (GAME_TYPE == GAME_TYPE_RECIPE || _tutorial != null || _hasContinueGame)
                gameOver(false);
            else
                _showContinueWindow();
		}

        if (GAME_TYPE == GAME_TYPE_SURVIVAL)
        {
            try
            {
                if (_curLevel.completeFruitCnt < 0)
                {
                    _changeLevel();
                }
            }
            catch
            {
                Debug.Log(_curLevel);
                Debug.Log(_levels);
                Debug.Log(_levels.Count);
                Debug.Log(_curLevel.completeFruitCnt);
            }
        }

	}

    private void _showContinueWindow()
    {
        _timeScaleBeforeContinue = Time.timeScale;
        Time.timeScale = 1;
        GameManager.GAME_RUNNING = false;
        float explosionsDuration = gameOverAnimation.startExplosions(false) * 0.1f;
        Invoke("_showContinueWindowAfterExplosions", explosionsDuration);

    }

    private void _showContinueWindowAfterExplosions()
    {
        continueWindow.show();
    }

    public void continueGame()
    {
        Time.timeScale = _timeScaleBeforeContinue;
        _hasContinueGame = true;
        GameManager.GAME_RUNNING = true;
        _lifebar.curValue = 0;
    }

	public void winRecipeGame()
	{
        if (GameManager.GAME_RUNNING == false) // уже проиграли
            return;
		UserData.setMaxLevel(Game.PACK_NUMBER, Game.LEVEL_NUMBER);
        int starsCnt = _lifebar.getStarsCnt();
        UserData.setLevelStars(PACK_NUMBER, LEVEL_NUMBER, starsCnt);
        GameManager.gamecenter.changeLevelStars(PACK_NUMBER, LEVEL_NUMBER, starsCnt);
        _money = _curLevel.moneyBonus + Settings.starsAddMoney[starsCnt - 1];
        gameOver(true);
	}

	public void resetLevel()
	{
        _isStarted = false;
		Alert.gameOver();
        if (GAME_TYPE != GAME_TYPE_TIME)
		    LifebarDigits.gameOver();
	}

    public void resetFabrics()
    {
        if (_tutorial != null)
        {
            _tutorial.closeTutorial();
        }
        _fruitFactory.reset();
        _bonusFactory.reset();
    }

    public void gameOver(bool win = true, bool needExplosions = true)
	{
        if (GameManager.GAME_RUNNING == false && needExplosions)
            return;

        if (_tutorial != null)
            _tutorial.closeTutorial();

        plugins.endTimedEvent(TYPE_NAMES[GAME_TYPE]);
        plugins.flurryParams.Add("win", win.ToString());
        plugins.flurryParams.Add("level", GAME_TYPE == GAME_TYPE_RECIPE ? PACK_NUMBER.ToString() + "_" + LEVEL_NUMBER.ToString() : _curLevelIndex.ToString());

        GameManager.GAME_RUNNING = false;
        Time.timeScale = 1;

        if (_jellyLocker != null)
            _jellyLocker.gameObject.SetActive(false);

        _soundController.playWinLoseSound(win ? _soundController.win : _soundController.loose);
		resetLevel();
		_isGameOver = true;
		bool needStars = GAME_TYPE == GAME_TYPE_RECIPE;
        int bestScore = 0;

		if (GAME_TYPE == GAME_TYPE_SURVIVAL)
		{
			UserData.arcadeBestScore = score;
            bestScore = UserData.arcadeBestScore;
		}
		else if (GAME_TYPE == GAME_TYPE_TIME)
		{
            _money = Settings.timeModeBonus - _fails * Settings.timeModePenalty;
            GameManager.gamecenter.timeModeFails = _fails;
            _money = Mathf.Max(0, _money);
            if (_fails == 0)
            {
                _money += Settings.timeModePerfectBonusMoney;
                score += Settings.timeModePerfectBonusScore;
            }

			UserData.timeBestScore = score;
            bestScore = UserData.timeBestScore;
		}


        plugins.flurryParams.Add("money", _money.ToString());
        plugins.logEvent("game over " + TYPE_NAMES[GAME_TYPE], plugins.flurryParams);

        gameOverAnimation.looseScreen.show(_fruitFactory.fruitCnt, bestScore, _money, _fruitFactory.totalCombo, _fails, score, needStars, _lifebar.getStarsCnt());
        if (needExplosions)
            gameOverAnimation.start(win);
        else
            gameOverAnimation.startAfterExplosions();
        
	}

    public void closeGame()
    {
        plugins.endTimedEvent(TYPE_NAMES[GAME_TYPE]);
        plugins.flurryParams.Add("game type", GAME_TYPE.ToString());
        plugins.flurryParams.Add("level", GAME_TYPE == GAME_TYPE_RECIPE ? PACK_NUMBER.ToString() + "_" + LEVEL_NUMBER.ToString() : _curLevelIndex.ToString());
        plugins.logEvent("close game", plugins.flurryParams);
    }

	public Level curLevel {
		get 
		{
			return _curLevel;
		}
	}
}

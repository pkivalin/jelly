using UnityEngine;
using System.Collections;

public class LooseScreenController : MonoBehaviour {

	public tk2dUIItem menuBtn;
	public tk2dUIItem nextLvlBtn;
	public tk2dUIItem restartBtn;
    public LooseScreenInfo[] infos;
	public LevelStars levelStars;
    public GameObject bugSpriteGO;
    public GameObject medalSpriteGO;
    public tk2dSprite moneyMakerSprite;
    public tk2dTextMesh moneyMakerText;

    private Game _game;
    private LooseScreenInfo _score;
    private LooseScreenInfo _bestScore;
    private LooseScreenInfo _money;
    private LooseScreenInfo _mistakes;
    private LooseScreenInfo _combo;
    private Vector3 _rightBtnPosition;
    private Vector3 _middleBtnPosition;

    private GameManager _gameManager;
    public GameManager gameManager
    {
        get
        {
            if (_gameManager == null)
                _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            return _gameManager;
        }
    }

    private SoundController _soundController;
    public SoundController soundController
    {
        get
        {
            if (_soundController == null)
                _soundController = GameObject.Find("SoundController").GetComponent<SoundController>();
            return _soundController;
        }
    }

	// Use this for initialization
	void Start() 
	{
		menuBtn.OnClick += _menu;
		restartBtn.OnClick += _restart;
		nextLvlBtn.OnClick += _nextLevel;
	}

	void _nextLevel()
	{
        if (Game.LEVEL_NUMBER == Game.LEVELS_IN_PACK - 1)
        {
            Game.LEVEL_NUMBER = 0;
            Game.PACK_NUMBER++;
        }
        else
        {
            Game.LEVEL_NUMBER++;
        }

        Game.LEVEL_CHANGED = true;
        soundController.playSound(soundController.click);
        gameObject.SetActive(false);
        _game.resetFabrics();
        _game.startGame();
	}

	public void init(Game game)
	{
        _rightBtnPosition = nextLvlBtn.transform.position;
        _middleBtnPosition = restartBtn.transform.position;
		_game = game;
        gameObject.transform.parent = _game.gameObject.transform;
	}

	private void _menu()
	{
        _game.resetFabrics();
        soundController.playSound(soundController.click);
        gameObject.SetActive(false);
        gameManager.showMenu();
	}

    private void _restart()
	{
        soundController.playSound(soundController.click);
        gameObject.SetActive(false);
        _game.resetFabrics();
        _game.startGame(true);
	}

    public void show(int fruitCnt, int bestScore, int money, int combo, int mistakes, int score, bool needStars = false, int starsCnt = 0)
	{
        if (starsCnt == 0 && Game.GAME_TYPE == Game.GAME_TYPE_RECIPE)
        {
            bugSpriteGO.SetActive(true);
            medalSpriteGO.SetActive(false);
        }
        else
        {
            bugSpriteGO.SetActive(false);
            medalSpriteGO.SetActive(true);
        }

        _score = null;
        _bestScore = null;
        _money = null;
        _mistakes = null;
        _combo = null;

        if (Game.GAME_TYPE == Game.GAME_TYPE_SURVIVAL)
        {
            infos[0].gameObject.SetActive(true);
            infos[1].gameObject.SetActive(true);
            infos[2].gameObject.SetActive(true);
            infos[3].gameObject.SetActive(true);
            _score = infos[0];
            _bestScore = infos[1];
            _combo = infos[2];
            _money = infos[3];
        }
        else if (Game.GAME_TYPE == Game.GAME_TYPE_TIME)
        {
            infos[0].gameObject.SetActive(true);
            infos[1].gameObject.SetActive(true);
            infos[2].gameObject.SetActive(true);
            infos[3].gameObject.SetActive(true);
            _score = infos[0];
            _bestScore = infos[1];
            _mistakes = infos[2];
            _money = infos[3];
        }
        else
        {
            infos[0].gameObject.SetActive(false);
            infos[1].gameObject.SetActive(false);
            infos[2].gameObject.SetActive(true);
            infos[3].gameObject.SetActive(true);

            _combo = infos[2];
            _money = infos[3];
        }

        if (_score != null)
        {
            _score.icon.SetSprite("smallScore");
            _score.title = "Score";
            _score.count = score;
        }
        if (_bestScore != null)
        {
            _bestScore.icon.SetSprite("smallBestScore");
            _bestScore.title = "Best score";
            _bestScore.count = bestScore;
        }
        if (_money != null)
        {
            _money.icon.SetSprite("smallCoin");
            _money.title = "Coins";
            _money.count = money;
        }
        if (_mistakes != null)
        {
            _mistakes.icon.SetSprite("smallMistakes");
            _mistakes.title = "Mistakes";
            _mistakes.count = mistakes;
        }
        if (_combo != null)
        {
            _combo.icon.SetSprite("smallCombo");
            _combo.title = "Max combo";
            _combo.count = combo;
        }

		if (needStars)
		{
			levelStars.gameObject.SetActive(true);
			levelStars.setStars(starsCnt);
		}
		else
		{
			levelStars.gameObject.SetActive(false);
		}

        if (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE && // если рецепты
            ((Game.LEVEL_NUMBER == Game.LEVELS_IN_PACK - 1 && Game.PACK_NUMBER != Game.PACKS_CNT && UserData.hasLevelAccess(0, Game.PACK_NUMBER + 1)) // и последний уровень в паке, но не последний пак
                || (UserData.hasLevelAccess(Game.LEVEL_NUMBER + 1, Game.PACK_NUMBER) && (Game.LEVEL_NUMBER + 1 < Game.LEVELS_IN_PACK)))) // или не последний уровень
		{
			nextLvlBtn.gameObject.SetActive(true);
            restartBtn.transform.position = _middleBtnPosition;
            nextLvlBtn.transform.position = _rightBtnPosition;
		}
		else
		{
            restartBtn.transform.position = _rightBtnPosition;
			nextLvlBtn.gameObject.SetActive(false);
		}

        if (UserData.getUpgradeLevel(Upgrade.DOUBLE_COINS) > 0)
        {
            moneyMakerSprite.gameObject.SetActive(true);
            moneyMakerText.text = money.ToString();
            moneyMakerText.Commit();
            money *= 2;
        }
        else
        {
            moneyMakerSprite.gameObject.SetActive(false);
        }

        UserData.money += money;
        GameManager.gamecenter.moneyEarned += money;
	}

}

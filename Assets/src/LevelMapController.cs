﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class LevelMapController : MonoBehaviour 
{
	static public int ROWS = 4;
	static public int COLS = 4;

    static public string[] MECHANIC_ICONS = 
    {
        "level_icon_common",
        "level_icon_barriers",
        "level_icon_freeze",
        "level_icon_wind",
        "level_icon_bug_gun",
        "level_icon_ice_cube"
    };

    static private string[][] LEVEL_ICONS = new string[Game.PACKS_CNT][];

    public PackIcon packIcon;

    private List<LevelBtn> _levelBtns;
	private MainMenuController _mainMenuController;

    public void init() 
	{
        XmlDocument pack;
        XmlNode level;
        int mechanicType;

        for (int i = 0; i < LEVEL_ICONS.Length; i++) 
        {
            pack = Settings.getPackXml(i + 1);
            LEVEL_ICONS[i] = new string[Game.LEVELS_IN_PACK];
            for (int j = 0; j < Game.LEVELS_IN_PACK; j++)
            {
                mechanicType = 0;
                level = pack.SelectSingleNode("settings/levels/level" + j);
                if (level["mechanic"] != null)
                    mechanicType = int.Parse(level["mechanic"]["type"].InnerText);
                LEVEL_ICONS[i][j] = MECHANIC_ICONS[mechanicType];
            }
        }


        _levelBtns = new List<LevelBtn>();
        LevelBtn levelBtn;
		PositionedSprite positionedSprite;
        int number;

		Vector2 startIphone = new Vector2(-18, 63);
		Vector2 startIpad = new Vector2(-17, 53);
		Vector2 startIphone5 = new Vector2(-15, 63);

		Vector2 gapIphone = new Vector2(31, -42);
		Vector2 gapIpad = new Vector2(30, -38);
		Vector2 gapIphone5 = new Vector2(26, -42);

		for (int i = 0; i < ROWS; i++)
		{
			for (int j = 0; j < COLS; j++)
			{
                levelBtn = Instantiate(Resources.Load("LevelBtn", typeof(LevelBtn))) as LevelBtn;
                positionedSprite = levelBtn.gameObject.GetComponent<PositionedSprite>();
                number = i * ROWS + j + 1;
                levelBtn.number = number;

				Vector2 neededIphone = new Vector2(startIphone.x + gapIphone.x * j, startIphone.y + gapIphone.y * i);
				Vector2 neededIpad = new Vector2(startIpad.x + gapIpad.x * j, startIpad.y + gapIpad.y * i);
				Vector2 neededIphone5 = new Vector2(startIphone5.x + gapIphone5.x * j, startIphone5.y + gapIphone5.y * i);

                positionedSprite.init(neededIphone, neededIpad, neededIphone5);
                _levelBtns.Add(levelBtn);
                levelBtn.gameObject.transform.parent = gameObject.transform;
			}
		}
	}

	public void updateInfo()
	{
        packIcon.packNumber = Game.PACK_NUMBER;
        packIcon.updateInfo();
        for (int i = 0; i < _levelBtns.Count; i++)
		{
            _levelBtns[i].icon.SetSprite(LEVEL_ICONS[Game.PACK_NUMBER - 1][i]);
            if (i == 0 && Game.PACK_NUMBER == 1)
                _levelBtns[i].startsSprite.SetSprite("level_icon_tutorial");
            else if (UserData.hasLevelAccess(i, Game.PACK_NUMBER))
                _levelBtns[i].startsSprite.SetSprite("level_star_" + UserData.getLevelStars(Game.PACK_NUMBER, i));
			else
                _levelBtns[i].startsSprite.SetSprite("level_icon_lock");
		}
	}
}

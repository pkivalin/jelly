﻿using UnityEngine;
using System.Collections;

public class JellyColor : MonoBehaviour {

    public tk2dSprite jelly;
    public tk2dSprite jellyTop;
    private Color[] _colors;
    private float _alpha;
    private int _index;

    public const int COLORS_CNT = 15;

    void Start()
    {
        _colors = new Color[COLORS_CNT];
        _colors[0] = new Color(179.0f / 255.0f, 19.0f / 255.0f, 64.0f / 255.0f);
        _colors[1] = new Color(222.0f / 255.0f, 20.0f / 255.0f, 0.0f / 255.0f);
        _colors[2] = new Color(222.0f / 255.0f, 66.0f / 255.0f, 0.0f / 255.0f);
        _colors[3] = new Color(247.0f / 255.0f, 147.0f / 255.0f, 23.0f / 255.0f);
        _colors[4] = new Color(247.0f / 255.0f, 206.0f / 255.0f, 0.0f / 255.0f);
        _colors[5] = new Color(132.0f / 255.0f, 214.0f / 255.0f, 17.0f / 255.0f);
        _colors[6] = new Color(21.0f / 255.0f, 166.0f / 255.0f, 13.0f / 255.0f);
        _colors[7] = new Color(9.0f / 255.0f, 135.0f / 255.0f, 70.0f / 255.0f);
        _colors[8] = new Color(25.0f / 255.0f, 212.0f / 255.0f, 230.0f / 255.0f);
        _colors[9] = new Color(10.0f / 255.0f, 135.0f / 255.0f, 166.0f / 255.0f);
        _colors[10] = new Color(10.0f / 255.0f, 62.0f / 255.0f, 166.0f / 255.0f);
        _colors[11] = new Color(53.0f / 255.0f, 7.0f / 255.0f, 161.0f / 255.0f);
        _colors[12] = new Color(117.0f / 255.0f, 0.0f / 255.0f, 153.0f / 255.0f);
        _colors[13] = new Color(209.0f / 255.0f, 0.0f / 255.0f, 196.0f / 255.0f);
        _colors[14] = new Color(199.0f / 255.0f, 0.0f / 255.0f, 104.0f / 255.0f);
    }

    public void init()
    {
        _alpha = 1.0f;
        _index = 0;
        _setColor();
    }

    public void setIndex(int index)
    {
        _index = index % COLORS_CNT;
        _setColor();
    }

    private void _setColor()
    {
        _colors[_index].a = _alpha;
        jelly.color = _colors[_index];
        _colors[_index].a = 1.0f;
        jellyTop.color = _colors[_index];
    }

    public void setAlpha(float alpha)
    {
        _alpha = alpha;
        _setColor();
    }

    public void setSpriteColor(tk2dBaseSprite sprite)
    {
        sprite.color = _colors[_index];
    }
}

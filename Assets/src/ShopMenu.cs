using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class ShopMenu : MonoBehaviour 
{

	public GameObject[] categories;
	public tk2dUIItem[] categoryBtns;

	private GameObject _curCategory;
	private ShopCategory[] categoryControllers;
    public tk2dTextMesh titleTF;
    public tk2dUIItem restoreBtn;

    private SoundController _soundController;
    public SoundController soundController
    {
        get
        {
            if (_soundController == null)
                _soundController = GameObject.Find("SoundController").GetComponent<SoundController>();
            return _soundController;
        }
    }

    private PluginsProxy _plugins;
    public PluginsProxy plugins
    {
        get
        {
            if (_plugins == null)
            {
                _plugins = GameObject.Find("PluginsController").GetComponent<PluginsProxy>();
            }

            return _plugins;
        }
    }

    public void init()
	{
		Shop.init();

		categoryControllers = new ShopCategory[categories.Length];

        for (int i = 0; i < categories.Length; i++)
		{
			ShopCategory category = categories[i].GetComponent<ShopCategory>();
			categoryControllers[i] = category;
			categories[i].SetActive(false);
            category.init();
			categoryBtns[i].OnClickUIItem += showCategory;
		}

        restoreBtn.OnClick += restoreTransactions;
	}

    void restoreTransactions()
    {
        soundController.playSound(soundController.click);
        plugins.restoreTransactions();
    }

    public void show(int categoryId)
	{
        curCategory = categories[categoryId];
        categoryControllers[categoryId].updateInfo();
	}

	GameObject curCategory
	{
		set
		{
			if (_curCategory != null)
				_curCategory.SetActive(false);
			_curCategory = value;
			_curCategory.SetActive(true);
            for (int i = 0; i < categories.Length; i++)
            {
                if (categories[i] == _curCategory)
                    categoryBtns[i].gameObject.GetComponentInChildren<tk2dSlicedSprite>().scale = new Vector3(1.3f, 1.3f);
                else
                    categoryBtns[i].gameObject.GetComponentInChildren<tk2dSlicedSprite>().scale = new Vector3(1, 1);
            }

            titleTF.text = _curCategory.GetComponent<ShopCategory>().title;
            titleTF.Commit();
		}
	}

	void showCategory(tk2dUIItem categoryBtn)
	{
        soundController.playSound(soundController.click);
        for (int i = 0; i < categoryBtns.Length; i++)
		{
			if (categoryBtns[i] == categoryBtn)
			{
				curCategory = categories[i];
				categoryControllers[i].updateInfo();
                break;
			}
		}
	}
	
}
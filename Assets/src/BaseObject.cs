using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using Holoville.HOTween;

public class BaseObject : MonoBehaviour 
{
    private const int PRECACHED_EXPLOSIONS_CNT = 15;
    private const int PRECACHED_FROZEN_MODELS_CNT = 5;

    static protected Stack<tk2dSpriteAnimator> _explosionPrefabs;
    static private List<tk2dSpriteAnimator> _curExplosions = new List<tk2dSpriteAnimator>();
    static protected Stack<GameObject> _frozenPrefabs;

	protected Vector3 _velocity;
	protected bool _success = false;
	protected Tweener _tweener;
	public bool fromLeft;
	protected Game _game;
    public bool needUpForce;
    public bool needCalcBounce;
    public bool needAlert;
    private bool _forceDestroy;
    protected SoundController _soundController;

    private int _bounceCnt;
    public int bounceCnt
    {
        get
        {
            return _bounceCnt;
        }
        set
        {
            if (needCalcBounce)
                _bounceCnt = value;
        }
    }

    private tk2dSpriteAnimator _explosion;
    public tk2dSpriteAnimator explosion
    {
        get
        {
            if (_explosion == null)
            {
                if (_explosionPrefabs.Count == 0)
                {
                    _explosion = Instantiate(Resources.Load("ExplosionAnimation", typeof(tk2dSpriteAnimator))) as tk2dSpriteAnimator;
                }
                else
                {
                    _explosion = _explosionPrefabs.Pop();
                    _explosion.gameObject.SetActive(true);
                }
            }
            _curExplosions.Add(_explosion);
            return _explosion;
        }
    }

    public static void init ()
    {
        _explosionPrefabs = new Stack<tk2dSpriteAnimator>();
        tk2dSpriteAnimator explosionPrefab;

        for (int i = 0; i < PRECACHED_EXPLOSIONS_CNT; i++)
        {
            explosionPrefab = Instantiate(Resources.Load("ExplosionAnimation", typeof(tk2dSpriteAnimator))) as tk2dSpriteAnimator;
            explosionPrefab.gameObject.SetActive(false);
            _explosionPrefabs.Push(explosionPrefab);
        }

        _frozenPrefabs = new Stack<GameObject> ();
        GameObject prefab;
        for (int i = 0; i < PRECACHED_FROZEN_MODELS_CNT; i++)
        {
            prefab = Instantiate(Resources.Load("fruits/FrozenFruit", typeof(GameObject))) as GameObject;
            prefab.transform.localScale = Vector3.one;
            prefab.SetActive(false);
            _frozenPrefabs.Push(prefab);
        }
    }

	protected virtual void Update () 
	{
        if (GameManager.GAME_RUNNING == false)
            return;

		if (Time.timeScale == 0.0f || success)
			return;

		if (Mathf.Abs(gameObject.rigidbody.velocity.x) < Settings.minVelocityHorizontal)
		{
			gameObject.rigidbody.AddForce(_velocity * Time.deltaTime, ForceMode.VelocityChange);
		}

        if (needUpForce)
        {
            Vector3 oldVelocity = gameObject.rigidbody.velocity;
            oldVelocity.y = Settings.minVelocityVertical;
            gameObject.rigidbody.velocity = oldVelocity;
            needUpForce = false;
        }
	}

    void OnDisable()
    {
        if (_tweener != null)
        {
            _tweener.Kill();
            _tweener = null;
        }
    }


	public void changeDirection()
	{
		fromLeft = !fromLeft;
		_velocity *= -1;
		initCollisions();
	}

    public virtual void start()
    {
        if (_explosion != null)
        {
            _explosion.Stop();
            _explosion.gameObject.SetActive(false);
            _explosionPrefabs.Push(_explosion);
            if (_curExplosions.Contains(_explosion))
                _curExplosions.Remove(_explosion);
            _explosion = null;
        }
        if (_soundController == null)
        {
            _soundController = GameObject.Find("SoundController").GetComponent<SoundController>();
        }
        _bounceCnt = 0;
        needCalcBounce = false;
        needUpForce = false;
        needAlert = true;
        forceDestroy = false;
    }

	public void initCollisions()
	{
        if (_game.leftCollider.gameObject.activeSelf && _game.rightCollider.gameObject.activeSelf && gameObject.collider.enabled)
		{
			Physics.IgnoreCollision(gameObject.collider, _game.leftCollider.collider, fromLeft);
			Physics.IgnoreCollision(gameObject.collider, _game.rightCollider.collider, !fromLeft);
		}
	}

	protected virtual void apply()
	{
		gameObject.collider.enabled = false;
		gameObject.rigidbody.isKinematic = true;

		float neededY = Game.MIN_Y - 1;
		float time = (gameObject.transform.position.y - neededY) / gameObject.rigidbody.velocity.y;
		if (time > 0)
			time *= -1;

		float neededX = gameObject.transform.position.x - gameObject.rigidbody.velocity.x * time;
		neededX = Mathf.Max(Game.END_MIN_X, neededX);
		neededX = Mathf.Min(Game.END_MAX_X, neededX);
		Vector3 neededPosition = new Vector3(neededX, neededY, gameObject.transform.position.z);
        Quaternion neededRotation = transform.rotation * Quaternion.Euler(gameObject.rigidbody.angularVelocity * Settings.fruitAngularSlow);

		TweenParms tweenParams = new TweenParms();
		tweenParams.Prop("position", neededPosition); 
		tweenParams.Prop("rotation", neededRotation); 
		tweenParams.Ease(EaseType.EaseOutExpo);
		_tweener = HOTween.To(gameObject.transform, Settings.fruitSlow, tweenParams);
	}

	public bool canDestroy
	{
		get;
		set;
	}

    public bool success
    {
        get
        {
            return _success;
        }
        set
        {
            if (_success == value)
                return;
            _success = value;
            if (value)
                apply();
        }
    }

    public void playExplosion()
    {
        explosion.gameObject.SetActive(true);
        Vector3 position = gameObject.transform.position;
        position.z = 0;
        explosion.transform.position = position;
        explosion.Play();
        explosion.AnimationCompleted = explosionCompleteDelegate;
        _soundController.playSound(_soundController.objectExplosion);
    }

    void explosionCompleteDelegate(tk2dSpriteAnimator sprite, tk2dSpriteAnimationClip clip) 
    {
        _explosion.gameObject.SetActive(false);
        _explosionPrefabs.Push(_explosion);
        if (_curExplosions.Contains(_explosion))
            _curExplosions.Remove(_explosion);
        _explosion = null;
    }

    static public void deactivateExplosions()
    {
        for (int i = 0; i < _curExplosions.Count; i++)
        {
            _curExplosions[i].gameObject.SetActive(false);
            _explosionPrefabs.Push(_curExplosions[i]);
        }

        _curExplosions.Clear();
    }

    public bool forceDestroy
	{
		get
		{
            return _forceDestroy;
		}
		set
		{
            if (_forceDestroy == value)
                return;
            _forceDestroy = value;
            if (_forceDestroy && !success)
                playExplosion();
		}
	}
}
using UnityEngine;
using System.Collections;
using System.Xml;

public class AddTime : Bonus {

	protected override void apply () 
	{
		_soundController.playSound(_soundController.bonusAddLive);
		base.apply();
		_game.lifebar.curValue -= (int)_bonus;
	}


	public override void startBonus()
	{
		base.startBonus();

		if (UserData.getUpgradeLevel(Upgrade.ADD_TIME_BONUS) != 0)
		{
			_bonus = UserData.getUpgradeValue(Upgrade.ADD_TIME_BONUS);
		}
	}
}

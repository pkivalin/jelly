using UnityEngine;
using System.Collections;
using System.Xml;

public class LockJelly : Bonus {

	protected override void apply () 
	{
		_soundController.playSound(_soundController.bonusFreeze);
		base.apply();
        _game.lockJelly((int)_bonus);
	}

    protected override void Update () 
    {
        if (GameManager.GAME_RUNNING == false)
            return;

        base.Update();

        if (Time.timeScale == 0.0f || success)
            return;

        gameObject.rigidbody.AddForce(Vector3.up * _compensatingForce * Time.deltaTime, ForceMode.Force);
    }

    private int _compensatingForce;

    public override void startBonus()
    {
        base.startBonus();

        _compensatingForce = int.Parse(_settings["compensatingForce"].InnerText);
    }
}

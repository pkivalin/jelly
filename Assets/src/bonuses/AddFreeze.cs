using UnityEngine;
using System.Collections;
using System.Xml;

public class AddFreeze : Bonus {

    static public int INDEX = 2;

	protected override void apply () 
	{
		_soundController.playSound(_soundController.bonusFreeze);
		canDestroy = false;
		base.apply();
        bonusInfo.setSprite(BonusInfo.SPRITE_FREEZE);
        bonusInfo.show(_bonus, this);
        _needDecreaseTimeScale = true;
	}

    public override void resetBonus()
    {
        Debug.Log("resetBonus");
        _needIncreaseTimeScale = true;
    }

    protected override void Update () 
    {
        if (GameManager.GAME_RUNNING == false)
            return;

        base.Update();

        if (Time.timeScale == 0.0f)
            return;

        if (_needIncreaseTimeScale)
        {
            if (_scaleDelay == 0)
                Time.timeScale = 1;
            else
                Time.timeScale = Mathf.Min(1, Time.timeScale + (1 - _timeScale) * Time.deltaTime / Time.timeScale /_scaleDelay);
            if (Time.timeScale == 1)
            {
                _needIncreaseTimeScale = false;
                canDestroy = true;
            }
        }
        else if (_needDecreaseTimeScale)
        {
            if (_scaleDelay == 0)
                Time.timeScale = _timeScale;
            else
                Time.timeScale = Mathf.Max(_timeScale, Time.timeScale - (1 - _timeScale) * Time.deltaTime / Time.timeScale /_scaleDelay);
            if (Time.timeScale == _timeScale)
            {
                _needDecreaseTimeScale = false;
            }
        }
    }

	private float _timeScale;
    private float _scaleDelay;
    private bool _needIncreaseTimeScale;
    private bool _needDecreaseTimeScale;

	public override void startBonus()
	{
		base.startBonus();

        _needDecreaseTimeScale = false;
        _needIncreaseTimeScale = false;

        _timeScale = float.Parse(_settings["timeScale"].InnerText);
        _scaleDelay = float.Parse(_settings["scaleDelay"].InnerText);

		if (UserData.getUpgradeLevel(Upgrade.ADD_FREEZE_BONUS) != 0)
		{
			_bonus = UserData.getUpgradeValue(Upgrade.ADD_FREEZE_BONUS);
		}
	}
}

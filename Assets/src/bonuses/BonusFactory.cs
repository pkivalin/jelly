using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class BonusFactory : MonoBehaviour
{
	private Game _game;
    private List<GameObject> _bonuses = new List<GameObject>();
    private List<Bonus> _bonuseControlers = new List<Bonus>();
	private float _bonusDelay;
	private XmlNode _settings;

	private int _bonusesCnt;
	public int bonusesCnt 
	{
		get 
		{
			return _bonusesCnt;
		}
	}

	public void init(Game game)
	{
		_game = game;
		_bonusesCnt = 0;

		_settings = Settings.getBonuses();
        GameObject bonus;

        for (int i = 0; i < Bonus.TYPES.Length; i++)
        {
            bonus = Instantiate(Resources.Load("bonuses/" + Bonus.TYPES[i], typeof(GameObject))) as GameObject;
            _bonuses.Add(bonus);
            _bonuseControlers.Add(bonus.GetComponent<Bonus>());
            _bonuseControlers[i].init(_settings.SelectSingleNode(Bonus.TYPES[i]), _game);
            bonus.SetActive(false);
        }
	}

    private float _getBonusDelay()
	{
		if (UserData.getUpgradeLevel(Upgrade.BONUS_DELAY) == 0)
            return Settings.bonusDelay; 
		
        return UserData.getUpgradeValue(Upgrade.BONUS_DELAY);
	}

    public void startGame()
    {
        _bonusDelay = Settings.bonusFirstDelay;
    }

	void Update()
	{
        if (GameManager.GAME_RUNNING == false )
            return;

		if (Time.timeScale == 0.0f)
			return;

        if (GameManager.TUTORIAL_RUNNING)
            return; // не добавляем бонусы в туториале

		_bonusDelay -= Time.deltaTime;
		if (_bonusDelay <= 0)
		{
            StartCoroutine(addBonus());
            _bonusDelay = _getBonusDelay();
		}

		GameObject bonus;
        Bonus bonusController;
		int gap = 0;

		for (int i = 0; i < _bonuses.Count; i++)
		{
            bonus = _bonuses[i];

            if (bonus.activeSelf == false)
                continue;
			
            bonusController = _bonuseControlers[i];

            if (bonus.transform.position.y < Game.MIN_Y && bonusController.canDestroy || bonusController.forceDestroy)
            {
                if (bonusController.success == false)
                {
                    _bonusDelay -= _getBonusDelay() / 4.0f; // если предыдущий не поймали - задержку между бонусами уменьшаем на 25%
                }
                _bonuses[i].SetActive(false);
            }

            gap += Bonus.GAP;

			Vector3 newPosition = bonus.transform.position;
			newPosition.z = gap;
			bonus.transform.position = newPosition;
		}
	}

    IEnumerator addBonus()
	{
        bool[] validBonuses = new bool[Bonus.TYPES.Length];
        for (int i = 0; i < validBonuses.Length; i++)
            validBonuses[i] = true;

        if (Game.GAME_TYPE == Game.GAME_TYPE_TIME || _game.lifebar.curValue == 0)
            validBonuses[AddLive.INDEX] = false; // не даем жизни

        if (Game.GAME_TYPE == Game.GAME_TYPE_TIME) // в режиме на время только джекпот
        {
            validBonuses[AddBorders.INDEX] = false;
            validBonuses[AddFreeze.INDEX] = false;
        }

        if (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE)
            validBonuses[AddFruits.INDEX] = false; // не даем фрукты

        if (Game.MECHANIC == Level.MECHANIC_BORDERS)
            validBonuses[AddBorders.INDEX] = false; // не даем стенки

        if (Game.MECHANIC == Level.MECHANIC_FREEZE)
            validBonuses[AddFreeze.INDEX] = false; // не даем заморозку

        int bonusIndex = 0;

        int validBonusesCnt = 0;

        for (int i = 0; i < validBonuses.Length; i++)
            if (validBonuses[i])
                validBonusesCnt++; // считаем сколько всего видов бонуса доступно

        int bonusRandom = Random.Range(0, validBonusesCnt);

        for (int i = 0; i < validBonuses.Length; i++)
		{
            if (validBonuses[i])
			{
                if (bonusRandom == 0)
                {
                    bonusIndex = i;
                    break;
                }

                bonusRandom--;
            }
		}

        if (_bonuses[bonusIndex].activeSelf)
        {
            yield return new WaitForSeconds(1);
            StartCoroutine(addBonus()); // пробуем выдать другой бонус
        }
        else if (GameManager.GAME_RUNNING)
        {
            _bonuses[bonusIndex].SetActive(true);
            _bonuseControlers[bonusIndex].startBonus();
        }

	}

    public void gameOver()
    {
        StartCoroutine(_gameOver());
    }

    public void reset()
    {
        for (int i = 0; i < _bonuses.Count; i++) 
            _bonuses[i].SetActive(false);
    }

    public IEnumerator _gameOver()
    {
        for (int i = 0; i < _bonuses.Count; i++) 
        {
            if (_bonuses[i].activeSelf)
            {
                _bonuseControlers[i].playExplosion();
                yield return new WaitForSeconds(0.1f);
            }
        }
    }

    public void resetAllBonuses()
    {
        for (int i = 0; i < _bonuses.Count; i++) 
        {
            if (_bonuses[i].activeSelf)
            {
                _bonuses[i].SetActive(false);
            }
        }
    }
}


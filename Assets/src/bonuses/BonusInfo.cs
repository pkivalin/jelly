﻿using UnityEngine;
using System.Collections;

public class BonusInfo : MonoBehaviour
{
    private const float TWEEN_DURATION_IN = 0.5f;
    private const float TWEEN_DURATION_OUT = 0.4f;
    public const string SPRITE_FREEZE = "feezerBonusGameIcon";
    public const string SPRITE_BORDERS = "barrierBonusGameIcon";

    public tk2dSprite sprite;
    public tk2dTextMesh timer;
    public TweenedObject tweener;

    private float _timeLeft;
    private bool _isActive = false;
    private Bonus _bonus;

    void Update()
    {
        if (_isActive == false || Time.timeScale == 0 || GameManager.GAME_RUNNING == false)
            return;

        _timeLeft -= Time.deltaTime / Time.timeScale;
        timer.text = _timeLeft + 0.9f < 10 ? "0" : "";
        timer.text += ((int)(_timeLeft + 0.9f)).ToString();
        timer.Commit();
        if (_timeLeft <= 0)
            hide();
    }

    public void reset()
    {
        _isActive = false;
        gameObject.SetActive(false);
    }

    public void show(float duration, Bonus bonus)
    {
        _bonus = bonus;
        _timeLeft = duration;
        _isActive = true;
        tweener.tween(1, TWEEN_DURATION_IN, false, 0, Holoville.HOTween.EaseType.EaseOutBack, 0.2f);
    }

    public void hide()
    {
        if (_isActive)
        {
            _isActive = false;
            _bonus.resetBonus();
            tweener.tween(0, TWEEN_DURATION_OUT, true, 1, Holoville.HOTween.EaseType.EaseInBack);
        }
    }

    public void setSprite(string spriteName)
    {
        sprite.SetSprite(spriteName);
    }
}

using UnityEngine;
using System.Collections;
using System.Xml;

public class AddFruits : Bonus 
{

    static public int INDEX = 1;

	protected override void apply () 
	{
		_soundController.playSound(_soundController.bonusAddFruits);
		base.apply();
		_game.fruitFactory.addFruitsToQueue((int)_bonus, _fruitInterval, false, _fruitHorizontalForce, _fruitVerticalForce);
		_game.fruitFactory.removeAllBugs();
	}

	private Vector2 _fruitHorizontalForce;
	private Vector2 _fruitVerticalForce;
	private Vector2 _fruitInterval;

	public override void startBonus()
	{
		base.startBonus();
		_fruitHorizontalForce = new Vector2(float.Parse(_settings["fruits"]["force"]["horizontal"]["min"].InnerText)
		                                    , float.Parse(_settings["fruits"]["force"]["horizontal"]["max"].InnerText));
		_fruitVerticalForce = new Vector2(float.Parse(_settings["fruits"]["force"]["vertical"]["min"].InnerText)
		                                  , float.Parse(_settings["fruits"]["force"]["vertical"]["max"].InnerText));
		_fruitInterval = new Vector2(float.Parse(_settings["fruits"]["fruitInterval"]["min"].InnerText)
		                             , float.Parse(_settings["fruits"]["fruitInterval"]["max"].InnerText));

		if (UserData.getUpgradeLevel(Upgrade.ADD_FRUIT_BONUS) != 0)
		{
			_bonus = UserData.getUpgradeValue(Upgrade.ADD_FRUIT_BONUS);
		}
	}

}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;

public class AddLive : Bonus {

    static public int INDEX = 0;

	protected override void apply () 
	{
		_soundController.playSound(_soundController.bonusAddLive);
		base.apply();
		_game.lifebar.curValue -= (int)_bonus;
        _game.lifebar.gameObject.GetComponent<PulsationLifebarAnimator>().startPulsation();
	}

	public override void startBonus()
	{
		base.startBonus();

		if (UserData.getUpgradeLevel(Upgrade.ADD_LIVE_BONUS) != 0)
		{
			_bonus = UserData.getUpgradeValue(Upgrade.ADD_LIVE_BONUS);
		}
	}

}

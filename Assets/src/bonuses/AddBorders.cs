using UnityEngine;
using System.Collections;
using System.Xml;

public class AddBorders : Bonus 
{

    static public int INDEX = 3;

	protected override void apply() 
	{
		canDestroy = false;
		_soundController.playSound(_soundController.bonusBorders);
		base.apply();
        _game.activateBorders(true);
        bonusInfo.setSprite(BonusInfo.SPRITE_BORDERS);
        bonusInfo.show(_bonus, this);
	}

    public override void resetBonus()
    {
        Debug.Log("resetBonus");
        if (gameObject.activeSelf)
        {
            _game.deactivateBorders();
            canDestroy = true;
        }
    }

	public override void startBonus()
	{
		base.startBonus();

		if (UserData.getUpgradeLevel(Upgrade.ADD_BORDERS_BONUS) != 0)
		{
			_bonus = UserData.getUpgradeValue(Upgrade.ADD_BORDERS_BONUS);
		}
	}
}

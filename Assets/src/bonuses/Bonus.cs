using UnityEngine;
using System.Collections;
using System.Xml;

public class Bonus : BaseObject {

    const float ANGULAR_VELOCITY = 2.0f;
    public const int GAP = 5;
    protected static BonusInfo _bonusInfo;

	protected Vector2 _horizontalForce;
	protected Vector2 _verticalForce;
	protected float _bonus;
	protected float _scale;
	protected float _bounciness;
	protected XmlNode _settings;

    protected override void apply () 
    {
        base.apply();
        GameManager.gamecenter.bonusCnt++;
    }

    static public string[] TYPES = {
        "AddLive"
        , "AddFruits"
        , "AddFreeze"
        , "AddBorders"
    };

    public static BonusInfo bonusInfo
    {
        get
        {
            if (_bonusInfo == null)
                _bonusInfo = Instantiate(Resources.Load("BonusInfo", typeof(BonusInfo))) as BonusInfo;
            return _bonusInfo;
        }
    }

    public virtual void resetBonus()
    {
    }

    public void init(XmlNode settings, Game game)
    {
        _game = game;
        _settings = settings;
    }

	public virtual void startBonus()
	{
        base.start();
		gameObject.rigidbody.isKinematic = false;
		
		_horizontalForce = new Vector2(float.Parse(_settings["force"]["horizontal"]["min"].InnerText)
		                               , float.Parse(_settings["force"]["horizontal"]["max"].InnerText));

        _horizontalForce *= Settings.forceMultiplyHorizontal;

		_verticalForce = new Vector2(float.Parse(_settings["force"]["vertical"]["min"].InnerText)
		                             , float.Parse(_settings["force"]["vertical"]["max"].InnerText));

		_bonus = float.Parse(_settings["bonus"].InnerText);
		_bounciness = float.Parse(_settings["bounciness"].InnerText);
		_scale = float.Parse(_settings["scale"].InnerText);

		gameObject.rigidbody.velocity = Vector3.zero;
		gameObject.collider.enabled = true;
		gameObject.transform.localScale = new Vector3(_scale, _scale, _scale);

		canDestroy = true;
		success = false;
		fromLeft = Random.Range(0, 2) == 0 ? true : false;

		float startX = fromLeft ? Game.START_MIN_X : Game.START_MAX_X;

		gameObject.transform.localPosition = new Vector3(startX, Random.Range(Settings.startHeight.x, Settings.startHeight.y), 0);
		_velocity = (fromLeft ? Vector3.right : - Vector3.right) * Random.Range(_horizontalForce.x, _horizontalForce.y);

		gameObject.rigidbody.AddForce(_velocity, ForceMode.Impulse);

		gameObject.rigidbody.AddForce(Vector3.up * Random.Range(_verticalForce.x, _verticalForce.y), ForceMode.Impulse);

        gameObject.rigidbody.angularVelocity = new Vector3(Random.Range(ANGULAR_VELOCITY / 2, ANGULAR_VELOCITY)
            , Random.Range(ANGULAR_VELOCITY / 2, ANGULAR_VELOCITY)
            , 0);

		gameObject.collider.material.bounciness = _bounciness;
		initCollisions();
	}
	
}

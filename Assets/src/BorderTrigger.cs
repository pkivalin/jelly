﻿using UnityEngine;
using System.Collections;

public class BorderTrigger : MonoBehaviour {

	public bool isLeft;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider collider)
	{
		BaseObject baseObject = collider.GetComponent<BaseObject>();
        if (!baseObject)
            return;

		if (baseObject.fromLeft && !isLeft || !baseObject.fromLeft && isLeft)
		{
            baseObject.canDestroy = true;
		}
	}

}

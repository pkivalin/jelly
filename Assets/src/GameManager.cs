﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GameManager : MonoBehaviour {

    static public bool GAME_RUNNING = false;
    static public bool TUTORIAL_RUNNING = false;
    static public bool HIDE_COMBO = false;

    #if UNITY_IOS && !UNITY_EDITOR
    static public GamecenterProxy gamecenter = new IOSGamecenterProxy();
    #elif UNITY_ANDROID && !UNITY_EDITOR
    static public GamecenterProxy gamecenter = new AndroidGamecenterProxy();
    #else
    static public GamecenterProxy gamecenter = new GamecenterProxy();
    #endif

    static public bool isFirstPlay;

    public GameObject gameScreen;
    public GameObject menuScreen;
    public Game game;
    public MainMenuController mainMenu;
    public SoundController soundController;
    public bool cheatsEnabled;

    void Start()
    {
        Application.targetFrameRate = 30;
        isFirstPlay = UserData.isFirstPlay;
        gamecenter.init();
        GAME_RUNNING = false;
        HOTween.Init(true, true, true);
        game.init();
        BaseObject.init();
        gameScreen.SetActive(false);
        menuScreen.SetActive(true);
        soundController.playMenuTheme();
        Bonus.bonusInfo.gameObject.SetActive(false);
    }

    public void startGame()
    {
        gameScreen.SetActive(true);
        Game.LEVEL_CHANGED = true;
        menuScreen.SetActive(false);
      
        soundController.playMainTheme();

        game.startGame();
    }

    public void showMenu()
    {
        GAME_RUNNING = false;
        BaseObject.deactivateExplosions();
        game.resetLevel();
        gameScreen.SetActive(false);
        menuScreen.SetActive(true);
        mainMenu.showMainMenu();
        soundController.playMenuTheme();
    }

    public void showShop()
    {
        BaseObject.deactivateExplosions();
        gameScreen.SetActive(false);
        menuScreen.SetActive(true);
        mainMenu.showShopInGame();
    }

    public void closeShop()
    {
        gameScreen.SetActive(true);
        menuScreen.SetActive(false);
    }

}

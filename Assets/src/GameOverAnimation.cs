﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class GameOverAnimation : MonoBehaviour 
{
    private const float FIRST_TWEEN_DURATION = 0.27f;
    private const float SECOND_TWEEN_DURATION = 0.4f;
    private const float THIRD_TWEEN_DURATION = 0.4f;
    private const float LOOSE_SCREEN_DELAY = FIRST_TWEEN_DURATION + SECOND_TWEEN_DURATION + THIRD_TWEEN_DURATION;

    public LooseScreenController looseScreen;
    public TweenedObject looseScreenTweener;
    public tk2dSprite textSprite;
    private FruitFactory _fruitFactory;
    private BonusFactory _bonusFactory;
    private PulsationLifebarAnimator _lifebarPulsation;
    private PulsationLifebarAnimator _clockPulsation;
    private TweenParms _tween1;
    private TweenParms _tween2;
    private TweenParms _tween3;
    private Color _lastColor = Color.white;
    private Vector3 _newScale = new Vector3(0.0f, 0.0f, 0.0f);
    private Sequence _sequence;
    private int _fruitsCnt;

    private PluginsProxy _plugins;
    public PluginsProxy plugins
    {
        get
        {
            if (_plugins == null)
            {
                _plugins = GameObject.Find("PluginsController").GetComponent<PluginsProxy>();
            }

            return _plugins;
        }
    }

    void Awake()
    {
        _lastColor.a = 0.0f;

        _tween1 = new TweenParms();
        _tween1.Prop("scale", new Vector3(1.0f, 1.0f, 1.0f)); 
        _tween1.Ease(EaseType.Linear);

        _tween2 = new TweenParms();
        _tween2.Prop("scale", new Vector3(1.2f, 1.2f, 1.2f));
        _tween2.Ease(EaseType.Linear);

        _tween3 = new TweenParms();
        _tween3.Prop("color", _lastColor);
        _tween3.Prop("scale", new Vector3(1.3f, 1.3f, 1.3f)); 
        _tween3.Ease(EaseType.Linear);

        _sequence = new Sequence();
        _sequence.Append(HOTween.To(textSprite, FIRST_TWEEN_DURATION, _tween1));
        _sequence.Append(HOTween.To(textSprite, SECOND_TWEEN_DURATION, _tween2));
        _sequence.Append(HOTween.To(textSprite, THIRD_TWEEN_DURATION, _tween3));
        _sequence.autoKillOnComplete = false;
    }

    public void init(Game game, FruitFactory fruitFactory, BonusFactory bonusFactory, PulsationLifebarAnimator lifebarPulsation, PulsationLifebarAnimator clockPulsation)
    {
        looseScreen.init(game);
        _fruitFactory = fruitFactory;
        _bonusFactory = bonusFactory;
        _lifebarPulsation = lifebarPulsation;
        _clockPulsation = clockPulsation;
    }

    public int startExplosions(bool win = false)
    {
        textSprite.gameObject.SetActive(false);

        if (Game.GAME_TYPE == Game.GAME_TYPE_TIME)
            _clockPulsation.startPulsation();
        else if (Game.GAME_TYPE == Game.GAME_TYPE_SURVIVAL || win == false)
            _lifebarPulsation.startPulsation();

        looseScreen.gameObject.SetActive(false);
        _bonusFactory.gameOver();
        _fruitsCnt = 0;
        return _fruitFactory.gameOver();
    }

    public void startAfterExplosions(bool win = false)
    {
        _bonusFactory.resetAllBonuses();
        textSprite.gameObject.SetActive(true);
        textSprite.color = Color.white;
        textSprite.scale = _newScale;
        if (Game.GAME_TYPE == Game.GAME_TYPE_SURVIVAL)
            textSprite.SetSprite("gameOver");
        else if (Game.GAME_TYPE == Game.GAME_TYPE_TIME)
            textSprite.SetSprite("time_over");
        else if (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE && win)
            textSprite.SetSprite("wellDone");
        else 
            textSprite.SetSprite("rty_again");

        StartCoroutine(tweens());
    }

    public void start(bool win)
    {
        _fruitsCnt = startExplosions(win);
        startAfterExplosions(win);
    }

    IEnumerator tweens()
    {
        yield return new WaitForSeconds(_fruitsCnt * 0.1f); // ждем окончания взрывов

        _sequence.Restart();

        yield return new WaitForSeconds(LOOSE_SCREEN_DELAY);

        if (plugins.needBanner())
        {
            yield return new WaitForSeconds(0.2f);
            plugins.endLevel();
            yield return new WaitForSeconds(0.3f);
        }

        looseScreenTweener.tween(1, 0.5f, false, 0, Holoville.HOTween.EaseType.EaseOutQuad);
    }


}

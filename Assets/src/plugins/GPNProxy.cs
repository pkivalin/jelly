using UnityEngine;
using System.Collections;

public class GPNProxy : MonoBehaviour, ICrossPromotionPluginDelegate {
        
    public bool needRestart = false;
    public bool loaded = false;

    void Start () 
    {
        Plugin.Delegate = this;
    }

    #region ICrossPromotionPluginDelegate

        // Interstitial ad is received: it’s safe to present it now
    public void OnInterstitialReceived(CrossPromotionPlugin plugin) 
    {
        loaded = true;
        Debug.Log("Interstitial received");
    }

    // Interstitial ad failed to receive
    public void OnInterstitialFailed(CrossPromotionPlugin plugin)
    {
        needRestart = true;
        Debug.LogError("Interstitial failed");
    }

    // Interstitial ad presented a modal view: you can pause your game here
    public void OnInterstitialOpened(CrossPromotionPlugin plugin) 
    {
        Debug.Log("Interstitial opened");
    }

    // Interstitial ad closed a modal view: you can resume your game here
    public void OnInterstitialClosed(CrossPromotionPlugin plugin) 
    {
        Debug.Log("Interstitial closed");
    }

    #endregion
        
    #region CrossPromotion helper

    public void StartRequestingInterstitials() 
    {
        Plugin.StartRequestingInterstitials();
    }

    public void PresentInterstitial() 
    {
        CrossPromotionResult result = Plugin.PresentInterstitial();
        if (result != CrossPromotionResult.Presented) {
            Debug.Log("Interstitial not presented");
        }
    }

    private CrossPromotionPlugin Plugin {
        get { return gameObject.GetComponent<CrossPromotionPlugin>(); }
    }

    #endregion
}
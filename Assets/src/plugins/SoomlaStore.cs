using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Soomla;

public class SoomlaStore : IStoreAssets{

    public int GetVersion() {
        return 0;
    }

    public VirtualCurrency[] GetCurrencies() 
    {
        return new VirtualCurrency[]{COINS_CURRENCY};
    }

    public VirtualGood[] GetGoods() 
    {
        return new VirtualGood[] {};
    }

    public VirtualCurrencyPack[] GetCurrencyPacks() 
    {
        return new VirtualCurrencyPack[] {STACK_OF_COINS, PILE_OF_COINS, BAG_OF_COINS, BOX_OF_COINS};
    }

    public VirtualCategory[] GetCategories() 
    {
        return new VirtualCategory[]{GENERAL_CATEGORY};
    }

    public NonConsumableItem[] GetNonConsumableItems() 
    {
        return new NonConsumableItem[]{REMOVE_ADS, AUTOCOOLING_UPGRADE1, AUTOCOOLING_UPGRADE2, DOUBLE_COINS_UPGRADE};
    }

    /*        * Static Final members **/
    public const string COINS_CURRENCY_ITEM_ID      = "coins_currency";
    public const string STACK_OF_COINS_PRODUCT_ID = "net.kivalin.jelly.stackofcoins";
    public const string PILE_OF_COINS_PRODUCT_ID      = "net.kivalin.jelly.pileofcoins";
    public const string BAG_OF_COINS_PRODUCT_ID    = "net.kivalin.jelly.bagofcoins";
    public const string BOX_OF_COINS_PRODUCT_ID = "net.kivalin.jelly.boxofcoins";
    public const string AUTOCOOLING_UPGRADE1_PRODUCT_ID = "net.kivalin.jelly.autocooling1";
    public const string AUTOCOOLING_UPGRADE2_PRODUCT_ID = "net.kivalin.jelly.autocooling2";
    public const string DOUBLECOINS_UPGRADE_PRODUCT_ID = "net.kivalin.jelly.doublecoins";
    public const string REMOVE_ADS_PRODUCT_ID   = "net.kivalin.jelly.removeads";

    /*        * Virtual Currencies **/
    public static VirtualCurrency COINS_CURRENCY = new VirtualCurrency(
        "Coins",
        "",
        COINS_CURRENCY_ITEM_ID
    );

    /*        * Virtual Currency Packs **/

    public static VirtualCurrencyPack STACK_OF_COINS = new VirtualCurrencyPack(
        "Stack of coins",                                   // name
        "Stack of 8000 coins",                       // description
        STACK_OF_COINS_PRODUCT_ID,                                   // item id
        8000,                                         // number of currencies in the pack
        COINS_CURRENCY_ITEM_ID,                        // the currency associated with this pack
        new PurchaseWithMarket(STACK_OF_COINS_PRODUCT_ID, 0.99)
    );

    public static VirtualCurrencyPack PILE_OF_COINS = new VirtualCurrencyPack(
        "Pile of coins",                                   // name
        "Pile of 50000 coins",                       // description
        PILE_OF_COINS_PRODUCT_ID,                                   // item id
        50000,                                         // number of currencies in the pack
        COINS_CURRENCY_ITEM_ID,                        // the currency associated with this pack
        new PurchaseWithMarket(PILE_OF_COINS_PRODUCT_ID, 4.99)
    );

    public static VirtualCurrencyPack BAG_OF_COINS = new VirtualCurrencyPack(
        "Bag of coins",                                   // name
        "Bag of 220000 coins",                       // description
        BAG_OF_COINS_PRODUCT_ID,                                   // item id
        220000,                                         // number of currencies in the pack
        COINS_CURRENCY_ITEM_ID,                        // the currency associated with this pack
        new PurchaseWithMarket(BAG_OF_COINS_PRODUCT_ID, 19.99)
    );

    public static VirtualCurrencyPack BOX_OF_COINS = new VirtualCurrencyPack(
        "Box of coins",                                   // name
        "Box of 600000 coins",                       // description
        BOX_OF_COINS_PRODUCT_ID,                                   // item id
        600000,                                         // number of currencies in the pack
        COINS_CURRENCY_ITEM_ID,                        // the currency associated with this pack
        new PurchaseWithMarket(BOX_OF_COINS_PRODUCT_ID, 49.99)
    );

    /*        * Virtual Categories **/
    // The muffin rush theme doesn't support categories, so we just put everything under a general category.
    public static VirtualCategory GENERAL_CATEGORY = new VirtualCategory(
        "General", new List<string>(new string[] {})
    );


    /*        * Google MANAGED Items **/

    public static NonConsumableItem REMOVE_ADS  = new NonConsumableItem(
        "Remove Ads",
        "Remove all Ads",
        REMOVE_ADS_PRODUCT_ID,
        new PurchaseWithMarket(new MarketItem(REMOVE_ADS_PRODUCT_ID, MarketItem.Consumable.NONCONSUMABLE , 0.99))
    );

    public static NonConsumableItem AUTOCOOLING_UPGRADE1  = new NonConsumableItem(
        "Autocooling upgrade 1",
        "Autocooling upgrade 1",
        AUTOCOOLING_UPGRADE1_PRODUCT_ID,
        new PurchaseWithMarket(new MarketItem(AUTOCOOLING_UPGRADE1_PRODUCT_ID, MarketItem.Consumable.NONCONSUMABLE , 1.99))
    );

    public static NonConsumableItem AUTOCOOLING_UPGRADE2  = new NonConsumableItem(
        "Autocooling upgrade 2",
        "Autocooling upgrade 2",
        AUTOCOOLING_UPGRADE2_PRODUCT_ID,
        new PurchaseWithMarket(new MarketItem(AUTOCOOLING_UPGRADE2_PRODUCT_ID, MarketItem.Consumable.NONCONSUMABLE , 1.99))
    );

    public static NonConsumableItem DOUBLE_COINS_UPGRADE  = new NonConsumableItem(
        "Double coins",
        "Double coins",
        DOUBLECOINS_UPGRADE_PRODUCT_ID,
        new PurchaseWithMarket(new MarketItem(DOUBLECOINS_UPGRADE_PRODUCT_ID, MarketItem.Consumable.NONCONSUMABLE , 3.99))
    );

}


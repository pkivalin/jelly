using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using PlayHaven;
using Chartboost;
using System;

public class PluginsProxy : MonoBehaviour 
{
    private const float AD_LOAD_DELAY = 10;
    static public bool INTERNET_AVAILABLE = true;
    static public string FLURRY_KEY = "";

    public SoomlaProxy soomlaProxy;
    public GPNProxy gpnProxy;

    private bool _isEnabled = false;
    private bool _isInited = false;
    private AdcolonyProxy _adcolonyProxy;
    private NerdFlurry _flurryProxy;
    private bool _playhavenHasLoadedBanner;
    private float _adDelay = 0;


    #if UNITY_EDITOR_OSX || UNITY_EDITOR || UNITY_STANDALONE || UNITY_STANDALONE_WIN

    #elif UNITY_ANDROID


    void Start () 
    {
    _initPlugins();
    //StartCoroutine(_checkConnection());
    }

    void OnApplicationPause(bool pause)
    {
    if(!pause && _isInited)
    {
    PlayHavenManager.instance.OpenNotification();
    CBBinding.init();
    }
    }

    void Update()
    {
    _adDelay -= Time.deltaTime;
    if (Input.GetKeyUp(KeyCode.Escape)) 
    {
    if (_isInited && CBBinding.onBackPressed())
    return;
    }
    }

    #elif UNITY_IPHONE
    void Awake() 
    {
    _initPlugins();
    //StartCoroutine(_checkConnection());
    }

    void Update()
    {
    _adDelay -= Time.deltaTime;
    }

    #endif



    public void _initPlugins()
    {
        _isEnabled = true;
        Debug.Log("C# _initPlugins");
        _isInited = true;
        Instantiate(Resources.Load("PlayHavenManager", typeof(GameObject)));
        _adcolonyProxy = Instantiate(Resources.Load("AdcolonyManager", typeof(AdcolonyProxy))) as AdcolonyProxy;
        _adcolonyProxy.init(this);

        _playhavenHasLoadedBanner = false;
        _flurryProxy = new NerdFlurry();

        #if UNITY_ANDROID && !UNITY_EDITOR
        _flurryProxy.StartSession("JM72YW3TBGQ2FBDYHBD5"); 
        CBBinding.init();
        #elif UNITY_IPHONE && !UNITY_EDITOR
        _flurryProxy.StartSession("6Z53ZPHHM5BVCV8PHNFD"); //Jelly Fruit
        CBBinding.init("52b19c4b9ddc3563be4aa104", "f79f2c583fc094606cfa3d86e1f865e78394abd9");
        #endif
        #if (UNITY_ANDROID || UNITY_IPHONE)  && !UNITY_EDITOR
        PlayHavenManager.Instance.OpenNotification();
        PlayHavenManager.Instance.OnSuccessPreloadRequest += OnPlayhavenRequestRecieved;
        PlayHavenManager.Instance.ContentPreloadRequest("end_level");
        CBBinding.cacheInterstitial(null);
        #endif



        gpnProxy.StartRequestingInterstitials();
    }

    void OnPlayhavenRequestRecieved(int requestId)
    {
        Debug.Log("!! playhaven banner received");
        _playhavenHasLoadedBanner = true;
    }

    void OnApplicationQuit()
    {
        #if UNITY_ANDROID || UNITY_IPHONE
        if (_flurryProxy != null)
            _flurryProxy.EndSession();
        #endif
        UserData.save();
    }

    public void logEvent(string message, bool timed = false) 
    {
        if (!_isEnabled)
            return;

        _flurryProxy.LogEvent(message, timed);
    }

    private Dictionary<string, string> _flurryParams;
    public Dictionary<string, string> flurryParams
    {
        get
        {
            if (_flurryParams == null)
                _flurryParams = new Dictionary<string, string>();
            if (!_isEnabled)
                _flurryParams.Clear();
            return _flurryParams;
        }
    }

    public void endTimedEvent(string eventName)
    {
        if (!_isEnabled)
            return;
        _flurryProxy.EndTimedEvent(eventName);
    }

    public void logEvent(string message, Dictionary<string, string> parameters, bool timed = false) 
    {
        if (!_isEnabled)
            return;
        _flurryProxy.LogEvent(message, parameters, timed);
        _flurryParams.Clear();
    }

    public bool needBanner()
    {
        if (!_isEnabled || UserData.getUpgradeLevel(Upgrade.REMOVE_ADS) != 0 || GameManager.isFirstPlay)
            return false;
        return true;
    }

    public void endLevel()
    {
        if (needBanner() == false)
            return;



        #if (UNITY_ANDROID || UNITY_IPHONE)  && !UNITY_EDITOR
        Debug.Log("C# hasCachedInterstitial = " + CBBinding.hasCachedInterstitial(null).ToString());

        if (gpnProxy.loaded)
        {
            gpnProxy.loaded = false;
            gpnProxy.PresentInterstitial();
            _adDelay = Settings.endLevelAdsDelay;
            flurryParams.Add("provider", "GPN");
        }
        else if (CBBinding.hasCachedInterstitial(null))
        {
            CBBinding.showInterstitial(null);
            _adDelay = Settings.endLevelAdsDelay;
            flurryParams.Add("provider", "chartboost");
        }
        else if (_playhavenHasLoadedBanner)
        {
        _playhavenHasLoadedBanner = false;
        PlayHavenManager.Instance.ContentRequest("end_level");

            _adDelay = Settings.endLevelAdsDelay;
        flurryParams.Add("provider", "playhaven");
        }
        else
        {
            flurryParams.Add("provider", "fail");
        }

        if (_playhavenHasLoadedBanner == false)
        PlayHavenManager.Instance.ContentPreloadRequest("end_level");

        if (gpnProxy.needRestart)
            gpnProxy.StartRequestingInterstitials();

        CBBinding.cacheInterstitial(null);

        logEvent("showFullscreenAd", flurryParams);

        #endif
    }

    public void restoreTransactions()
    {
        if (!_isEnabled)
            return;
        soomlaProxy.restoreTransactions();
    }

    public void openStore()
    {
        if (!_isEnabled)
            return;
        soomlaProxy.openStore();
    }

    public void closeStore()
    {
        if (!_isEnabled)
            return;
        soomlaProxy.closeStore();
    }

    public void moreGames() 
    {
        if (!_isEnabled)
            return;

        logEvent("moreGames");

        PlayHavenManager.Instance.ContentRequest("more_games", true);
    }

    public void videoAds(Upgrade item) 
    {
        if (!_isEnabled)
            return;

        _adcolonyProxy.showVideo(item);
    }

    public bool videoAdEnabled()
    {
        if (!_isEnabled || _adcolonyProxy == null)
            return false;
        else
            return _adcolonyProxy.videoAdEnabled();

    }

    public void buyMarketItem(string itemId)
    {
        if (!_isEnabled)
            return;
        soomlaProxy.buyItem(itemId);
    }

    private IEnumerator _checkConnection()
    {
        INTERNET_AVAILABLE = Application.internetReachability != NetworkReachability.NotReachable;
        Debug.Log("C# _checkConnection = " + INTERNET_AVAILABLE.ToString());
        _isEnabled = INTERNET_AVAILABLE;

        if (_isEnabled && !_isInited)
            _initPlugins();

        yield return new WaitForSeconds(5.0f);
    }
}


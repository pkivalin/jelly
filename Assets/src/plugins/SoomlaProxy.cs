using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Soomla;

public class SoomlaProxy : MonoBehaviour 
{
    private bool _isOpening = false;

    public void restoreTransactions()
    {
        Debug.Log("Soomla  restoreTransactions");
        StoreController.RestoreTransactions();
    }

    void Start() 
    {
        Debug.Log("Soomla  Start");
        StoreController.Initialize(new SoomlaStore());

        Events.OnMarketPurchase += onMarketPurchase;
        Events.OnMarketPurchaseStarted += OnMarketPurchaseStarted;
        Events.OnMarketPurchaseCancelled += OnMarketPurchaseCancelled;
        Events.OnStoreControllerInitialized += onStoreControllerInitialized;
        Events.OnRestoreTransactions += onRestoreTransactions;
    }

    public void buyItem(String itemId)
    {
        Debug.Log("Soomla  buyItem = " + itemId);
        StoreController.BuyMarketItem(itemId);
    }

    public void openStore()
    {
        #if UNITY_ANDROID && !UNITY_EDITOR
        if (!_isOpening)
        {
            _isOpening = true;
            StoreController.StartIabServiceInBg();
        }
        #endif
    }

    public void closeStore()
    {
        #if UNITY_ANDROID && !UNITY_EDITOR
        if (_isOpening)
        {
            _isOpening = false;
            StoreController.StopIabServiceInBg();
        }
        #endif
    }

    void onRestoreTransactions(bool result)
    {
        Debug.Log("Soomla  onRestoreTransactions = " + result.ToString());
    }

    void onStoreControllerInitialized()
    {
        Debug.Log("Soomla  onStoreControllerInitialized");
    }


    void OnMarketPurchaseStarted(PurchasableVirtualItem obj)
    {
        Debug.Log("Soomla  OnMarketPurchaseStarted");
        Debug.Log("Going to purchase an item with productId: " + obj.ItemId);
        Debug.Log("Going to purchase an item with productId: " + obj.Name);
        Debug.Log("Going to purchase an item with productId: " + obj.Description);
        Debug.Log("Going to purchase an item with productId: " + obj.PurchaseType);
    }

    void OnMarketPurchaseCancelled(PurchasableVirtualItem obj)
    {
        Debug.Log("Soomla  OnMarketPurchaseCancelled");
        Debug.Log("Going to purchase an item with productId: " + obj.ItemId);
        Debug.Log("Going to purchase an item with productId: " + obj.Name);
        Debug.Log("Going to purchase an item with productId: " + obj.Description);
        Debug.Log("Going to purchase an item with productId: " + obj.PurchaseType);
    }

    void OnUnexpectedErrorInStore()
    {
        Debug.Log("Soomla  OnUnexpectedErrorInStore");
    }

    public void onMarketPurchase(PurchasableVirtualItem pvi) 
    {
        Debug.Log("Soomla  onMarketPurchase");
        Debug.Log("Going to purchase an item with productId: " + pvi.ItemId);
        Debug.Log("Going to purchase an item with productId: " + pvi.Name);
        Debug.Log("Going to purchase an item with productId: " + pvi.Description);
        Debug.Log("Going to purchase an item with productId: " + pvi.PurchaseType);

        if (pvi.ItemId == SoomlaStore.AUTOCOOLING_UPGRADE1_PRODUCT_ID && UserData.getUpgradeLevel(Upgrade.THERMOMETER_REPAIR) >= 1)
            return; // если автокулинг 1го уровня, но он у нас уже есть - ничего не делаем

        Upgrade.REAL_MONEY_UPGRADES[pvi.ItemId].apply();

        if (pvi.ItemId == SoomlaStore.AUTOCOOLING_UPGRADE2_PRODUCT_ID && UserData.getUpgradeLevel(Upgrade.THERMOMETER_REPAIR) < 2)
            Upgrade.REAL_MONEY_UPGRADES[pvi.ItemId].apply();
    }

}


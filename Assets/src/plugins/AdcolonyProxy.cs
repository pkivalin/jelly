using UnityEngine;
using System.Collections;
using System;


public class AdcolonyProxy : MonoBehaviour
{
    #if UNITY_ANDROID
    private const string VERSION = "version:0.9,store:google";
    private const string APP_ID = "app8cc871781e2a46c98a";
    private const string ZONE_ID = "vz3b91508dce4f458ebf";
    #elif UNITY_IPHONE
    private const string VERSION = "version:0.9,store:apple";
    private const string APP_ID = "appbaddfb802c854102b1";
    private const string ZONE_ID = "vz0a68d566c1b943638c";
    #else
    private const string VERSION = "x";
    private const string APP_ID = "x";
    private const string ZONE_ID = "x";
    #endif

    private Upgrade _item;
    private PluginsProxy _plugins;

    public void init(PluginsProxy plugins)
    {
        _plugins = plugins;
        // Assign any AdColony Delegates before calling Configure
        AdColony.OnVideoFinished = this.OnVideoFinished;
        AdColony.OnV4VCResult = this.OnV4VCResult;

        // If you wish to use a the customID feature, you should call  that now.
        // Then, configure AdColony:
        AdColony.Configure(VERSION, APP_ID, ZONE_ID);

        Debug.Log("C# " + AdColony.StatusForZone("vz3b91508dce4f458ebf"));

        Debug.Log("C# init adcolony");
    }

    public bool videoAdEnabled()
    {
        return AdColony.IsV4VCAvailable(ZONE_ID);
    }

    // When a video is available, you may choose to play it in any fashion you like.
    // Generally you will play them automatically during breaks in your game,
    // or in response to a user action like clicking a button.
    // Below is a method that could be called, or attached to a GUI action.
    public void showVideo(Upgrade item)
    {
        _item = item;

        // Check to see if a video for V4VC is available in the zone.
        if(AdColony.IsV4VCAvailable(ZONE_ID))
        {
            AdColony.ShowV4VC(false, ZONE_ID);
            _plugins.flurryParams.Add("success", "true");
        }
        else
        {
            _plugins.flurryParams.Add("success", "false");
            Debug.Log("c# V4VC Ad Not Available");
        }

        _plugins.logEvent("chartboost", _plugins.flurryParams);
    }

    private void OnVideoFinished(bool adShown)
    {
        Debug.Log("c# On Video Finished shown = " + adShown.ToString());
        // Resume your app here.
    }

    // The V4VCResult Delegate assigned in Initialize -- AdColony calls this after confirming V4VC transactions with your server
    // success - true: transaction completed, virtual currency awarded by your server - false: transaction failed, no virtual currency awarded
    // name - The name of your virtual currency, defined in your AdColony account
    // amount - The amount of virtual currency awarded for watching the video, defined in your AdColony account
    private void OnV4VCResult(bool success, string name, int amount)
    {
        if(success)
        {
            Debug.Log("c# V4VC SUCCESS: name = " + name + ", amount = " + amount);
            _item.apply();
        }
        else
        {
            Debug.LogWarning("V4VC FAILED!");
        }
    }
}
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GooglePlayGames;

public class AndroidGamecenterProxy:GamecenterProxy
{



    protected override void _initIDs()
    {
        PlayGamesPlatform.DebugLogEnabled = false;//true;
        PlayGamesPlatform.Activate();
        _arcadeLbId = "CgkI-dbE4PEZEAIQDg";
        _timeLbId = "CgkI-dbE4PEZEAIQDw";

        TIME_MODE_ACHIEVEMENT = "CgkI-dbE4PEZEAIQAQ";
        FRUIT_CATCHER_1_ACHIEVEMENT = "CgkI-dbE4PEZEAIQAg";
        FRUIT_CATCHER_2_ACHIEVEMENT = "CgkI-dbE4PEZEAIQAw";
        FRUIT_CATCHER_3_ACHIEVEMENT = "CgkI-dbE4PEZEAIQBA";
        COMBO_1_ACHIEVEMENT = "CgkI-dbE4PEZEAIQBQ";
        COMBO_2_ACHIEVEMENT = "CgkI-dbE4PEZEAIQBg";
        COMBO_3_ACHIEVEMENT = "CgkI-dbE4PEZEAIQBw";
        BONUS_CATCHER_1_ACHIEVEMENT = "CgkI-dbE4PEZEAIQCA";
        BONUS_CATCHER_2_ACHIEVEMENT = "CgkI-dbE4PEZEAIQCQ";
        BONUS_CATCHER_3_ACHIEVEMENT = "CgkI-dbE4PEZEAIQCg";
        BUG_HATER_1_ACHIEVEMENT = "CgkI-dbE4PEZEAIQCw";
        BUG_HATER_2_ACHIEVEMENT = "CgkI-dbE4PEZEAIQDA";
        BUG_HATER_3_ACHIEVEMENT = "CgkI-dbE4PEZEAIQDQ";
        MONEY_MAKER_1_ACHIEVEMENT = "CgkI-dbE4PEZEAIQEA";
        MONEY_MAKER_2_ACHIEVEMENT = "CgkI-dbE4PEZEAIQEQ";
        MONEY_MAKER_3_ACHIEVEMENT = "CgkI-dbE4PEZEAIQEg";
        UPGRADES_1_ACHIEVEMENT = "CgkI-dbE4PEZEAIQEw";
        UPGRADES_2_ACHIEVEMENT = "CgkI-dbE4PEZEAIQFA";
        UPGRADES_3_ACHIEVEMENT = "CgkI-dbE4PEZEAIQFQ";
        LOYAL_1_ACHIEVEMENT = "CgkI-dbE4PEZEAIQFg";
        LOYAL_2_ACHIEVEMENT = "CgkI-dbE4PEZEAIQFw";
        LOYAL_3_ACHIEVEMENT = "CgkI-dbE4PEZEAIQGA";
        ARCADE_1_ACHIEVEMENT = "CgkI-dbE4PEZEAIQGQ";
        ARCADE_2_ACHIEVEMENT = "CgkI-dbE4PEZEAIQGg";
        ARCADE_3_ACHIEVEMENT = "CgkI-dbE4PEZEAIQGw";
        RECIPES_1_ACHIEVEMENT = "CgkI-dbE4PEZEAIQHA";
        RECIPES_2_ACHIEVEMENT = "CgkI-dbE4PEZEAIQHQ";
        RECIPES_3_ACHIEVEMENT = "CgkI-dbE4PEZEAIQHg";
        STARS_1_ACHIEVEMENT = "CgkI-dbE4PEZEAIQHw";
        STARS_2_ACHIEVEMENT = "CgkI-dbE4PEZEAIQIA";
        STARS_3_ACHIEVEMENT = "CgkI-dbE4PEZEAIQIQ";
        TIME_1_ACHIEVEMENT = "CgkI-dbE4PEZEAIQIg";
        TIME_2_ACHIEVEMENT = "CgkI-dbE4PEZEAIQIw";
        TIME_3_ACHIEVEMENT = "CgkI-dbE4PEZEAIQJA";

        base._initIDs();
    }

    public override void setProgress(string id, float progress)
    {
        progress = Mathf.Min(100, progress);

        if (_recievedAchievements[id] == false && (progress > _achievementsProgress[id] || progress == 100))
        {
            Debug.Log("setProgress - " + id);
            UserData.setAchievementProgress(id, progress);
            _achievementsProgress[id] = progress;
            if (progress == 100)
            {
                _recievedAchievements[id] = true;
                if (_logged)
                    Social.ReportProgress(id, progress, success => {});
            }
        }
    }
}


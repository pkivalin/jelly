using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SocialPlatforms.GameCenter;

public class IOSGamecenterProxy:GamecenterProxy
{
    protected override void _initIDs()
    {
        _arcadeLbId = "grp.net.kivalin.jellyfruit.arcadelb";
        _timeLbId = "grp.net.kivalin.jellyfruit.timelb";

        TIME_MODE_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.timemode";
        FRUIT_CATCHER_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.fruitcatcher1";
        FRUIT_CATCHER_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.fruitcatcher2";
        FRUIT_CATCHER_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.fruitcatcher3";
        COMBO_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.combo1";
        COMBO_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.combo2";
        COMBO_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.combo3";
        BONUS_CATCHER_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.bonuscatcher1";
        BONUS_CATCHER_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.bonuscatcher2";
        BONUS_CATCHER_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.bonuscatcher3";
        BUG_HATER_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.bughater1";
        BUG_HATER_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.bughater2";
        BUG_HATER_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.bughater3";
        MONEY_MAKER_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.moneymaker1";
        MONEY_MAKER_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.moneymaker2";
        MONEY_MAKER_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.moneymaker3";
        UPGRADES_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.upgrades1";
        UPGRADES_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.upgrades2";
        UPGRADES_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.upgrades3";
        LOYAL_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.loyal1";
        LOYAL_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.loyal2";
        LOYAL_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.loyal3";
        ARCADE_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.arcade1";
        ARCADE_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.arcade2";
        ARCADE_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.arcade3";
        RECIPES_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.recipes1";
        RECIPES_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.recipes2";
        RECIPES_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.recipes3";
        STARS_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.stars1";
        STARS_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.stars2";
        STARS_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.stars3";
        TIME_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.time1";
        TIME_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.time2";
        TIME_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.time3";

        GameCenterPlatform.ShowDefaultAchievementCompletionBanner(false);

        base._initIDs();
    }
        
    public override void showLeaderboard()
    {
        if (_logged)
        {
            GameCenterPlatform.ShowLeaderboardUI(_arcadeLbId, UnityEngine.SocialPlatforms.TimeScope.Week);
        }
        else
        {
            _needShowLeaderboards = true;
            _authenticate();
        }
    }

    public override void resetAchievements()
    {
        GameCenterPlatform.ResetAllAchievements(success => {});
        base.resetAchievements();
    }
}


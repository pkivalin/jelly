using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
using UnityEngine.SocialPlatforms.GameCenter;

public class GamecenterProxy
{
    public static string TIME_MODE_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.timemode";
    public static string FRUIT_CATCHER_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.fruitcatcher1";
    public static string FRUIT_CATCHER_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.fruitcatcher2";
    public static string FRUIT_CATCHER_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.fruitcatcher3";
    public static string COMBO_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.combo1";
    public static string COMBO_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.combo2";
    public static string COMBO_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.combo3";
    public static string BONUS_CATCHER_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.bonuscatcher1";
    public static string BONUS_CATCHER_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.bonuscatcher2";
    public static string BONUS_CATCHER_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.bonuscatcher3";
    public static string BUG_HATER_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.bughater1";
    public static string BUG_HATER_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.bughater2";
    public static string BUG_HATER_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.bughater3";
    public static string MONEY_MAKER_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.moneymaker1";
    public static string MONEY_MAKER_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.moneymaker2";
    public static string MONEY_MAKER_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.moneymaker3";
    public static string UPGRADES_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.upgrades1";
    public static string UPGRADES_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.upgrades2";
    public static string UPGRADES_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.upgrades3";
    public static string LOYAL_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.loyal1";
    public static string LOYAL_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.loyal2";
    public static string LOYAL_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.loyal3";
    public static string ARCADE_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.arcade1";
    public static string ARCADE_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.arcade2";
    public static string ARCADE_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.arcade3";
    public static string RECIPES_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.recipes1";
    public static string RECIPES_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.recipes2";
    public static string RECIPES_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.recipes3";
    public static string STARS_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.stars1";
    public static string STARS_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.stars2";
    public static string STARS_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.stars3";
    public static string TIME_1_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.time1";
    public static string TIME_2_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.time2";
    public static string TIME_3_ACHIEVEMENT = "grp.net.kivalin.jellyfruit.time3";

    public static string[] ACHIEVEMNT_IDS;

    protected string _arcadeLbId;
    protected string _timeLbId;

    protected Dictionary<string, bool> _recievedAchievements;
    protected Dictionary<string, float> _achievementsProgress;
    protected int[][] _levelStars = new int[Game.PACKS_CNT][];
    protected int _totalStarsCnt;
    protected bool _inited = false;
    protected bool _logged = false;
    protected bool _needShowAchievements = false;
    protected bool _needShowLeaderboards = false;

    public void init()
    {
        if (_inited)
            return;

        _inited = true;

        _initIDs();
        _initAchievementsProgress();

        #if UNITY_IPHONE || UNITY_EDITOR
        _authenticate();
        #endif

        _totalStarsCnt = 0;
        for (int i = 0; i < _levelStars.Length; i++)
        {
            _levelStars[i] = new int[Game.LEVELS_IN_PACK];
            for (int j = 0; j < _levelStars[i].Length; j++)
            {
                _levelStars[i][j] = UserData.getLevelStars(i + 1, j);
                _totalStarsCnt += _levelStars[i][j];
            }
        }

        _bonusCnt = UserData.achievementBonusCnt;
        _avoidBugCnt = UserData.achievementAvoidBugCnt;
        _moneyEarned = UserData.achievementMoneyEarned;
        _upgradesBuyed = UserData.achievementUpgradesBuyed;

        _checkLoyalAchievement();
    }

    protected void _authenticate()
    {
        Social.localUser.Authenticate (success => {
            if (success) 
            {
                UserData.gamecenterLogged = true;
                _logged = true;

                _sendProgress();

                #if UNITY_IPHONE && !UNITY_EDITOR
                GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
                #endif

                if (_needShowAchievements)
                {
                    _needShowAchievements = false;
                    showAchievements();
                }
                else if (_needShowLeaderboards)
                {
                    _needShowLeaderboards = false;
                    showLeaderboard();
                }
            }
            else
                Debug.Log ("Authentication failed");
        });
    }

    protected void _sendProgress()
    {
        setTimeBestScore(UserData.timeBestScore);
        setArcadeBestScore(UserData.arcadeBestScore);

        for (int i = 0; i < ACHIEVEMNT_IDS.Length; i++)
        {
            setProgress(ACHIEVEMNT_IDS[i], _achievementsProgress[ACHIEVEMNT_IDS[i]]);
        }
    }

    protected virtual void _initIDs()
    {
        ACHIEVEMNT_IDS = new string[]{TIME_MODE_ACHIEVEMENT, FRUIT_CATCHER_1_ACHIEVEMENT, FRUIT_CATCHER_2_ACHIEVEMENT
            , FRUIT_CATCHER_3_ACHIEVEMENT, COMBO_1_ACHIEVEMENT, COMBO_2_ACHIEVEMENT, COMBO_3_ACHIEVEMENT, BONUS_CATCHER_1_ACHIEVEMENT
            , BONUS_CATCHER_2_ACHIEVEMENT, BONUS_CATCHER_3_ACHIEVEMENT, BUG_HATER_1_ACHIEVEMENT, BUG_HATER_2_ACHIEVEMENT, BUG_HATER_3_ACHIEVEMENT
            , MONEY_MAKER_1_ACHIEVEMENT, MONEY_MAKER_2_ACHIEVEMENT, MONEY_MAKER_3_ACHIEVEMENT, UPGRADES_1_ACHIEVEMENT, UPGRADES_2_ACHIEVEMENT
            , UPGRADES_3_ACHIEVEMENT, LOYAL_1_ACHIEVEMENT, LOYAL_2_ACHIEVEMENT, LOYAL_3_ACHIEVEMENT, ARCADE_1_ACHIEVEMENT, ARCADE_2_ACHIEVEMENT
            , ARCADE_3_ACHIEVEMENT, RECIPES_1_ACHIEVEMENT, RECIPES_2_ACHIEVEMENT, RECIPES_3_ACHIEVEMENT, STARS_1_ACHIEVEMENT, STARS_2_ACHIEVEMENT
            , STARS_3_ACHIEVEMENT, TIME_1_ACHIEVEMENT, TIME_2_ACHIEVEMENT, TIME_3_ACHIEVEMENT};
    }

    void _initAchievementsProgress()
    {
        _recievedAchievements = new Dictionary<string, bool>();
        _achievementsProgress = new Dictionary<string, float>();

        for (int i = 0; i < ACHIEVEMNT_IDS.Length; i++)
        {
            _recievedAchievements.Add(ACHIEVEMNT_IDS[i], false); // в начале сессии всем ставим что ачивки нет, чтобы повторно заслали, вдруг инета не было в прошлую сессию
            _achievementsProgress.Add(ACHIEVEMNT_IDS[i], UserData.getAchievementProgress(ACHIEVEMNT_IDS[i]));
        }
    }

    public void changeLevelStars(int pack, int level, int stars)
    {
        if (_levelStars[pack - 1][level] < stars)
        {
            _totalStarsCnt -= _levelStars[pack - 1][level];
            _totalStarsCnt += stars;
            _levelStars[pack - 1][level] = stars;

            setProgress(STARS_1_ACHIEVEMENT, _totalStarsCnt * 100 / 20.0f);
            setProgress(STARS_2_ACHIEVEMENT, _totalStarsCnt * 100 / 60.0f);
            setProgress(STARS_3_ACHIEVEMENT, _totalStarsCnt * 100 / 120.0f);

            if (stars == 3)
            {
                int perfectLevelsCnt = 0;
                for (int i = 0; i < _levelStars[pack - 1].Length; i++)
                    if (_levelStars[pack - 1][i] == 3)
                        perfectLevelsCnt++;

                if (pack == 1)
                    setProgress(RECIPES_1_ACHIEVEMENT, perfectLevelsCnt * 100 / 16.0f);
                else if (pack == 2)
                    setProgress(RECIPES_2_ACHIEVEMENT, perfectLevelsCnt * 100 / 16.0f);
                else if (pack == 3)
                    setProgress(RECIPES_3_ACHIEVEMENT, perfectLevelsCnt * 100 / 16.0f);
            }
        }
    }

    private void _checkLoyalAchievement()
    {
        DateTime launchDate = new DateTime();
        DateTime.TryParse(UserData.achievementLaunchDate, out launchDate);
        int daysFromLaunch = (DateTime.Now - launchDate).Minutes;
        int achievementLoyalDays = daysFromLaunch - UserData.achievementLoyalDays;
        if (achievementLoyalDays == 1) // прошел 1 день
        {
            UserData.achievementLoyalDays++;
            if (UserData.achievementLoyalDays == 7)
                awardAchievement(LOYAL_1_ACHIEVEMENT);
            else if (UserData.achievementLoyalDays == 14)
                awardAchievement(LOYAL_2_ACHIEVEMENT);
            else if (UserData.achievementLoyalDays == 30)
                awardAchievement(LOYAL_3_ACHIEVEMENT);
        }
        else if (achievementLoyalDays > 1)
        {
            UserData.achievementLoyalDays = 0;
            UserData.achievementLaunchDate = DateTime.Now.ToString();
        }
    }


    public void setArcadeBestScore(int points)
    {
        #if (UNITY_ANDROID || UNITY_IPHONE)  && !UNITY_EDITOR
        if (_logged)
            Social.ReportScore(points, _arcadeLbId, success => {});
        #endif
    }

    public void setTimeBestScore(int points)
    {
        #if (UNITY_ANDROID || UNITY_IPHONE)  && !UNITY_EDITOR
        if (_logged)
            Social.ReportScore(points, _timeLbId, success => {});
        #endif
        Debug.Log("setTimeBestScore " + points.ToString());
    }

    public void awardAchievement(string id)
    {
        Debug.Log("awardAchievement " + id);
        setProgress(id, 100);
    }

    public bool hasAchievement(string id)
    {
        init(); // костыль чтобы был верный порядок инициализации
        return UserData.getAchievementProgress(id) >= 100;
    }

    public virtual void setProgress(string id, float progress)
    {
        progress = Mathf.Min(100, progress);

        if (_recievedAchievements[id] == false && (progress > _achievementsProgress[id] || progress == 100))
        {
            Debug.Log("setProgress - " + id);
            #if UNITY_IPHONE && !UNITY_EDITOR
            if (_logged)
                Social.ReportProgress(id, progress, success => {});
            #endif
            UserData.setAchievementProgress(id, progress);
            _achievementsProgress[id] = progress;
            if (progress >= 100)
                _recievedAchievements[id] = true;
        }
    }

    public void showAchievements()
    {
        Debug.Log("!!! logged = " + _logged);
        if (_logged)
        {
            Social.ShowAchievementsUI();
        }
        else
        {
            _needShowAchievements = true;
            _authenticate();
        }

    }

    public virtual void showLeaderboard()
    {
        if (_logged)
        {
            Social.ShowLeaderboardUI();
        }
        else
        {
            _needShowLeaderboards = true;
            _authenticate();
        }
    }
    public virtual void resetAchievements()
    {
        Debug.Log("resetAchievements");
        _initAchievementsProgress();
    }

    public int score 
    {
        set
        {
            if (Game.GAME_TYPE == Game.GAME_TYPE_SURVIVAL)
            {
                if (value >= Settings.unlockTimeModeScores)
                    awardAchievement(TIME_MODE_ACHIEVEMENT);

                if (value >= 6000)
                    awardAchievement(ARCADE_3_ACHIEVEMENT);
                else if (value >= 3000)
                    awardAchievement(ARCADE_2_ACHIEVEMENT);
                else if (value >= 1500)
                    awardAchievement(ARCADE_1_ACHIEVEMENT);
            }
        }
    }

    private int _fruitInRow;
    public int fruitInRow 
    {
        set
        {
            if (value == 20)
                awardAchievement(FRUIT_CATCHER_1_ACHIEVEMENT);
            else if (value == 60)
                awardAchievement(FRUIT_CATCHER_2_ACHIEVEMENT);
            else if (value == 120)
                awardAchievement(FRUIT_CATCHER_3_ACHIEVEMENT);

            _fruitInRow = value;
        }
        get
        {
            return _fruitInRow;
        }
    }

    public int combo 
    {
        set
        {
            if (value == 15)
                awardAchievement(COMBO_1_ACHIEVEMENT);
            else if (value == 30)
                awardAchievement(COMBO_2_ACHIEVEMENT);
            else if (value == 60)
                awardAchievement(COMBO_3_ACHIEVEMENT);
        }
    }

    public int timeModeFails 
    {
        set
        {
            if (value <= 8)
                awardAchievement(TIME_1_ACHIEVEMENT);
            if (value <= 3)
                awardAchievement(TIME_2_ACHIEVEMENT);
            if (value == 0)
                awardAchievement(TIME_3_ACHIEVEMENT);
        }
    }

    private int _bonusCnt;
    public int bonusCnt 
    {
        set
        {
            setProgress(BONUS_CATCHER_1_ACHIEVEMENT, value * 100 / 20.0f);
            setProgress(BONUS_CATCHER_2_ACHIEVEMENT, value * 100 / 60.0f);
            setProgress(BONUS_CATCHER_3_ACHIEVEMENT, value * 100 / 120.0f);
            _bonusCnt = value;
            UserData.achievementBonusCnt = _bonusCnt;
        }
        get
        {
            return _bonusCnt;
        }
    }

    private int _avoidBugCnt;
    public int avoidBugCnt 
    {
        set
        {
            setProgress(BUG_HATER_1_ACHIEVEMENT, value * 100 / 50.0f);
            setProgress(BUG_HATER_2_ACHIEVEMENT, value * 100 / 150.0f);
            setProgress(BUG_HATER_3_ACHIEVEMENT, value * 100 / 300.0f);
            _avoidBugCnt = value;
            UserData.achievementAvoidBugCnt = _avoidBugCnt;
        }
        get
        {
            return _avoidBugCnt;
        }
    }

    private int _moneyEarned;
    public int moneyEarned 
    {
        set
        {
            setProgress(MONEY_MAKER_1_ACHIEVEMENT, value * 100 / 10000.0f);
            setProgress(MONEY_MAKER_2_ACHIEVEMENT, value * 100 / 40000.0f);
            setProgress(MONEY_MAKER_3_ACHIEVEMENT, value * 100 / 100000.0f);
            _moneyEarned = value;
            UserData.achievementAvoidBugCnt = _moneyEarned;
        }
        get
        {
            return _moneyEarned;
        }
    }

    private int _upgradesBuyed;
    public int upgradesBuyed 
    {
        set
        {
            setProgress(UPGRADES_1_ACHIEVEMENT, value * 100 / 5.0f);
            setProgress(UPGRADES_2_ACHIEVEMENT, value * 100 / 15.0f);
            setProgress(UPGRADES_3_ACHIEVEMENT, value * 100 / 30.0f);
            _upgradesBuyed = value;
            UserData.achievementAvoidBugCnt = _upgradesBuyed;
        }
        get
        {
            return _upgradesBuyed;
        }
    }

}


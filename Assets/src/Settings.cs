using System;
using UnityEngine;
using System.Xml;

public static class Settings
{

    static private XmlDocument _settings = new XmlDocument();
    static private XmlDocument _survival = new XmlDocument();
    static private XmlDocument _survivalNoob = new XmlDocument();
    static private XmlDocument _time = new XmlDocument();
    static private XmlDocument _pack1 = new XmlDocument();
    static private XmlDocument _pack2 = new XmlDocument();
    static private XmlDocument _pack3 = new XmlDocument();
    static private XmlDocument _bonuses = new XmlDocument();
    static private XmlDocument _tutorial = new XmlDocument();
    static public Vector2 startHeight;
    static public float minVelocityVertical;
    static public float minVelocityHorizontal;
    static public float bugForce;
    static public float jellyScale;
    static public float jellyGap;
    static public float jellyAlpha;
    static public float comboMaxDelay;
    static public int comboBonus;
    static public int comboBonusForFive;
    static public int recipeFruitScoreMulti;
    static public float fruitSlow;
    static public float fruitAngularSlow;
    static public int[] stars;
    static public int[] starsAddMoney;
    static public float bonusDelay;
    static public float bonusFirstDelay;
    static public float muteMainTheme;
    static public float continueArcadeTime;
    static public int continueArcadeCost;
    static public int timeModeDuration;
    static public int timeModeBonus;
    static public int timeModePenalty;
    static public int timeModePerfectBonusMoney;
    static public int timeModePerfectBonusScore;
    static public int timeModeClock;
    static public int scaleAlarm;
    static public int maxFruitBounceCnt;
    static public float endLevelAdsDelay;
    static public int arcadeScores;
    static public int unlockTimeModeScores;
    static public int maxRecipeWrongFruitInterval;
    static public float forceDeltaVertical;
    static public float forceDeltaHorizontal;
    static public bool cheatsIsEnabled;
    static public float forceMultiplyHorizontal;
    static public Vector2 bugHorizontalForce;
    static public Vector2 bugVerticalForce;

	static Settings ()
	{
	}

	static public void init()
	{
		try
		{            
            _settings.Load("settings.xml");
            _survival.Load("survival.xml");
            _survivalNoob.Load("survivalNoob.xml");
            _time.Load("time.xml");
            _pack1.Load("pack1.xml");
            _pack2.Load("pack2.xml");
            _pack3.Load("pack3.xml");
            _bonuses.Load("bonuses.xml");
            _tutorial.Load("tutorial.xml");
		}
		catch
		{
			TextAsset textAsset = (TextAsset) Resources.Load("settings");  
            _settings.LoadXml(textAsset.text);
            textAsset = (TextAsset) Resources.Load("survival");  
            _survival.LoadXml(textAsset.text);
            textAsset = (TextAsset) Resources.Load("survivalNoob");  
            _survivalNoob.LoadXml(textAsset.text);
            textAsset = (TextAsset) Resources.Load("time");  
            _time.LoadXml(textAsset.text);
            textAsset = (TextAsset) Resources.Load("pack1");  
            _pack1.LoadXml(textAsset.text);
            textAsset = (TextAsset) Resources.Load("pack2");  
            _pack2.LoadXml(textAsset.text);
            textAsset = (TextAsset) Resources.Load("pack3");  
            _pack3.LoadXml(textAsset.text);
            textAsset = (TextAsset) Resources.Load("bonuses");  
            _bonuses.LoadXml(textAsset.text);
            textAsset = (TextAsset) Resources.Load("tutorial");  
            _tutorial.LoadXml(textAsset.text);
		}

        cheatsIsEnabled = bool.Parse(_settings.SelectSingleNode("settings/cheats").InnerText);
        minVelocityVertical = float.Parse(_settings.SelectSingleNode("settings/minVelocity/vertical").InnerText);
        minVelocityHorizontal = float.Parse(_settings.SelectSingleNode("settings/minVelocity/horizontal").InnerText);
        bugForce = float.Parse(_settings.SelectSingleNode("settings/bugForce").InnerText);
        jellyGap = float.Parse(_settings.SelectSingleNode("settings/jelly/gap").InnerText);
        jellyScale = float.Parse(_settings.SelectSingleNode("settings/jelly/scale").InnerText);
        jellyAlpha = float.Parse(_settings.SelectSingleNode("settings/jelly/alpa").InnerText);
        fruitSlow = float.Parse(_settings.SelectSingleNode("settings/jelly/fruitSlow").InnerText);
        fruitAngularSlow = float.Parse(_settings.SelectSingleNode("settings/jelly/fruitAngularSlow").InnerText);
        comboMaxDelay = float.Parse(_settings.SelectSingleNode("settings/combo/maxDelay").InnerText);
        comboBonus = int.Parse(_settings.SelectSingleNode("settings/combo/bonus").InnerText);
        comboBonusForFive = int.Parse(_settings.SelectSingleNode("settings/combo/bonusForFive").InnerText);
        maxFruitBounceCnt = int.Parse(_settings.SelectSingleNode("settings/maxFruitBounceCnt").InnerText);
        recipeFruitScoreMulti = int.Parse(_settings.SelectSingleNode("settings/recipeFruitScoreMulti").InnerText);
        bonusDelay = float.Parse(_settings.SelectSingleNode("settings/bonuses/delay").InnerText);
        bonusFirstDelay = float.Parse(_settings.SelectSingleNode("settings/bonuses/firstDelay").InnerText);
        muteMainTheme = float.Parse(_settings.SelectSingleNode("settings/sounds/muteMainTheme").InnerText);
        forceDeltaVertical = float.Parse(_settings.SelectSingleNode("settings/forceDelta/vertical").InnerText);
        forceDeltaHorizontal = float.Parse(_settings.SelectSingleNode("settings/forceDelta/horizontal").InnerText);
        timeModeClock = int.Parse(_settings.SelectSingleNode("settings/sounds/timeModeClock").InnerText);
        scaleAlarm = int.Parse(_settings.SelectSingleNode("settings/sounds/scaleAlarm").InnerText);
        endLevelAdsDelay = float.Parse(_settings.SelectSingleNode("settings/ads/endLevelTime").InnerText);
        continueArcadeCost = int.Parse(_settings.SelectSingleNode("settings/continueArcade/cost").InnerText);
        continueArcadeTime = float.Parse(_settings.SelectSingleNode("settings/continueArcade/time").InnerText);
        arcadeScores = int.Parse(_settings.SelectSingleNode("settings/arcadeScores").InnerText);
        unlockTimeModeScores = int.Parse(_settings.SelectSingleNode("settings/unlockTimeModeScores").InnerText);
        maxRecipeWrongFruitInterval = int.Parse(_settings.SelectSingleNode("settings/maxRecipeWrongFruitInterval").InnerText);
        startHeight = new Vector2(float.Parse(_settings.SelectSingleNode("settings/startHeight/min").InnerText)
            , float.Parse(_settings.SelectSingleNode("settings/startHeight/max").InnerText));
		stars = new int[3];
		for (int i = 1; i <= 3; i++)
		{
            stars[i - 1] = int.Parse(_settings.SelectSingleNode("settings/stars/star" + i.ToString()).InnerText);
		}

        starsAddMoney = new int[3];
        for (int i = 1; i <= 3; i++)
        {
            starsAddMoney[3 - i] = int.Parse(_settings.SelectSingleNode("settings/starsAddMoney/star" + i.ToString()).InnerText);
        }


        timeModeDuration = int.Parse(_time.SelectSingleNode("settings/duration").InnerText);
        timeModeBonus = int.Parse(_time.SelectSingleNode("settings/completeBonus").InnerText);
        timeModePenalty = int.Parse(_time.SelectSingleNode("settings/mistakePenalty").InnerText);
        timeModePerfectBonusMoney = int.Parse(_time.SelectSingleNode("settings/perfectGameBonus/money").InnerText);
        timeModePerfectBonusScore = int.Parse(_time.SelectSingleNode("settings/perfectGameBonus/score").InnerText);

        bugHorizontalForce = new Vector2(float.Parse(_settings.SelectSingleNode("settings/bug/force/horizontal/min").InnerText)
            , float.Parse(_settings.SelectSingleNode("settings/bug/force/horizontal/max").InnerText));

        bugVerticalForce = new Vector2(float.Parse(_settings.SelectSingleNode("settings/bug/force/vertical/min").InnerText)
            , float.Parse(_settings.SelectSingleNode("settings/bug/force/vertical/max").InnerText));
	}

    static private XmlDocument _getModeXml()
    {
        XmlDocument xml;
        if (Game.GAME_TYPE == Game.GAME_TYPE_SURVIVAL)
        {
            if (UserData.arcadeBestScore >= arcadeScores)
                xml = _survival;
            else
                xml = _survivalNoob;
        }
        else if (Game.GAME_TYPE == Game.GAME_TYPE_TIME)
            xml = _time;
        else if (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE && Game.PACK_NUMBER == 1)
            xml = _pack1;
        else  if (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE && Game.PACK_NUMBER == 2)
            xml = _pack2;
        else
            xml = _pack3;

        return xml;
    }

    static public XmlDocument getPackXml(int packNumber)
    {
        XmlDocument xml;
        if (packNumber == 1)
            xml = _pack1;
        else  if (packNumber == 2)
            xml = _pack2;
        else
            xml = _pack3;

        return xml;
    }

    static public XmlNode getRecipeTutorial()
    {
        return _tutorial.SelectSingleNode("tutorials/recipe");
    }

    static public XmlNode getSurvivalTutorial()
    {
        return _tutorial.SelectSingleNode("tutorials/survival");
    }

	static public XmlNode getLevels()
	{
        if (Game.GAME_TYPE == Game.GAME_TYPE_RECIPE)
        {
            return _getModeXml().SelectSingleNode("settings/levels/level" + Game.LEVEL_NUMBER);
        }
        else
        {
            return _getModeXml().SelectSingleNode("settings/levels");
        }
	}

	static public XmlNode getLifebar()
	{
        return _settings.SelectSingleNode("settings/lifebar");
	}

	static public XmlNode getFruit(String name)
	{
        return _settings.SelectSingleNode("settings/fruits/" + name);
	}

	static public XmlNode getBonuses()
	{
        return _bonuses.SelectSingleNode("bonuses");
	}
	
}
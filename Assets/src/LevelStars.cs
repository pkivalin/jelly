﻿using UnityEngine;
using System.Collections;

public class LevelStars : MonoBehaviour {

	public tk2dSprite[] stars;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setStars(int starsCount)
	{
		for (int i = 0; i < stars.Length; i++)
		{
			if (i < starsCount)
				stars[i].SetSprite("star_ld_full");
			else
				stars[i].SetSprite("star_ld_empty");
		}
	}
}

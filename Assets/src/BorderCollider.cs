﻿using UnityEngine;
using System.Collections;

public class BorderCollider : MonoBehaviour 
{
    private const float TWEEN_DURATION = 0.4f;

	public bool isLeft;
    public TweenedObject tweener;
    private bool _needDestroyBug;
    public tk2dSprite sprite;
    private FruitFactory _fruitFactory;
    private bool _isActive = false;

    public void init(FruitFactory fruitFactory)
    {
        _fruitFactory = fruitFactory;
    }

    public bool needDestroyBug
    {
        get
        {
            return _needDestroyBug;
        }
        set
        {
            _needDestroyBug = value;
            sprite.gameObject.SetActive(_needDestroyBug);
        }
    }

    public void show(bool value)
    {
        _isActive = true;
        needDestroyBug = value;
        tweener.tween(1, TWEEN_DURATION, false, 0, Holoville.HOTween.EaseType.EaseOutQuad);
    }

    public void reset()
    {
        _isActive = false;
    }

    public void hide()
    {
        if (_isActive)
        {
            _isActive = false;
            tweener.tween(0, TWEEN_DURATION, true, 1, Holoville.HOTween.EaseType.EaseInQuad, 0.2f);
        }
    }
	
	void OnCollisionEnter(Collision collision)
	{
		BaseObject baseObject = collision.gameObject.GetComponent<BaseObject>();
        if (!baseObject)
            return;
        baseObject.needCalcBounce = true;
        baseObject.bounceCnt++;
        // если жук или это фрукт в рецептах и он там не нужен =\
        bool bugOrWrongFruit = baseObject is Bug || (baseObject is Fruit && Game.GAME_TYPE == Game.GAME_TYPE_RECIPE && !_fruitFactory.needInRecipe(baseObject as Fruit));
        if ((bugOrWrongFruit && needDestroyBug) || baseObject.bounceCnt == Settings.maxFruitBounceCnt)
        {
            baseObject.forceDestroy = true;
            baseObject.needAlert = false;
        }
		else 
        {
			baseObject.changeDirection();
        }
	}
}

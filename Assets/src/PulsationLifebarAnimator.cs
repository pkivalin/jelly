﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class PulsationLifebarAnimator : MonoBehaviour 
{
    private TweenParms _tween1;
    private TweenParms _tween2;
    private TweenParms _tween3;
    private TweenParms _tween4;

    private int _loopsLeft;

    public void startPulsation(int loopsCnt = 3)
    {
        if (_tween1 == null)
        {
            _tween1 = new TweenParms();
            _tween1.Ease(EaseType.Linear);
            _tween2 = new TweenParms();
            _tween2.Ease(EaseType.Linear);
        }
        float timeX = 0.13f;
        Vector3 scale = gameObject.transform.localScale * 1.1f;
        _tween1.Prop("localScale", scale); 
        scale = gameObject.transform.localScale;
        _tween2.Prop("localScale", scale); 

        Sequence sequence = new Sequence();
        sequence.Append(HOTween.To(gameObject.transform, timeX, _tween1));
        sequence.Append(HOTween.To(gameObject.transform, timeX * 1.5f, _tween2));
        if (loopsCnt > 1)
        {
            sequence.Append(HOTween.To(gameObject.transform, timeX, _tween1));
            sequence.Append(HOTween.To(gameObject.transform, timeX * 1.5f, _tween2));
        }
        if (loopsCnt > 2)
        {
            sequence.Append(HOTween.To(gameObject.transform, timeX, _tween1));
            sequence.Append(HOTween.To(gameObject.transform, timeX * 3f, _tween2));
        }

        sequence.Play();
    }

}

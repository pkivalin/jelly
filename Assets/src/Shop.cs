using System;
using UnityEngine;
using System.Xml;

public static class Shop
{

	static private XmlDocument _xml = new XmlDocument();

	static Shop ()
	{
	}

	static public void init()
	{
		try
		{            
			_xml.Load("shop.xml");
		}
		catch
		{
			TextAsset textAsset = (TextAsset) Resources.Load("shop");  
			_xml.LoadXml(textAsset.text);
		}
	}

	static public XmlNode getCategory(int categoryId)
	{
		return _xml.SelectSingleNode("upgrades/category" + categoryId.ToString());
	}
	
}
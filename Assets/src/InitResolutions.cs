using UnityEngine;
using System.Collections;

public class InitResolutions : MonoBehaviour {

    public bool muteSounds;
    private bool _started = false;

	// Use this for initialization
    void Awake() 
    {
        if (muteSounds)
        {
            AudioListener.pause = true;
            AudioListener.volume = 0.0f;
        }
		tk2dCamera _camera = gameObject.GetComponent<tk2dCamera>();
        Vector2 targetResolution = new Vector2(Mathf.Max(_camera.TargetResolution.x, _camera.TargetResolution.y), Mathf.Min(_camera.TargetResolution.x, _camera.TargetResolution.y));
        MultiResolution.init(targetResolution);

        if (targetResolution.x < 700)
			tk2dSystem.CurrentPlatform = "1x";
        else if (targetResolution.x < 1500)
			tk2dSystem.CurrentPlatform = "2x";
		else 
            tk2dSystem.CurrentPlatform = "2x";

        #if UNITY_IPHONE && !UNITY_EDITOR
        if (iPhone.generation == iPhoneGeneration.iPad1Gen || iPhone.generation == iPhoneGeneration.iPodTouch4Gen)
            tk2dSystem.CurrentPlatform = "1x";
        #endif

        Instantiate(Resources.Load("Preloader", typeof(GameObject)));

		Settings.init();
	}

	// Update is called once per frame
	void Update () 
    {
        if (!_started)
        {
            _started = true;
            Application.LoadLevel("game");
        }
	}
}

﻿using UnityEngine;
using System.Collections;

public class JellyBorders : MonoBehaviour {

	public tk2dCamera guiCamera;
	public Camera gameCamera;
	public GameObject cube;
	public GameObject jellySprite;
	public GameObject tappedAnimation;
	public GameObject leftBorder;
	public GameObject rightBorder;
    private bool _isLong;

	// Use this for initialization
    public void init(Game game) 
	{
        game.initGameObjectScale(cube);
        game.initGameObjectScale(jellySprite);
        game.initGameObjectScale(tappedAnimation);
	}

    public void setBorders(bool needLong = false)
    {
        PositionedSprite positionedSprite = leftBorder.GetComponent<PositionedSprite>();

        string spriteName;
        if (needLong)
        {
            spriteName = "barrierPackGlass";
        }
        else
        {
            spriteName = "glass";
        }

        leftBorder.GetComponent<tk2dSprite>().SetSprite(spriteName);
        float neededX = Settings.jellyGap - 100 + 2;
        positionedSprite.init(new Vector2(neededX, -100)
            , new Vector2(neededX, -100)
            , new Vector2(neededX, -100));

        rightBorder.GetComponent<tk2dSprite>().SetSprite(spriteName);
        positionedSprite = rightBorder.GetComponent<PositionedSprite>();
        neededX = 100 - Settings.jellyGap - 2;
        positionedSprite.init(new Vector2(neededX, -100)
            , new Vector2(neededX, -100)
            , new Vector2(neededX, -100));

    }

}

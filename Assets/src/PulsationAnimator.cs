﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class PulsationAnimator : MonoBehaviour 
{
    private TweenParms _tween1;
    private TweenParms _tween2;
    private TweenParms _tween3;
    private TweenParms _tween4;

    void Start()
    {
        _tween1 = new TweenParms();
        _tween1.Prop("localScale", new Vector3(1.1f, 1.1f, 1.1f)); 
        _tween1.Ease(EaseType.EaseOutBack);

        _tween2 = new TweenParms();
        _tween2.Prop("localScale", new Vector3(1.0f, 1.0f, 1.0f)); 
        _tween2.Ease(EaseType.Linear);

        _tween3 = new TweenParms();
        _tween3.Prop("localScale", new Vector3(1.0f, 1.0f, 1.0f)); 

        Sequence sequence = new Sequence();
        sequence.Append(HOTween.To(gameObject.transform, 0.2f, _tween1));
        sequence.Append(HOTween.To(gameObject.transform, 0.4f, _tween2));
        sequence.Append(HOTween.To(gameObject.transform, 1.0f, _tween3));
        sequence.loops = - 1;
        sequence.Play();
    }

}

﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class ContinueWindow : MonoBehaviour {
   
    public tk2dUIItem shopBtn;
    public tk2dTextMesh priceTF;
    public tk2dTextMesh timerTF;

    private bool _isActive;
    private Game _game;
    private float _timeLeft;

    private TweenParms _tween1;

    private GameManager _gameManager;
    public GameManager gameManager
    {
        get
        {
            if (_gameManager == null)
                _gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
            return _gameManager;
        }
    }

    public void init(Game game)
    {
        _game = game;
    }

    public void show()
    {
        gameObject.SetActive(true);
        gameObject.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);

        if (_tween1 == null)
        {
            _tween1 = new TweenParms();
            _tween1.Prop("localScale", new Vector3(1, 1, 1));
            _tween1.Ease(EaseType.EaseOutBack);
        }

        HOTween.To(gameObject.transform, 0.2f, _tween1);
    }

    void OnEnable()
    {
        _isActive = true;
        _timeLeft = Settings.continueArcadeTime;
    }

    void Update()
    {
        if (_isActive == false || Time.timeScale == 0)
            return;

        _timeLeft -= Time.deltaTime;
        timerTF.text = ((int)(_timeLeft + 0.9f)).ToString();
        timerTF.Commit();
        if (_timeLeft <= 0)
            close();
    }

    void Start()
    {
        priceTF.text = Settings.continueArcadeCost.ToString();
        priceTF.Commit();
        shopBtn.OnClick += showShop;
    }

    void showShop()
    {
        _isActive = false;
        if (UserData.money >= Settings.continueArcadeCost)
        {
            UserData.money -= Settings.continueArcadeCost;
            gameObject.SetActive(false);
            _game.continueGame();
        }
        else
        {
            gameManager.showShop();
        }


    }

    void close()
    {
        gameObject.SetActive(false);
        _game.gameOver(true, false);
    }

}

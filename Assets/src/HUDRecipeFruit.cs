﻿using UnityEngine;
using System.Collections;

public class HUDRecipeFruit : MonoBehaviour 
{

    public tk2dSprite icon;
    public tk2dSprite completedSprite;
    public tk2dTextMesh countTF;
    public PulsationLifebarAnimator pulsator;

    private HUDRecipeFruits _container;
    private int _index;
    private PositionedSprite positionedSprite;

    public void init(HUDRecipeFruits container, int index)
    {
        _container = container;
        _index = index;
        positionedSprite = gameObject.GetComponent<PositionedSprite>();
    }

    public void reset(string spriteName, int cnt)
    {
        gameObject.SetActive(true);
        countTF.gameObject.SetActive(true);
        icon.gameObject.SetActive(true);
        completedSprite.gameObject.SetActive(false);
        icon.SetSprite(spriteName);
        count = cnt;
    }

    public void initPosition(int x)
    {
        positionedSprite.currentOrientation = new Vector2(x, positionedSprite.currentOrientation.y);
        positionedSprite.calc();
    }

    public int count
    {
        set
        {
            if (value > 0)
            {
                countTF.text = value.ToString();
                countTF.Commit();
                pulsator.startPulsation(1);
            }
            else
            {
                countTF.gameObject.SetActive(false);
                completedSprite.gameObject.SetActive(true);
                _container.fruitComplete(_index);
            }
        }
    }

}

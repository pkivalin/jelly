//
//  CrossPromotionPlugin.cs
//
//  Copyright 2014 GameHouse, a division of RealNetworks, Inc.
// 
//  The GameHouse Promotion Network SDK is licensed under the Apache License, 
//  Version 2.0 (the "License"); you may not use this file except in compliance 
//  with the License. You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
//  Unless required by applicable law or agreed to in writing, software
//  distributed under the License is distributed on an "AS IS" BASIS,
//  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//  See the License for the specific language governing permissions and
//  limitations under the License.
//

using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

public interface ICrossPromotionPluginDelegate {
	void OnInterstitialReceived(CrossPromotionPlugin plugin);
	void OnInterstitialFailed(CrossPromotionPlugin plugin);
	void OnInterstitialOpened(CrossPromotionPlugin plugin);
	void OnInterstitialClosed(CrossPromotionPlugin plugin);
}

public struct CrossPromotionDebugSettings
{
	public bool debugMode;
}

public class CrossPromotionPlugin : MonoBehaviour {

    private bool _inited = false;

	private delegate void CrossPromotionPluginCallback();
	
	/// <summary>
	/// The SDK wrapper version.
	/// </summary>
	public static readonly string WrapperVersion = "2.0.0";
	
	/// <summary>
	/// The plugin delegate. Listens for all CrossPromotion-related events.
	/// </summary>
	private ICrossPromotionPluginDelegate pluginDelegate;
	
	/// <summary>
	/// The cross promotion bridge object.
	/// </summary>
	private CrossPromotion crossPromotion;
	
	/// <summary>
	/// The callbacks lookup.
	/// </summary>
	private IDictionary<String, CrossPromotionPluginCallback> callbacksLookup;
	
	/// <summary>
	/// The app identifier for iOS
	/// </summary>
	public String appIdIOS = "0";
	
	/// <summary>
	/// The app identifier for Android
	/// </summary>
	public String appIdAndroid = "0";
	
	/// <summary>
	/// Debug mode flag.
	/// </summary>
	public bool debugMode;
	
	#region CrossPromotion
	
	public void Init() {
		if (callbacksLookup == null) {
			callbacksLookup = CreateCallbacksLookup();
		}
		
		if (crossPromotion == null) {

			CrossPromotionDebugSettings settings = new CrossPromotionDebugSettings();
			settings.debugMode = debugMode;

			crossPromotion = CreateCrossPromotion();
			crossPromotion.Initialize(settings);
		}

        _inited = true;
	}
	
	public void StartRequestingInterstitials() {
        if (_inited == false)
            Init();
		crossPromotion.StartRequestingInterstitials();
	}
	
	public CrossPromotionResult PresentInterstitial(IDictionary<string, string> args = null) {
		string argsString = CreateArgsString(args);
		return crossPromotion.PresentInterstitial(argsString);
	}
	
	public void StopRequestingInterstitials() {
		crossPromotion.StopRequestingInterstitials();
	}
	
	internal void OverrideAppId(String appId) {
		crossPromotion.OverrideAppId(appId);
	}
	
	internal void OverrideBaseURL(String baseURL) {
		crossPromotion.OverrideBaseURL(baseURL);
	}
	
	private CrossPromotion CreateCrossPromotion() {
		#if UNITY_IPHONE
		if (Application.platform != RuntimePlatform.OSXEditor) {
			return new IPhoneCrossPromotion(appIdIOS, name);
		}
		return new NullCrossPromotion();
		#elif UNITY_ANDROID
		return new AndroidCrossPromotion(appIdAndroid, name);
		#else
		return new NullCrossPromotion();
		#endif
	}
	
	private string CreateArgsString(IDictionary<string, string> args) {
		if (args != null && args.Count > 0) {
			StringBuilder buffer = new StringBuilder();
			int argIndex = 0;
			foreach (KeyValuePair<string, string> e in args) {
				buffer.Append(e.Key);
				buffer.Append('=');
				buffer.Append(e.Value);
				
				if (++argIndex < args.Count)
				{
					buffer.Append('&');
				}
			}
			return buffer.ToString();
		}
		
		return null;
	}
	
	public void dispatchNativeMessage(String message) {
		CrossPromotionPluginCallback callback;
		if (callbacksLookup.TryGetValue(message, out callback)) {
			callback();
		} else {
			Debug.LogError("CrossPromotion: unable to find callback for '" + message + "'");
		}	
	}
	
	#endregion
	
	#region Callbacks
	
	private IDictionary<String, CrossPromotionPluginCallback> CreateCallbacksLookup() {
		IDictionary<String, CrossPromotionPluginCallback> lookup = new Dictionary<String, CrossPromotionPluginCallback>();
		lookup["gpn.received"] = OnInterstitialReceived;
		lookup["gpn.failed"]   = OnInterstitialFailed;
		lookup["gpn.opened"]   = OnInterstitialOpened;
		lookup["gpn.closed"]   = OnInterstitialClosed;
		
		return lookup;
	}
	
	private void OnInterstitialReceived() {
		Debug.Log("Interstitial received");
		if (pluginDelegate != null) {
			pluginDelegate.OnInterstitialReceived(this);
		}
	}
	
	private void OnInterstitialFailed() {
		Debug.LogError("Interstitial failed");
		if (pluginDelegate != null) {
			pluginDelegate.OnInterstitialFailed(this);
		}
	}
	
	private void OnInterstitialOpened() {
		Debug.Log("Interstitial opened");
		if (pluginDelegate != null) {
			pluginDelegate.OnInterstitialOpened(this);
		}
	}
	
	private void OnInterstitialClosed() {
		Debug.Log("Interstitial closed");
		if (pluginDelegate != null) {
			pluginDelegate.OnInterstitialClosed(this);
		}
	}
	
	#endregion
	
	#region Properties
	
	public ICrossPromotionPluginDelegate Delegate {
		get { return pluginDelegate; }
		set { pluginDelegate = value; }
	}
	
	public String AppId {
		get { return crossPromotion != null ? crossPromotion.AppId : null; }
	}

	public String SDKVersion {
		get { return crossPromotion != null ? crossPromotion.SDKVersion : "?.?.?"; }
	}
	
	#endregion
}

public enum CrossPromotionResult {
	Presented,
	NotPresented,
	Failed
}

abstract class CrossPromotion {
	protected String appId;
	
	protected CrossPromotion(String appId) {
		this.appId = appId;
	}
	
	public abstract void Initialize(CrossPromotionDebugSettings debugSettings);
	public abstract void StartRequestingInterstitials();
	public abstract CrossPromotionResult PresentInterstitial(String argString);
	public abstract void StopRequestingInterstitials();
	public abstract void Destroy();
	public abstract void OverrideAppId(String appId);
	public abstract void OverrideBaseURL(String baseURL);
	
	public String AppId {
		get { return appId; }
	}

	public virtual String SDKVersion {
		get { return "?.?.?"; }
	}
}

#region CrossPromotion Null

/// <summary>
/// Default implementation for Unity Editor and unsupported platforms
/// </summary>
class NullCrossPromotion : CrossPromotion {
	
	public NullCrossPromotion() : base("0") {
		Debug.LogWarning("CrossPromotion warning: platform not supported. All calls will be ignored");
	}
	
	public override void Initialize(CrossPromotionDebugSettings debugSettings) {
	}
	
	public override void StartRequestingInterstitials() {
	}
	
	public override CrossPromotionResult PresentInterstitial(String argString) {
		return CrossPromotionResult.Failed;
	}
	
	public override void StopRequestingInterstitials() {
	}
	
	public override void Destroy() {
	}
	
	public override void OverrideAppId(String appId) {
	}
	
	public override void OverrideBaseURL(String baseURL) {
	}
}

#endregion

#region CrossPromotion iPhone

#if UNITY_IPHONE
class IPhoneCrossPromotion : CrossPromotion {
	
	[DllImport("__Internal")]
	private static extern void _CPInitialize(String version, String targetName, String appID, bool debugMode);
	[DllImport("__Internal")]
	private static extern void _CPStartRequestingInterstitials();
	[DllImport("__Internal")]
	private static extern int  _CPPresentInterstitial(string argString);
	[DllImport("__Internal")]
	private static extern int  _CPStopRequestingInterstitials();
	[DllImport("__Internal")]
	private static extern void _CPInterstitialDestroy();
	[DllImport("__Internal")]
	private static extern void _CPSetAppId(String appId);
	[DllImport("__Internal")]
	private static extern void _CPSetBaseURL(String url);
	[DllImport("__Internal")]
	private static extern int  _CPGetSDKVersion();
	
	private String gameObjectName;
	
	public IPhoneCrossPromotion(String appId, String gameObjectName) : base(appId) {
		this.gameObjectName = gameObjectName;
	}
	
	public override void Initialize(CrossPromotionDebugSettings debugSettings) {
		_CPInitialize(CrossPromotionPlugin.WrapperVersion, gameObjectName, appId, debugSettings.debugMode);
	}
	
	public override void StartRequestingInterstitials() {
		_CPStartRequestingInterstitials();
	}
	
	public override CrossPromotionResult PresentInterstitial(string argString) {
		int result = _CPPresentInterstitial(argString);
		return (CrossPromotionResult)result;
	}
	
	public override void StopRequestingInterstitials() {
		_CPStopRequestingInterstitials();
	}
	
	public override void Destroy() {
		_CPInterstitialDestroy();
	}
	
	public override void OverrideAppId(String appId) {
		_CPSetAppId(appId);
	}
	
	public override void OverrideBaseURL(String baseURL) {
		_CPSetBaseURL(baseURL);
	}

	public override String SDKVersion {
		get { 
			int value = _CPGetSDKVersion();
			if (value == -1) {
				return "?.?.?"; 
			}

			int c = value % 100; value /= 100;
			int b = value % 100; value /= 100;
			int a = value % 100;

			return a + "." + b + "." + c;
		}
	}
}
#endif // UNITY_IPHONE

#endregion

#region CrossPromotion Android

#if UNITY_ANDROID
class AndroidCrossPromotion : CrossPromotion {
	
	private static readonly String PLUGIN_CLASSNAME	      = "com.gamehouse.crosspromotion.unity.CrossPromotionPluginActivity";
	
	private static readonly String METHOD_INITIALIZE      = "initializeCrossPromotion";
	private static readonly String METHOD_REQUEST         = "startRequestingInterstitials";
	private static readonly String METHOD_PRESENT         = "presentInterstitial";
	private static readonly String METHOD_CANCEL          = "stopRequestingInterstitials";
	private static readonly String METHOD_DESTROY         = "destroyCrossPromotion";
	private static readonly String METHOD_SET_APP_ID      = "setAppId";
	private static readonly String METHOD_SET_BASE_URL    = "setBaseURL";
	private static readonly String METHOD_CONNECT_MONITOR = "connectRemoteMonitor";
	private static readonly String METHOD_GET_SDK_VERSION = "getSDKVersion";
	
	private AndroidJavaClass pluginClass;
	private String gameObjectName;
	
	public AndroidCrossPromotion(String appId, String gameObjectName) : base(appId) {
		this.gameObjectName = gameObjectName;
	}
	
	public override void Initialize(CrossPromotionDebugSettings debugSettings) {
		pluginClass = new AndroidJavaClass(PLUGIN_CLASSNAME);

		bool debugMode = debugSettings.debugMode;
		pluginClass.CallStatic(METHOD_INITIALIZE, CrossPromotionPlugin.WrapperVersion, gameObjectName, appId, debugMode);
	}
	
	public override void StartRequestingInterstitials() {
		pluginClass.CallStatic(METHOD_REQUEST);
	}
	
	public override CrossPromotionResult PresentInterstitial(string argString) {
		int result = pluginClass.CallStatic<int>(METHOD_PRESENT, argString);
		return (CrossPromotionResult)result;
	}
	
	public override void StopRequestingInterstitials() {
		pluginClass.CallStatic(METHOD_CANCEL);
	}
	
	public override void Destroy() {
		if (pluginClass != null) {
			pluginClass.CallStatic(METHOD_DESTROY);
			pluginClass.Dispose();
			pluginClass = null;
		}
	}
	
	public override void OverrideAppId(String appId) {
		pluginClass.CallStatic(METHOD_SET_APP_ID, appId);
	}
	
	public override void OverrideBaseURL(String baseURL) {
		pluginClass.CallStatic(METHOD_SET_BASE_URL, baseURL);
	}

	public override String SDKVersion {
		get { 
			if (pluginClass != null) {
				return pluginClass.CallStatic<String>(METHOD_GET_SDK_VERSION);
			}
			return "?.?.?"; 
		}
	}
}
#endif // UNITY_ANDROID

#endregion
